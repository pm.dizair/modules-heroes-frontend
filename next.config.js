/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  env: {
    BASE_URL: process.env.BASE_URL,
    FAST_REVALIDATE: process.env.FAST_REVALIDATE,
    MIDDLE_REVALIDATE: process.env.MIDDLE_REVALIDATE,
    LONG_REVALIDATE: process.env.LONG_REVALIDATE,
    DAY_REVALIDATE: process.env.DAY_REVALIDATE,
    FACEBOOK_APP_ID: process.env.FACEBOOK_APP_ID,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    MEDIA_ROOT: process.env.MEDIA_ROOT,
    SOCKET_URL: process.env.SOCKET_URL,
  },
  images: {
    deviceSizes: [320, 375, 420, 540, 600, 767, 1024, 1280, 1440, 1920],
    remotePatterns: [
      {
        protocol: 'http',
        hostname: '127.0.0.1',
      },
     
    ],
  },
};

module.exports = nextConfig;
