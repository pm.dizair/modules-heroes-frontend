import { baseApi } from '@/utils/api/rtk-query';

export const navigationApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    getNavigation: build.query({
      query: ({ language }) => ({
        url: `${language}/content/navigation/`,
        method: 'GET',
      }),
    }),
  }),
});

export const { useGetNavigationQuery } = navigationApi;
