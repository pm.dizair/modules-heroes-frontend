import { IWithError } from '@/common/types';
import { StringPropertyNames } from '@/common/types/general';
import { IIcon, INavigation } from '@/layouts/Basic/types';

export interface IGetNavigation {
  deposit: INavigation[];
  profile: INavigation[];
  vip: INavigation[];
  footer: INavigation[];
  header: INavigation[];
}

export interface IGetFooter {
  crypto: IIcon[];
  partners: IIcon[];
  partnersFooter: IIcon[];
  paysystem: IIcon[];
}
export interface IStateNavigation extends IWithError, IGetNavigation {}
export interface IStateFooter extends IWithError, IGetFooter {}

export interface IGetNavigationResponse extends IWithError {
  data?: IGetNavigation;
}
export interface IGetFooterResponse extends IWithError {
  data?: IGetFooter;
}
export type TStringNavigation = StringPropertyNames<IGetNavigation>;
export type TStringFooter = StringPropertyNames<IGetFooter>;
