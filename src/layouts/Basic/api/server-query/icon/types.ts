import { IWithError } from '@/common/types';
import { INavigation } from '@/layouts/Basic/types';
export interface IGetFooterIconsResponse extends IWithError {
  items: INavigation[];
}
