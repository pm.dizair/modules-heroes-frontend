import { handleError } from '@/common/helpers/handleError';
import { axiosBase } from '@/utils/api/server-query/base';

import { IGetFooterIconsResponse } from './types';

export const IconApi = {
  async getFooterIcons(): Promise<Partial<IGetFooterIconsResponse>> {
    return handleError<IGetFooterIconsResponse>(async () => {
      const { data } = await axiosBase.get(`/content/partners/`);
      return data;
    });
  },
};
