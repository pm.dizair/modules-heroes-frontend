import { useAppSelector } from '@/common/hooks';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  IGetFooterResponse,
  IGetNavigationResponse,
  TStringFooter,
  TStringNavigation,
} from './api/rtk-query/navigation/types';
import { ILayoutSliceState } from './types';

const initialState: ILayoutSliceState = {
  navigation: {
    header: [],
    footer: [],
    deposit: [],
    profile: [],
    vip: [],
  },
  footer: {
    crypto: [],
    partners: [],
    partnersFooter: [],
    paysystem: [],
  },
};

export const layoutSlice = createSlice({
  name: 'layout',
  initialState,
  reducers: {
    updateNavigation: (
      state,
      action: PayloadAction<IGetNavigationResponse>
    ) => {
      const { data, isError, error } = action.payload;
      if (data) {
        for (const key in data) {
          state.navigation[key as TStringNavigation] =
            data[key as TStringNavigation];
        }
      }
      if (isError) {
        state.navigation.isError = isError;
        state.navigation.error = error;
      }
    },
    updateFooter: (state, action: PayloadAction<IGetFooterResponse>) => {
      const { data, isError, error } = action.payload;
      if (data) {
        for (const key in data) {
          state.footer[key as TStringFooter] = data[key as TStringFooter];
        }
      }
      if (isError) {
        state.footer.isError = isError;
        state.footer.error = error;
      }
    },
  },
});

export const useLayout = () =>
  useAppSelector((state) => state[layoutSlice.name]);

export const { updateNavigation, updateFooter } = layoutSlice.actions;
