import { PropsWithChildren } from 'react';

import {
  IStateFooter,
  IStateNavigation,
} from './api/rtk-query/navigation/types';
export interface ILayoutSliceState {
  navigation: IStateNavigation;
  footer: IStateFooter;
}

export interface IIcon {
  id: number;
  name: string;
  image: string;
  url: string | null | undefined;
}

export interface INavigation {
  id: number;
  name: string;
  keyword: string;
  description?: string;
  slug: string;
  image?: string;
}
export interface IBasicLayout extends PropsWithChildren {
  isMobileHide?: boolean;
}
