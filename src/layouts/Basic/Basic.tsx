import { LittleMobileOn } from '@/utils/lib/responsive/responsive';
import { FC } from 'react';

import { CSSTabletOff } from '@/utils/lib/responsive/breakpoints';
import BottomMenu from './components/BottomMenu/BottomMenu';
import BurgerMenu from './components/BurgerMenu/BurgerMenu';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import { LayoutProvider } from './context';
import { Wrapper } from './styles';
import { IBasicLayout } from './types';

const BasicLayout: FC<IBasicLayout> = (props) => {
  const { children, isMobileHide } = props;

  if (isMobileHide) {
    return (
      <LayoutProvider>
        <CSSTabletOff>
          <Header />
          <BurgerMenu />
        </CSSTabletOff>
        <Wrapper>{children}</Wrapper>
        <CSSTabletOff>
          <Footer />
          <LittleMobileOn>
            <BottomMenu />
          </LittleMobileOn>
        </CSSTabletOff>
      </LayoutProvider>
    );
  }
  return (
    <LayoutProvider>
      <Header />
      <BurgerMenu />
      <Wrapper>{children}</Wrapper>
      <Footer />
      <LittleMobileOn>
        <BottomMenu />
      </LittleMobileOn>
    </LayoutProvider>
  );
};
export default BasicLayout;
