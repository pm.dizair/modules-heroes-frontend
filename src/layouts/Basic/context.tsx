import { createContext, FC, ReactNode, useContext, useState } from 'react';

interface ILayoutContext {
  isOpenBurgerMenu: boolean;
  setIsOpenBurgerMenu: (x: boolean) => void;
}

export const LayoutContext = createContext<ILayoutContext>({
  isOpenBurgerMenu: false,
  setIsOpenBurgerMenu: (x: boolean) => {},
});

export const useLayoutContext = () => useContext(LayoutContext);

interface ILayoutProps {
  children: ReactNode;
}

export const LayoutProvider: FC<ILayoutProps> = (props) => {
  const { children } = props;
  const [isOpenBurgerMenu, setIsOpenBurgerMenu] = useState<boolean>(false);

  return (
    <LayoutContext.Provider
      value={{
        isOpenBurgerMenu,
        setIsOpenBurgerMenu: (value: boolean) => setIsOpenBurgerMenu(value),
      }}
    >
      {children}
    </LayoutContext.Provider>
  );
};
