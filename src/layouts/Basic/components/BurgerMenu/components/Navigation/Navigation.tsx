import { useLayout } from '@/layouts/Basic/slice';
import Image from 'next/image';
import { useRouter } from 'next/router';

import { PAGES } from '@/common/const';
import { getImage } from '@/common/helpers/getImage';
import { useLayoutContext } from '@/layouts/Basic/context';
import { Icon, Item, List, Wrapper } from './styles';

const Navigation = () => {
  const { navigation } = useLayout();
  const { pathname } = useRouter();
  const { GAMES, SHOP } = PAGES;
  const { setIsOpenBurgerMenu } = useLayoutContext();

  return (
    <Wrapper>
      <List>
        {navigation.header?.map((item) => (
          <Item
            $active={pathname.includes(item.slug)}
            shallow
            href={`/${item.slug}${
              item.slug === GAMES
                ? '?category=popular'
                : item.slug === SHOP
                ? '/bonus_money'
                : ''
            }`}
            key={item.id}
            onClick={() => setIsOpenBurgerMenu(false)}
          >
            <Icon>
              <Image
                src={getImage(item.image)}
                width={34}
                height={34}
                alt=""
              />
            </Icon>
            {item.name}
          </Item>
        ))}
      </List>
    </Wrapper>
  );
};
export default Navigation;
