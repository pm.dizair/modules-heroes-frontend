import Link from 'next/link';
import styled from 'styled-components';
import { IBurgerMenuStyle } from '../../types';

export const Wrapper = styled.div`
  width: 100%;
  z-index: 100;
`;
export const List = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  z-index: 100;
`;
export const Item = styled(Link)<IBurgerMenuStyle>`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  font-family: var(--font-nunito);
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  color: var(--gray);
  padding: 8px 0;
  z-index: 100;
  position: relative;
  border-bottom: 1px solid #29333f;
  ${(props) => props.$active && `color: var(--gold);`}
  &:hover {
    color: var(--gold);
  }
`;
export const Icon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  min-width: 40px;
  height: 34px;
  margin-right: 12px;
  & > img {
    width: 100%;
    height: 100%;
    display: block;
    object-fit: contain;
  }
`;
