import styled from 'styled-components';
export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  padding: 16px 0;
`;
export const Logo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 100;
  & > img {
    height: 30px;
  }
`;
export const CloseBtn = styled.button`
  position: absolute;
  right: 12px;
  top: 12px;
  width: 30px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  & > svg {
    fill: #b3b3b3;
    transition: 0.3s;
    width: 30px;
    height: 30px;
  }
  &:hover {
    & > svg {
      transition: 0.3s;
      fill: #ffd600;
    }
  }
  @media screen and (max-width: 600px) {
    right: 8px;
  }
`;
