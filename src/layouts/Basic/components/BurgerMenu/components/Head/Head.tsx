import { IconClose } from '@/assets/icons-ts/global';
import IconLogo from '@/assets/icons/logo_text.svg';
import { LINK_TEMPLATES } from '@/common/const';

import { useLayoutContext } from '@/layouts/Basic/context';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { CloseBtn, Logo, Wrapper } from './styles';

const Head = () => {
  const { push } = useRouter();
  const { setIsOpenBurgerMenu } = useLayoutContext();

  const handlerMainPagePush = () => {
    push(LINK_TEMPLATES.HOME(), undefined, { shallow: true });
  };

  return (
    <Wrapper>
      <Logo onClick={handlerMainPagePush}>
        <Image
          priority
          src={IconLogo}
          placeholder="empty"
          alt=""
        />
      </Logo>
      <CloseBtn
        onClick={() => setIsOpenBurgerMenu(false)}
        type="button"
        title="close"
      >
        <IconClose />
      </CloseBtn>
    </Wrapper>
  );
};
export default Head;
