import Link from 'next/link';
import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 16px;
`;

export const AvatarWrap = styled.div`
  width: 60px;
  min-width: 60px;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 1px;
  margin-right: 12px;
  border-radius: 8px;
  overflow: hidden;
  background: radial-gradient(
      50% 775.7% at 50% 50%,
      #f5e58c 18.77%,
      #f5d121 86.01%,
      #e78f03 100%
    ),
    radial-gradient(50% 50% at 50% 50%, #2b5e99 0%, #1e073c 100%);
`;
export const Avatar = styled(Link)`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 7px;
  overflow: hidden;
  background: radial-gradient(
    49.99% 50.24% at 50.1% 49.45%,
    #234038 54%,
    #162f32 77%,
    #061b2b 100%
  );

  & > img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    display: block;
  }
`;
export const Column = styled.div`
  width: calc(100% - 72px);
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  gap: 6px;
`;
export const Name = styled.div`
  width: 100%;
  font-family: var(--font-grenze-gotisch);
  font-style: normal;
  font-weight: 700;
  font-size: 28px;
  line-height: 28px;
  color: var(--gold);
  ${({ theme }) => theme.text.overflow};
`;
export const LinkProfile = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: var(--font-nunito);
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 22px;
  color: var(--gray);
  &:hover {
    text-decoration: underline;
  }
  & > img {
    margin-left: 6px;
  }
`;
