import IconHelmet from '@/assets/icons/Helmet.svg';
import IconArrowGray from '@/assets/icons/shortArrowGray.svg';
import { LINK_TEMPLATES } from '@/common/const';
import { useLayoutContext } from '@/layouts/Basic/context';
import { useUser } from '@/modules/auth/store/user/user.slice';
import Image from 'next/image';
import React from 'react';
import {
  Avatar,
  AvatarWrap,
  Column,
  LinkProfile,
  Name,
  Wrapper,
} from './styles';

const User = () => {
  const { user } = useUser();
  const { username, first_name, email } = user || {};
  const { setIsOpenBurgerMenu } = useLayoutContext();

  return (
    <Wrapper>
      <AvatarWrap>
        <Avatar
          title="profile"
          href={LINK_TEMPLATES.PROFILE({
            tab: 'personal-info',
          })}
          onClick={() => setIsOpenBurgerMenu(false)}
        >
          <Image
            src={IconHelmet}
            alt=""
            width={60}
            height={60}
          />
        </Avatar>
      </AvatarWrap>
      <Column>
        <Name>{username || first_name || email}</Name>
        <LinkProfile
          href={LINK_TEMPLATES.PROFILE({
            tab: 'personal-info',
          })}
          onClick={() => setIsOpenBurgerMenu(false)}
        >
          My profile
          <Image
            width={12}
            height={22}
            src={IconArrowGray}
            alt=""
          />
        </LinkProfile>
      </Column>
    </Wrapper>
  );
};
export default User;
