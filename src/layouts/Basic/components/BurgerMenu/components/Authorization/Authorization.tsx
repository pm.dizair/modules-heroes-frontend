import { useTranslations } from "@/common/hooks";
import { useUser } from "@/modules/auth/store/user/user.slice";
import { Button } from "@/ui-library/buttons";
import { SeparateLine, StarRating } from "@/ui-library/decorations";
import { MODALS } from "@/utils/lib/modal/const";
import { useModal } from "@ebay/nice-modal-react";

import { LINK_TEMPLATES } from "@/common/const";
import { useLayoutContext } from "@/layouts/Basic/context";
import { useRouter } from "next/router";
import React from "react";
import User from "../User/User";
import { ButtonWrap, Row, VipLink, Wrapper } from "./styles";

const Authorization = () => {
  const { isAuth } = useUser();
  const { show: showRegister } = useModal(MODALS.REGISTER);
  const { setIsOpenBurgerMenu } = useLayoutContext();
  const t = useTranslations();
  const { push } = useRouter();

  const redirectToProfile = () => {
    setIsOpenBurgerMenu(false);
    push(
      LINK_TEMPLATES.PROFILE({
        tab: "personal-info",
      })
    );
  };
  const login = () => {
    setIsOpenBurgerMenu(false);
    showRegister({ modal: MODALS.LOGIN });
  };
  const registration = () => {
    setIsOpenBurgerMenu(false);
    showRegister({ modal: MODALS.REGISTER });
  };

  return (
    <>
      {isAuth ? (
        <Wrapper>
          <User />
          <Row>
            <StarRating level={3} />
            <VipLink onClick={redirectToProfile}>More</VipLink>
          </Row>
          <SeparateLine />
        </Wrapper>
      ) : (
        <ButtonWrap>
          <Button theme="gray" size="xl" onClick={login}>
            {t("header_log_in")}
          </Button>
          <Button size="xl" onClick={registration}>
            {t("header_register")}
          </Button>
        </ButtonWrap>
      )}
    </>
  );
};
export default Authorization;
