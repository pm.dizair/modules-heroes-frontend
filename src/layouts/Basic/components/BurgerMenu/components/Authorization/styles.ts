import Link from 'next/link';
import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  padding: 16px 0 8px 0;
  .separate-line {
    margin-top: 16px;
  }
`;

export const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 12px;
  padding-bottom: 24px;
  & > button {
    width: 100%;
  }
`;
export const Row = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const VipLink = styled.button`
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;
  color: var(--gray);
  &:hover {
    text-decoration: underline;
    color: var(--gold);
  }
`;
