import Image from 'next/image';
import styled from 'styled-components';
import { IBurgerMenuStyle } from './types';

export const Mask = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 1000;
  position: fixed;
  top: 0;
  left: 0;
  backdrop-filter: blur(5px);
  background: rgba(0, 0, 0, 0.5);
  transition: background-color 0.5s linear;
`;
export const Content = styled.div<IBurgerMenuStyle>`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10000;
  transform: ${(props) =>
    props.isOpen ? 'translate(0, 0)' : 'translate(-100%, 0)'};
  transition: transform ${(props) => (props.isOpen ? '0.5s' : ' 0.9s')};
  @media screen and (max-width: 767px) {
    transition: transform ${(props) => (props.isOpen ? '0.5s' : ' 0.4s')};
  }
  @media screen and (max-width: 540px) {
    height: calc(100% + 60px);
    padding-bottom: 60px;
  }
`;

export const Body = styled.div`
  width: 375px;
  height: 100%;
  position: relative;
  display: flex;
  overflow-y: auto;
  flex-direction: column;
  padding: 0 16px 32px 16px;
  background: #010d1b;
  overflow-y: auto;

  &::-webkit-scrollbar {
    width: 0px;
  }

  @media screen and (max-width: 540px) {
    width: 100%;
    padding: 0 16px 16px 16px;
  }
`;

export const Img = styled(Image)`
  position: absolute;
  width: 375px;
  height: auto;
  bottom: 0;
  left: 0;
  @media screen and (max-width: 540px) {
    width: 100%;
    left: auto;
    right: 0;
  }
`;
export const Row = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 24px 0;
  & > div {
    justify-content: flex-start;
    & > button {
      flex-direction: row-reverse;
      justify-content: flex-end;
    }
  }
  .drop-animated {
    min-width: 150px;
    left: 0;
    transform: translate(0, calc(100% + 8px)) !important;
    background-color: #010d1b;
    border: 1px solid #29333f;
    & > li {
      padding: 6px 12px;
      justify-content: flex-end;
      flex-direction: row-reverse;
      border-bottom: 1px solid #29333f;
      &:hover {
        background-color: #162231;
      }
    }
  }
`;
