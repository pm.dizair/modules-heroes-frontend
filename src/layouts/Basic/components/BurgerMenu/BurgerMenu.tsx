import { IconSearch } from '@/assets/icons-ts/global';
import ImgBg from '@/assets/images/backgrounds/menu_bg.webp';
import { useCommon } from '@/common/store/common/common.slice';
import { RoundButton } from '@/ui-library/buttons';
import { MODALS } from '@/utils/lib/modal/const';
import { useModal } from '@ebay/nice-modal-react';
import React, { useEffect } from 'react';
import { useLayoutContext } from '../../context';
import { DropDown } from '../Header/components';
import Authorization from './components/Authorization/Authorization';
import Head from './components/Head/Head';
import Navigation from './components/Navigation/Navigation';
import { Body, Content, Img, Mask, Row } from './styles';

const BurgerMenu = () => {
  const { isOpenBurgerMenu, setIsOpenBurgerMenu } = useLayoutContext();
  const { allLanguages, language } = useCommon();
  const { show: showSearch } = useModal(MODALS.SEARCH);

  const search = () => {
    setIsOpenBurgerMenu(false);
    showSearch();
  };
  useEffect(() => {
    const body = document.querySelector('body');
    if (body) {
      body.style.overflowY = isOpenBurgerMenu ? 'hidden' : 'auto';
    }
  }, [isOpenBurgerMenu]);

  return (
    <>
      {isOpenBurgerMenu && <Mask />}
      <Content
        isOpen={isOpenBurgerMenu}
        onClick={() => setIsOpenBurgerMenu(false)}
      >
        <Body
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <Head />
          <Authorization />
          <Navigation />
          <Row>
            <DropDown
              allLanguages={allLanguages || []}
              language={language}
            />

            <RoundButton
              onClick={search}
              title="search"
            >
              <IconSearch />
            </RoundButton>
          </Row>
        </Body>
        <Img
          src={ImgBg}
          alt=""
        />
      </Content>
    </>
  );
};
export default BurgerMenu;
