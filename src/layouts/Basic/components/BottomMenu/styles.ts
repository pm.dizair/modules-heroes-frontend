import Image from 'next/image';
import Link from 'next/link';
import styled from 'styled-components';

import { IStyle } from './types';

export const Wrapper = styled.div<IStyle>`
  width: 100%;
  height: ${(props) => (props.isActive ? '171px' : '70px')};
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: column;
  @media screen and (max-device-width: 767px) and (orientation: landscape) {
    display: none;
  }
`;
export const Circle = styled.div`
  position: absolute;
  bottom: -10%;
  left: 50%;
  z-index: 0;
  width: calc(100% + 20px);
  transform: translate(-50%, 50%);
  height: 200%;
  pointer-events: none;
  background: rgb(8 20 34 / 98%);
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-shadow: inset 0px 0px 12px rgba(255, 255, 255, 0.12);
  border-radius: 50% 50% 0 0;
`;

export const RelativeBox = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.8));
`;

export const Shadow = styled.div`
  background: linear-gradient(
    0deg,
    rgba(1, 13, 27, 1) 30%,
    rgba(1, 13, 27, 0.8) 50%,
    rgba(1, 13, 27, 0.6) 60%,
    rgba(1, 13, 27, 0.1) 80%,
    rgba(1, 13, 27, 0) 100%
  );
  height: calc(100% + 20px);
  width: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
`;

export const FirstList = styled.div<IStyle>`
  width: 100%;
  height: 72px;
  position: absolute;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding: 0 24px 10px 24px;
  transform: ${(props) =>
    props.isActive ? 'translate(0, 90px) ' : 'translate(0, 0px)'};
  opacity: ${(props) => (props.isActive ? '0 ' : '1')};
  z-index: ${(props) => (props.isActive ? '0 ' : '1')};
  transition: 0.4s;
  & > a {
    z-index: 1;
    & img {
      width: 100%;
      height: 30px;
      object-fit: contain;
    }
  }
`;
export const IconNav = styled(Image)`
  position: absolute;
  height: 72px;
  bottom: -1px;
  left: 50%;
  transform: translate(calc(-50% + 1px), 0);
  z-index: 0;
  pointer-events: none;
`;

export const Relative = styled.div`
  position: relative;
  width: 100%;
  padding: 0 24px;
  max-height: 210px;
  aspect-ratio: 4 / 2;
  max-width: 475px;
  margin: 0 auto;
`;

export const SecondList = styled(FirstList)`
  pointer-events: ${(props) => (props.isActive ? 'all' : 'none')};
  transform: ${(props) =>
    props.isActive ? 'translate(0)' : 'translate(0, 50px)'};
  opacity: ${(props) => (props.isActive ? '1 ' : '0')};
  z-index: ${(props) => (props.isActive ? '1 ' : '0')};
  transition: ${(props) => (props.isActive ? '0.6s' : '0.3s')};
  transition-delay: 0.1s;
  padding: 0;
  bottom: 0;
  left: 0;
  height: auto;
  width: 100%;
  background: transparent;
`;
export const IconDecor = styled(Image)`
  color: transparent;
  position: absolute;
  z-index: 122;
  width: 200px;
  left: 50%;
  transform: translate(-50%, 0);
  bottom: 0;
`;
export const AbsoluteBlock = styled.div<IStyle>`
  position: absolute;
  top: 0;
  left: 0;
  width: 56px;
  min-width: 56px;
  height: 56px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: -23px;
  transition: 0.4s;
  left: 50%;
  transform: ${(props) =>
    props.isActive ? 'translate3d(-50%,10px,0)' : 'translate3d(-50%,0,0)'};
  z-index: 100;
`;
export const Name = styled.div`
  font-family: var(--font-nunito);
  font-style: normal;
  font-weight: 600;
  font-size: 10px;
  line-height: 14px;
  color: #ffffff;
  padding: 2px 0px;
`;
export const LinkCard = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 40px;
  img {
    width: 40px;
    height: 40px;
    object-fit: contain;
  }
`;
export const AnimationBtn = styled.div<IStyle>`
  width: 56px;
  min-width: 56px;
  height: 56px;
  position: absolute;
  inset: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  backface-visibility: hidden;
  opacity: ${(props) => (props.isActive ? '0' : '1')};
  transition: 0.4s;
  & img {
    width: 100%;
    height: 100%;
  }
`;

export const Space = styled.div`
  width: 60px;
`;

export const Radius = styled.div`
  position: absolute;
  transform: rotate(0deg);
  height: 72px;
  left: 40px;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  bottom: calc(-10% + 20px);
  z-index: 5;
  &:nth-child(2) {
    height: 65%;
    width: calc(50% - 100px);
    left: 25%;
    align-items: flex-start;
  }
  &:nth-child(3) {
    height: 100%;
    width: 100%;
    left: 0;
    bottom: calc(-10% - 20px);
    align-items: flex-start;
    justify-content: center;
  }
  &:nth-child(4) {
    height: 65%;
    width: calc(50% - 100px);
    left: auto;
    right: 25%;
    align-items: flex-start;
    justify-content: flex-end;
  }
  &:nth-child(5) {
    width: 100%;
    left: auto;
    right: 40px;
    justify-content: flex-end;
  }
`;
