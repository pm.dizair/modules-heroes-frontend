export interface IMenuDiv {
  id: number;
  type: 'div';
}

export interface IMenuLink {
  url?: string;
  name: string;
  position: number;
  image: string;
  slug?: string;
}

export interface IStyle {
  isActive?: boolean;
}
