import IconFirstMenu from '@/assets/icons/bottom-menu/bottom-nav-frame.png';
import IconClose from '@/assets/icons/bottom-menu/close-menu.png';
import IconEye from '@/assets/icons/bottom-menu/eye.png';
import IconSecondMenu from '@/assets/icons/bottom-menu/menu2-3.png';
import { useCommon } from '@/common/store/common/common.slice';
import Image from 'next/image';
import { useRef, useState } from 'react';
import { transformCategories } from './helpers';

import { LINK_TEMPLATES } from '@/common/const';
import { getImage } from '@/common/helpers/getImage';
import { useOutSideClick } from '@/common/hooks';
import { ITab } from '@/common/types';
import { first_menu } from './data';
import {
  AbsoluteBlock,
  AnimationBtn,
  Circle,
  FirstList,
  IconDecor,
  IconNav,
  LinkCard,
  Name,
  Radius,
  Relative,
  RelativeBox,
  SecondList,
  Shadow,
  Space,
  Wrapper,
} from './styles';

const BottomMenu = () => {
  const [isActiveMenu, setIsActiveMenu] = useState<boolean>(false);
  const { categories } = useCommon();
  const ref = useRef<HTMLDivElement>(null);

  useOutSideClick([ref], () => {
    if (isActiveMenu) {
      setIsActiveMenu(false);
    }
  });
  const close = () => {
    setIsActiveMenu(false);
  };
  const handleClick = () => {
    setIsActiveMenu(!isActiveMenu);
  };

  const renderNav = () => {
    const renderLinks = (from: number, to: number) => {
      return first_menu.slice(from, to).map((item) => (
        <LinkCard
          // TODO: wait for backend
          href={item.url || ''}
          key={item.position}
        >
          <Image
            src={item.image}
            alt=""
            width={40}
            height={40}
          />
          <Name>{item.name}</Name>
        </LinkCard>
      ));
    };
    return (
      <>
        {renderLinks(0, 2)}
        <Space />
        {renderLinks(2, 4)}
      </>
    );
  };

  const renderInternalNav = () => {
    return transformCategories(
      categories.filter((item) => item.is_bottom === true).slice(0, 5)
    ).map((item: ITab) => (
      <Radius key={item.position}>
        <LinkCard
          href={LINK_TEMPLATES.GAME_LISTING({
            category: item.slug,
          })}
          shallow
          key={item.position}
          onClick={close}
        >
          <Image
            src={getImage(item.mobile_image) || ''}
            alt=""
            width={40}
            height={40}
          />
          <Name>{item.name}</Name>
        </LinkCard>
      </Radius>
    ));
  };

  return (
    <Wrapper ref={ref}>
      <RelativeBox>
        <AbsoluteBlock
          isActive={isActiveMenu}
          onClick={() => handleClick()}
        >
          <AnimationBtn isActive={isActiveMenu}>
            <Image
              src={IconEye}
              alt=""
              width={60}
              height={60}
            />
          </AnimationBtn>
          <AnimationBtn isActive={!isActiveMenu}>
            <Image
              src={IconClose}
              alt=""
              width={60}
              height={60}
            />
          </AnimationBtn>
        </AbsoluteBlock>
        <FirstList isActive={isActiveMenu}>
          {renderNav()}
          <Shadow />
          <IconNav
            src={IconFirstMenu}
            width={777}
            height={60}
            alt=""
          />
        </FirstList>
        <SecondList isActive={isActiveMenu}>
          <Relative>
            {renderInternalNav()}
            <Circle />
            <IconDecor
              src={IconSecondMenu}
              alt=""
              width={200}
              height={92}
            />
          </Relative>
        </SecondList>
      </RelativeBox>
    </Wrapper>
  );
};

export default BottomMenu;
