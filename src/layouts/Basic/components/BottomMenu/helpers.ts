import { ITab } from '@/common/types';
import { first_menu, second_menu } from './data';

const merged = first_menu.concat(second_menu);

export const transformCategories = (tabs: ITab[]) => {
  return tabs.map((tab) => {
    const matched = merged.find((el) => el.slug === tab.slug);

    return {
      ...tab,
      image: matched?.image || '',
    };
  });
};
