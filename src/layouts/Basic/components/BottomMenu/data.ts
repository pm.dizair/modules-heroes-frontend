import { LINK_TEMPLATES } from '@/common/const';

import IconHot from '@/assets/icons/bottom-menu/hot.svg';
import IconHelm from '@/assets/icons/bottom-menu/live.svg';
import IconNew from '@/assets/icons/bottom-menu/new.png';
import IconPercent from '@/assets/icons/bottom-menu/promo.png';
import IconRoulette from '@/assets/icons/bottom-menu/roulette.png';
import IconSlot from '@/assets/icons/bottom-menu/slot.svg';
import IconSupport from '@/assets/icons/bottom-menu/support.png';
import IconTable from '@/assets/icons/bottom-menu/table.svg';
import IconTourneys from '@/assets/icons/bottom-menu/tourneys.svg';

import { IMenuLink } from './types';

export const first_menu: IMenuLink[] = [
  {
    position: 1,
    image: IconHelm,
    name: 'Live',
    slug: 'live',
    url: LINK_TEMPLATES.LIVE_CASINO_LISTING(),
  },
  {
    position: 2,
    image: IconSlot,
    slug: 'slot',
    name: 'Slots',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
  {
    position: 4,
    image: IconPercent.src,
    name: 'Promo',
    url: LINK_TEMPLATES.PROMOTIONS(),
  },
  {
    position: 5,
    image: IconSupport.src,
    name: 'Support',
    url: '/',
  },
];

export const second_menu: IMenuLink[] = [
  {
    position: 1,
    image: IconRoulette.src,
    name: 'Roulette',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
  {
    position: 2,
    image: IconTable,
    name: 'Table',
    slug: 'roulette',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
  {
    position: 3,
    image: IconHot,
    name: 'Hot',
    slug: 'hot',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
  {
    position: 4,
    image: IconNew,
    slug: 'new',
    name: 'New',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
  {
    position: 5,
    image: IconTourneys,
    name: 'Tourneys',
    slug: 'tournament',
    url: LINK_TEMPLATES.GAME_LISTING(),
  },
];
