import { SnackbarContent, useSnackbar } from 'notistack';
import { forwardRef } from 'react';

import { IconAlert } from '@/assets/icons-ts/global';

import TemplateSnack from '../Template/TemplateSnack';
import { variantColors } from '../data';
import { ISnack } from '../types';

const ErrorSnack = forwardRef<HTMLDivElement, ISnack>((props, ref) => {
  const { message, id } = props;
  const { closeSnackbar } = useSnackbar();

  const handlerClose = () => {
    closeSnackbar(id);
  };

  return (
    <SnackbarContent ref={ref}>
      <TemplateSnack
        color={variantColors['error']}
        hide={handlerClose}
        icon={<IconAlert />}
        message={message}
      />
    </SnackbarContent>
  );
});

ErrorSnack.displayName = 'ErrorSnack';

export default ErrorSnack;
