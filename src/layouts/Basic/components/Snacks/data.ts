export const variantColors = {
  error: 'linear-gradient(154deg, #F74443 0%, #B42727 100%)',
  info: 'linear-gradient(180deg, #40C2FF 0%, #0168FF 100%)',
  success: 'linear-gradient(180deg, #2BAA39 0%, #016859 100%)',
  cookies: 'linear-gradient(180deg, #D092F0 0%, #9024B4 100%)',
};
