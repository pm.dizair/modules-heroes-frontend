import { SnackbarContent, useSnackbar } from 'notistack';
import { forwardRef } from 'react';
import { IconInfo } from '@/assets/icons-ts/global';
import TemplateSnack from '../Template/TemplateSnack';
import { variantColors } from '../data';
import { ISnack } from '../types';

const InfoSnack = forwardRef<HTMLDivElement, ISnack>((props, ref) => {
  const { message, id } = props;
  const { closeSnackbar } = useSnackbar();

  const handlerClose = () => {
    closeSnackbar(id);
  };

  return (
    <SnackbarContent ref={ref}>
      <TemplateSnack
        color={variantColors['info']}
        hide={handlerClose}
        icon={<IconInfo />}
        message={message}
      />
    </SnackbarContent>
  );
});

InfoSnack.displayName = 'InfoSnack';

export default InfoSnack;
