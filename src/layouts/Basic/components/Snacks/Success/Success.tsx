import { SnackbarContent, useSnackbar } from 'notistack';
import { forwardRef } from 'react';

import { IconCheckSuccess } from '@/assets/icons-ts/global';
import TemplateSnack from '../Template/TemplateSnack';
import { variantColors } from '../data';
import { ISnack } from '../types';

const SuccessSnack = forwardRef<HTMLDivElement, ISnack>((props, ref) => {
  const { message, id } = props;

  const { closeSnackbar } = useSnackbar();

  const handlerClose = () => {
    closeSnackbar(id);
  };

  return (
    <SnackbarContent ref={ref}>
      <TemplateSnack
        color={variantColors['success']}
        hide={handlerClose}
        icon={<IconCheckSuccess />}
        message={message}
      />
    </SnackbarContent>
  );
});

SuccessSnack.displayName = 'SuccessSnack';

export default SuccessSnack;
