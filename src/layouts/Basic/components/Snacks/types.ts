export interface IWrapper {
  color: string;
}

export interface ISnack {
  message: string;
  id: number;
}
