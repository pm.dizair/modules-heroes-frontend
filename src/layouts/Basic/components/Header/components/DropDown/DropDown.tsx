import { getImage } from '@/common/helpers/getImage';
import { useAppDispatch, useOutSideClick } from '@/common/hooks';
import { updateLanguage } from '@/common/store/common/actions';
import { ILanguage } from '@/common/types/location';
import cookies from 'js-cookie';
import Image from 'next/image';
import { FC, useEffect, useRef, useState } from 'react';

import {
  Code,
  CurrentLang,
  Item,
  LangIconWrapper,
  List,
  Wrapper,
} from './styles';
import { IDropDown } from './types';

const DropDown: FC<IDropDown> = (props) => {
  const { allLanguages, language, isTest = false } = props;
  const [isOpen, setOpen] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const [active, setActive] = useState<ILanguage | null>(
    allLanguages?.find((lang: ILanguage) => lang.code === language) || null
  );
  const ref = useRef<HTMLDivElement>(null);
  const listRef = useRef<HTMLUListElement>(null);

  const updateLang = (lang: ILanguage) => {
    setActive(lang);
    localStorage.setItem('language', lang.code);
    cookies.set('language', lang.code);
    dispatch(updateLanguage(lang.code));
    setOpen(false);
  };

  const renderLangs = () =>
    allLanguages
      ?.filter((item) => item.code !== active?.code)
      .map((lang, id) => (
        <Item
          key={id}
          onClick={() => updateLang(lang)}
        >
          <Code>{lang?.code}</Code>

          <LangIconWrapper>
            <Image
              fill
              alt={lang.name}
              src={getImage(lang?.image)}
            />
          </LangIconWrapper>
        </Item>
      ));

  useOutSideClick([ref], () => {
    setOpen(false);
  });

  useEffect(() => {
    if (allLanguages?.length) {
      setActive(allLanguages?.find((lang) => lang?.code === language) || null);
    }
  }, [allLanguages]);

  useEffect(() => {
    const blockElement = listRef.current;

    const handleIntersection = (entries: IntersectionObserverEntry[]) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting && entry.intersectionRatio >= 1) {
        } else {
          blockElement?.scrollIntoView({ behavior: 'smooth', block: 'end' });
        }
      });
    };
    const options = {
      root: null,
      threshold: 1,
    };

    const observer = new IntersectionObserver(handleIntersection, options);

    if (blockElement) {
      observer.observe(blockElement);
    }

    return () => {
      if (blockElement) {
        observer.unobserve(blockElement);
      }
    };
  }, [isOpen]);

  return (
    <Wrapper ref={ref}>
      <CurrentLang
        type="button"
        title="language"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="active-lang">{active?.code}</span>
        <LangIconWrapper>
          {active?.name && (
            <Image
              fill
              alt={active?.name || ''}
              src={getImage(active?.image)}
            />
          )}
        </LangIconWrapper>
      </CurrentLang>

      <List
        ref={listRef}
        isTest={isTest}
        className="drop-animated"
        open={isOpen}
      >
        {renderLangs()}
      </List>
    </Wrapper>
  );
};

export default DropDown;
