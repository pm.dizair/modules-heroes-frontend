import { ILanguage } from '@/common/types/location';

export const languages: ILanguage[] = [
  {
    id: 1,
    code: 'en',
    name: 'English',
    image: '/files/images/language/uk.png',
  },
  {
    id: 3,
    code: 'fr',
    name: 'French',
    image: '/files/images/language/france.png',
  },
  {
    id: 4,
    code: 'de',
    name: 'German',
    image: '/files/images/language/germany.png',
  },
  {
    id: 5,
    code: 'ca',
    name: 'English (Canada)',
    image: '/files/images/language/canada.png',
  },
  {
    id: 6,
    code: 'en-au',
    name: 'English (Australian)',
    image: '/files/images/language/australia.png',
  },
];
