import { ILanguage } from '@/common/types/location';

export interface IDropDown {
  language: string;
  allLanguages: ILanguage[];
  isTest?: boolean;
}
export interface IList {
  open: boolean;
  isTest: boolean;
}
