import { withPageProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import { IHomePageProps } from '@/pages';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import DropDown from './DropDown';
import { languages } from './mocks';
import { IDropDown } from './types';

const withProvider = (Story: StoryFn) => {
  return withPageProvider<IHomePageProps>({}, Story);
};

const Primary: StoryObj<IDropDown> = (args: IDropDown) => (
  <DropDown {...args} />
);

Primary.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);

  await step('check first element is first lang', async () => {
    const listBox = await canvas.findByText(languages[0].code);
    expect(listBox).toBeInTheDocument();
  });

  await step('check lang select', async () => {
    localStorage.setItem('language', 'en');

    await sleep(500);

    let listItems = await canvas.findAllByRole('lang');

    let listBox = await canvas.findByRole('listbox');
    expect(listBox.textContent).toBe(languages[0].code);
    await userEvent.click(listItems[0]);

    listBox = await canvas.findByRole('listbox');
    expect(listBox.textContent).toBe(languages[1].code);
    // change lang

    listItems = await canvas.findAllByRole('lang');
    expect(listItems[0].textContent).toBe(languages[0].code);
    // swap with current lang
  });
};

export const Default = {
  ...Primary,
};

const meta: Meta<typeof DropDown> = {
  title: 'UI/Dropdown/Dropdown',
  component: DropDown,
  tags: ['autodocs'],
  decorators: [withProvider],
  args: {
    allLanguages: languages,
    language: 'en',
    isTest: true,
  },
};

export default meta;
