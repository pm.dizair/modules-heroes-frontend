import styled, { css } from 'styled-components';

import { IList } from './types';

export const Wrapper = styled.div`
  position: relative;
  text-transform: uppercase;
  cursor: pointer;
  ${({ theme }) => theme.content.flex.center}
  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    bottom: -100%;
    left: 0;
  }
  @media screen and (min-width: 1025px) {
    &:hover {
      .drop-animated {
        pointer-events: all;
        z-index: 1;
        opacity: 1;
        transform: translate(-50%, calc(100% + 12px));
      }
    }
  }
`;

export const CurrentLang = styled.button`
  ${({ theme }) => theme.content.flex.center}
  min-width: 75px;
  gap: 8px;
  text-transform: uppercase;
  position: relative;
  z-index: 2;
  svg {
    max-width: 24px;
    max-height: 18px;
  }
  @media screen and (min-width: 1025px) {
    transition: all 0.4s linear;
    &:hover {
      .active-lang {
        color: var(--gold);
      }
    }
  }
`;

export const List = styled.ul<IList>`
  list-style: none;
  position: absolute;
  pointer-events: ${(props) => (props.isTest ? 'all' : 'none')};
  bottom: 0;
  perspective: 1000px;
  z-index: 1;
  left: 50%;
  transform: translate(-50%, calc(100% - 14px));
  background-color: #031a34b0;
  backdrop-filter: blur(4px);
  border-radius: 6px;
  overflow: hidden;
  opacity: 0;
  transition: transform 0.4s linear, opacity 0.3s linear;
  @media screen and (max-width: 1024px) {
    ${(props) =>
      props.open &&
      css`
        opacity: 1;
        pointer-events: all;
        transform: translate(-50%, calc(100% + 12px));
      `}
  }
`;

export const Item = styled.li`
  padding: 10px 20px;
  min-width: 100px;
  white-space: nowrap;
  justify-content: space-between;
  text-align: center;
  ${({ theme }) => theme.content.flex.center}
  gap: 8px;
  svg {
    max-width: 24px;
    max-height: 18px;
  }
  @media screen and (min-width: 1025px) {
    transition: all 0.4s linear;
    &:hover {
      color: var(--gold);
      background-color: #031a34b0;
    }
  }
`;

export const LangIconWrapper = styled.div`
  position: relative;
  width: 28px;
  height: 28px;
`;

export const Code = styled.div``;
