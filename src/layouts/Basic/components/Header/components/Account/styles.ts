import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  gap: 16px;
  .deposit-btn {
    margin-left: 16px;
    padding: 10px 35px;
  }
  @media screen and (max-width: 1440px) {
    gap: 18px;
  }
  @media screen and (max-width: 1024px) {
    gap: 12px;
    .deposit-btn {
      margin-left: 12px;
      padding: 7.5px 20px;
    }
  }
  @media screen and (max-width: 540px) {
    .deposit-btn {
      width: 44px;
      margin: 0;
      height: 44px;
      padding: 0;
      img {
        width: fit-content;
        height: 100%;
      }
    }
  }
`;
export const Label = styled.span`
  font-size: 12px;
  line-height: 16px;
  ${({ theme }) =>
    css`
      color: ${theme.colors.gray};
      ${theme.text.overflow}
    `}
`;

export const Score = styled.span`
  font-size: 18px;
  line-height: 18px;
  ${({ theme }) => css`
    color: ${theme.colors.white};
    ${theme.text.overflow}
  `}
`;

export const Balance = styled.div`
  ${({ theme }) => theme.content.flex.column}
  justify-content: center;
  font-weight: 500;
  gap: 2px;
`;

export const User = styled.div`
  height: 100%;
  ${({ theme }) => theme.content.flex.row}
  gap: 12px;
  .logo-account {
    padding: 0;
    width: 44px;
    height: 44px;
    img {
      width: fit-content;
      height: 100%;
    }
  }
  @media screen and (max-width: 1024px) {
    gap: 8px;
    .logo-account {
      width: 36px;
      height: 36px;
    }
  }
`;
