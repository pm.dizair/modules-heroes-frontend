export interface IAccount {
  currency: string;
  userLogo?: string | null;
}
