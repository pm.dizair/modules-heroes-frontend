import { withProvider } from '@/common/helpers/tests/providers/providers';
import '@/styles/scss/index.scss';
import { Meta, StoryObj } from '@storybook/react';

import Account from './Account';

const meta: Meta<typeof Account> = {
  title: 'Common/Layouts/Basic/Header/Account',
  component: Account,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;

export const Base: StoryObj<typeof Account> = {
  args: {
    currency: '€',
    userLogo: '',
  },
};
