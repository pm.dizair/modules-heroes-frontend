import { LINK_TEMPLATES } from '@/common/const';
import { formatNumber } from '@/common/helpers';
import { useTranslations } from '@/common/hooks';
import { Button } from '@/ui-library/buttons';
import {
  CSSLittleMobileOff,
  CSSLittleMobileOn,
} from '@/utils/lib/responsive/breakpoints';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { FC } from 'react';

import imageAccountLogo from '@/assets/icons/header/CreateAccount.svg';
import imageDeposit from '@/assets/icons/header/Score.svg';

import { useUser } from '@/modules/auth/store/user/user.slice';

import { Balance, Label, Score, User, Wrapper } from './styles';
import { IAccount } from './types';

const Account: FC<IAccount> = (props) => {
  const { userLogo, currency } = props;
  const { user } = useUser();
  const { push } = useRouter();
  const t = useTranslations();

  const pushToDeposit = () => {
    push(LINK_TEMPLATES.DEPOSIT());
  };

  return (
    <Wrapper>
      <User
        onClick={() =>
          push(
            LINK_TEMPLATES.PROFILE({
              tab: 'personal-info',
            })
          )
        }
      >
        <CSSLittleMobileOff>
          <Button
            theme="ghost"
            className="logo-account"
            title={'account'}
          >
            <Image
              src={userLogo || imageAccountLogo}
              alt=""
              placeholder="empty"
              sizes="50px"
            />
          </Button>
        </CSSLittleMobileOff>
        <Balance>
          <Label>{t(`header_balance`)}</Label>
          <Score data-testid="user-score">
            {`${currency} ${formatNumber(user?.balance || 0)}`}
          </Score>
        </Balance>
      </User>
      <CSSLittleMobileOff>
        <Button
          onClick={pushToDeposit}
          className="deposit-btn"
        >
          {t('header_deposit')}
        </Button>
      </CSSLittleMobileOff>
      <CSSLittleMobileOn>
        <Button
          title="deposit"
          theme="ghost"
          className="deposit-btn"
          onClick={pushToDeposit}
        >
          <Image
            src={imageDeposit}
            alt=""
            placeholder="empty"
            sizes="50px"
          />
        </Button>
      </CSSLittleMobileOn>
    </Wrapper>
  );
};

export default Account;
