import { DefaultError } from '@/common/components/errors';
import { Button } from '@/ui-library/buttons';
import { MODALS } from '@/utils/lib/modal/const';
import {
  CSSLittleMobileOff,
  CSSTabletOn,
} from '@/utils/lib/responsive/breakpoints';
import { LittleMobileOn } from '@/utils/lib/responsive/responsive';
import { useModal } from '@ebay/nice-modal-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { FC } from 'react';

import { IconSearch } from '@/assets/icons-ts/global';
import imageLogo from '@/assets/icons/Logo.svg';
import ImageBurger from '@/assets/icons/header/Menu.svg';

import { PAGES } from '@/common/const';
import { useLayoutContext } from '@/layouts/Basic/context';
import { useLayout } from '@/layouts/Basic/slice';
import { INavigation } from '@/layouts/Basic/types';
import { Logo, NavGap, NavigationLink, NavigationLinks } from '../../style';
import { INavigationProps } from './types';

const Navigation: FC<INavigationProps> = (props) => {
  const { navigation, handlerMainPagePush } = props;
  const { pathname } = useRouter();
  const { GAMES, SHOP } = PAGES;
  const {
    navigation: { error, isError },
  } = useLayout();

  const { isOpenBurgerMenu, setIsOpenBurgerMenu } = useLayoutContext();
  const { show: showSearch } = useModal(MODALS.SEARCH);

  const renderNavigation = () => {
    return navigation?.map((item: INavigation) => (
      <NavigationLink
        $active={pathname.includes(item.slug)}
        shallow
        href={`/${item.slug}${
          item.slug === GAMES
            ? '?category=popular'
            : item.slug === SHOP
            ? '/bonus_money'
            : ''
        }`}
        key={item.id}
      >
        {item.name}
      </NavigationLink>
    ));
  };

  if (isError) {
    return <DefaultError error={error} />;
  }

  return (
    <NavGap>
      <Button
        className="burger-icon"
        title="menu"
        theme="ghost"
        onClick={() => setIsOpenBurgerMenu(!isOpenBurgerMenu)}
      >
        <Image
          placeholder="empty"
          sizes="50px"
          src={ImageBurger}
          alt=""
        />
      </Button>
      <CSSLittleMobileOff>
        <CSSTabletOn>
          <Button
            theme="ghost"
            className="icon-search"
            onClick={() => showSearch()}
            title="search"
          >
            <IconSearch />
          </Button>
        </CSSTabletOn>
      </CSSLittleMobileOff>
      <NavigationLinks>{renderNavigation()}</NavigationLinks>
      <LittleMobileOn>
        <Logo
          onClick={handlerMainPagePush}
          title="logo"
        >
          <Image
            src={imageLogo}
            alt=""
          />
        </Logo>
      </LittleMobileOn>
    </NavGap>
  );
};

export default Navigation;
