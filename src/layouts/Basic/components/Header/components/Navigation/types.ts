import { IWithError } from '@/common/types';
import { INavigation } from '@/layouts/Basic/types';

export interface INavigationProps extends IWithError {
  navigation: INavigation[];
  handlerMainPagePush: () => void;
}
