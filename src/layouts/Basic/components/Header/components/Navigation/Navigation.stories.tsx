import '@/styles/scss/index.scss';
import { Meta, StoryObj } from '@storybook/react';

import { navigation } from '../../data';
import Navigation from './Navigation';

const meta: Meta<typeof Navigation> = {
  title: 'Common/Layouts/Basic/Header/Navigation',
  component: Navigation,
  tags: ['autodocs'],
};

export default meta;

export const Base: StoryObj<typeof Navigation> = {
  args: {
    navigation,
    handlerMainPagePush: () => {
      alert('main page push');
    },
  },
};
