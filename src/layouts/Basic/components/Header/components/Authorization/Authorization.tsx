import { useTranslations } from '@/common/hooks';
import { Button } from '@/ui-library/buttons';
import { MODALS } from '@/utils/lib/modal/const';
import {
  CSSLittleMobileOff,
  CSSLittleMobileOn,
} from '@/utils/lib/responsive/breakpoints';
import { useModal } from '@ebay/nice-modal-react';
import Image from 'next/image';

import ImageLogIn from '@/assets/icons/header/LogIn.svg';
import ImageCreateAccount from '@/assets/icons/header/UserIcon.svg';

import { Group } from '../../style';

const Authorization = () => {
  const { show: showRegister } = useModal(MODALS.REGISTER);
  const t = useTranslations();

  return (
    <>
      <CSSLittleMobileOff>
        <Group>
          <Button
            data-testid="login-btn"
            onClick={() => showRegister({ modal: MODALS.LOGIN })}
            theme="gray"
            className="login-btn"
          >
            {t('header_log_in')}
          </Button>
          <Button
            data-testid="register-btn"
            onClick={() => showRegister({ modal: MODALS.REGISTER })}
            className="create-btn"
          >
            {t('header_register')}
          </Button>
        </Group>
      </CSSLittleMobileOff>
      <CSSLittleMobileOn>
        <Group>
          <Button
            theme="ghost"
            className="icon-button"
            onClick={() => showRegister({ modal: MODALS.LOGIN })}
            title={'log-in'}
          >
            <Image
              src={ImageLogIn}
              alt=""
            />
          </Button>
          <Button
            className="icon-button"
            onClick={() => showRegister({ modal: MODALS.REGISTER })}
            theme="ghost"
            title={'create-account'}
          >
            <Image
              src={ImageCreateAccount}
              alt=""
            />
          </Button>
        </Group>
      </CSSLittleMobileOn>
    </>
  );
};

export default Authorization;
