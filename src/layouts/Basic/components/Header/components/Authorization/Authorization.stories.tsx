import { withProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { screen, userEvent, within } from '@storybook/testing-library';

import Cookies from 'js-cookie';
import Authorization from './Authorization';

const Component = () => (
  <div style={{ maxWidth: 100 }}>
    <Authorization />
  </div>
);

const meta: Meta<typeof Component> = {
  title: 'Common/Layouts/Basic/Header/Authorization',
  component: Component,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;

export const Base: StoryObj<typeof Component> = {
  args: {
    handlerLogIn: () => {
      alert('log in');
    },
    handlerCreateAccount: () => {
      alert('create account');
    },
  },
};

const checkIsInOrVisible = (value: RegExp = /Welcome back/i) => {
  const welcome = screen.queryByText(value);

  if (welcome) {
    expect(welcome).not.toBeVisible();
  } else {
    expect(welcome).not.toBeInTheDocument();
  }
};

Base.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);
  Cookies.remove('sessionId');

  // login form
  await step(
    'check login modal is opening and redirect to register',
    async () => {
      const login = await canvas.findByTestId('login-btn');

      expect(login).toBeInTheDocument();
      expect(screen.queryByText(/Welcome back/i)).not.toBeInTheDocument();

      await userEvent.click(login);
      await sleep(500);
      expect(screen.getByText(/Welcome back/i)).toBeVisible();

      const registration = screen.getByText(/Registration/i);

      await userEvent.click(registration);
      expect(screen.getByText(/Create your account/i)).toBeVisible();

      const close = await screen.findByTestId('close-btn');
      expect(close).toBeInTheDocument();
      await userEvent.click(close);
      await sleep(500);

      expect(screen.queryByText(/Welcome back/i)).not.toBeInTheDocument();
    }
  );

  // register form
  await step('check register modal is opening', async () => {
    await sleep(1000);
    const register = canvas.getByTestId('register-btn');

    expect(register).toBeInTheDocument();
    expect(screen.queryByText(/Create your account/i)).not.toBeInTheDocument();

    await userEvent.click(register);
    await sleep(500);
    expect(screen.getByText(/Create your account/i)).toBeVisible();

    const loginLink = screen.getByTestId('redirect-link');
    expect(loginLink).toBeVisible();

    await userEvent.click(loginLink);
    await sleep(500);

    expect(screen.getByText(/Welcome back/i)).toBeVisible();
  });

  await step('check login modal is closing', async () => {
    await sleep(1800);
    const close = await screen.findByTestId('close-btn');

    expect(close).toBeInTheDocument();
    await userEvent.click(close);
    await sleep(500);
    checkIsInOrVisible();
  });
};
