import styled, { css } from 'styled-components';

import Link from 'next/link';
import { ILink, IWrapper } from './types';

export const Content = styled.div<IWrapper>`
  padding: 12px 0;
  position: relative;
  &::before {
    content: '';
    position: absolute;
    inset: 0;
    background: linear-gradient(
      180deg,
      #010d1b 30%,
      rgba(1, 13, 27, 0.64) 100%
    );
    transition: all 0.4s ease;

    ${(props) =>
      props.scrolled &&
      css`
        opacity: 0;
      `}
  }
`;

export const Wrapper = styled.header`
  height: 80px;
  position: fixed;
  width: 100%;
  z-index: ${({ theme }) => theme.content.zIndex.header};

  @media screen and (max-width: 1440px) {
    height: 72px;
  }
  @media screen and (max-width: 1024px) {
    height: 64px;
  }
  @media screen and (max-width: 540px) {
    height: 70px;
  }
`;

export const InVision = styled.hr`
  border: 0;
  width: 100%;
  height: 0px;
`;

export const Flex = styled.div`
  ${({ theme }) => theme.content.flex.row}
  justify-content: space-between;
  width: 100%;
  height: 100%;
`;

export const NavGap = styled.div`
  ${({ theme }) => theme.content.flex.row}
  justify-content: flex-start;
  gap: 16px;
  width: calc(50% - 40px);
  .icon-search {
    padding: 0;
  }
  .burger-icon {
    padding: 0;
    width: fit-content;
    height: 44px;
    img {
      width: 100%;
      height: 100%;
    }
  }
  @media screen and (max-width: 1440px) {
    gap: 12px;
    .burger-icon {
      height: 40px;
    }
  }
  @media screen and (max-width: 1024px) {
    gap: 12px;
    .burger-icon {
      height: 36px;
    }
  }
  @media screen and (max-width: 540px) {
    width: 60%;
    .burger-icon {
      height: 44px;
      min-width: 44px;
    }
  }
`;

export const NavigationLinks = styled(NavGap)`
  gap: 24px;
  @media screen and (max-width: 1440px) {
    gap: 20px;
  }
  @media screen and (max-width: 1200px) {
    display: none;
  }
`;

export const NavigationLink = styled(Link)<ILink>`
  font-weight: 600;
  font-size: 18px;
  line-height: 25px;
  ${(props) => props.$active && `color: var(--gold);`}

  @media screen and (max-width: 1440px) {
    font-size: 16px;
    line-height: 22px;
  }
`;

export const UserEvents = styled(NavGap)`
  justify-content: flex-end;
  gap: 18px;

  @media screen and (max-width: 1440px) {
    gap: 10px;
  }
  @media screen and (max-width: 767px) {
    width: calc(50% + 20px);
  }
  @media screen and (max-width: 540px) {
    width: 50%;
  }
`;

export const Events = styled(NavGap)`
  justify-content: flex-end;
  gap: 20px;
  width: 100%;
  .search-button {
    padding: 0;
  }
  @media screen and (max-width: 1440px) {
    gap: 14px;
  }
`;

export const Group = styled.div`
  ${({ theme }) => theme.content.flex.row}
  justify-content: space-between;
  gap: 12px;
  .create-btn,
  .login-btn {
    min-width: 125px;
    max-width: 125px;
  }

  .create-btn {
    padding: 10px 24px;
  }
  .icon-button {
    padding: 0;
  }
  @media screen and (max-width: 1440px) {
    .create-btn {
      padding: 10px 19px;
    }
  }
  @media screen and (max-width: 1024px) {
    gap: 15px;
    .create-btn {
      padding: 7.5px 16px;
    }
  }
  @media screen and (max-width: 540px) {
    .wrapper-button {
      width: fit-content;
      height: 44px;
      img {
        width: 100%;
        height: 100%;
      }
    }
  }
`;

export const Logo = styled.div`
  height: 100%;
  margin: 0 4px;
  cursor: pointer;
  img {
    width: auto;
    height: 100%;
    object-fit: contain;
  }
`;
