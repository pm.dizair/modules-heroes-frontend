import { withProvider } from '@/common/helpers/tests/providers/providers';
import '@/styles/scss/index.scss';
import { Meta } from '@storybook/react';

import Header from './Header';

export const Basic = () => <Header />;

const meta: Meta<typeof Header> = {
  title: 'Common/Layouts/Basic/Header',
  component: Header,
  decorators: [withProvider],
};

export default meta;
