import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { LINK_TEMPLATES } from '@/common/const';
import { useOnScreen } from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { Button } from '@/ui-library/buttons';
import { MODALS } from '@/utils/lib/modal/const';
import { CSSTabletOff } from '@/utils/lib/responsive/breakpoints';
import { LittleMobileOff } from '@/utils/lib/responsive/responsive';
import { useModal } from '@ebay/nice-modal-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { FC, useRef } from 'react';

import { IconSearch } from '@/assets/icons-ts/global';
import imageLogo from '@/assets/icons/Logo.svg';

import { useUser } from '@/modules/auth/store/user/user.slice';

import { useListenNotificationSockets } from '@/common/helpers/socket/useListenNotificationSockets';
import { useLayout } from '../../slice';
import { Account, Authorization, DropDown, Navigation } from './components';
import {
  Content,
  Events,
  Flex,
  InVision,
  Logo,
  UserEvents,
  Wrapper,
} from './style';
import { IHeaderProps } from './types';

const Header: FC<IHeaderProps> = () => {
  const inVisionRef = useRef<HTMLHRElement>(null);
  const isOnScreen = useOnScreen(inVisionRef, '0px');
  const { show: showSearch } = useModal(MODALS.SEARCH);

  const { push, route } = useRouter();
  const { isAuth } = useUser();

  const { allLanguages, language } = useCommon();
  const { navigation } = useLayout();

  const handlerMainPagePush = () => {
    push(LINK_TEMPLATES.HOME(), undefined, { shallow: true });
  };

  const isGames = route === LINK_TEMPLATES.GAME_LISTING();

  useListenNotificationSockets();

  return (
    <>
      <InVision
        id="header-invision"
        ref={inVisionRef}
      />
      <Wrapper>
        <Content scrolled={isOnScreen}>
          <MainContainer>
            <Flex>
              <Navigation
                navigation={navigation.header}
                handlerMainPagePush={handlerMainPagePush}
              />
              <LittleMobileOff>
                <Logo onClick={handlerMainPagePush}>
                  <Image
                    priority
                    placeholder="empty"
                    src={imageLogo}
                    alt=""
                  />
                </Logo>
              </LittleMobileOff>
              <UserEvents>
                <CSSTabletOff>
                  <Events>
                    {!isGames && (
                      <Button
                        theme="ghost"
                        className="search-button"
                        onClick={() => showSearch()}
                        title="search"
                      >
                        <IconSearch />
                      </Button>
                    )}
                    <DropDown
                      allLanguages={allLanguages || []}
                      language={language}
                    />
                  </Events>
                </CSSTabletOff>
                {isAuth ? <Account currency={'€'} /> : <Authorization />}
              </UserEvents>
            </Flex>
          </MainContainer>
        </Content>
      </Wrapper>
    </>
  );
};

export default Header;
