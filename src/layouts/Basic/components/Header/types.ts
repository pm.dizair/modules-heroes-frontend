export interface IHeaderProps {}

export interface IWrapper {
  scrolled: boolean;
}

export interface ILink {
  $active: boolean;
}
