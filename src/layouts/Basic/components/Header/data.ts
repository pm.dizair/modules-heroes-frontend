import { INavigation } from '../../types';

export const navigation: INavigation[] = [
  {
    id: 1,
    name: 'Games',
    keyword: 'games',
    slug: 'games',
  },
  {
    id: 2,
    name: 'Live Casino',
    keyword: 'live-casino',
    slug: 'live-casino',
  },
  {
    id: 3,
    name: 'Shop',
    keyword: 'shop',
    slug: 'shop',
  },
  {
    id: 4,
    name: 'Promotions',
    keyword: 'promotions',
    slug: 'promotions',
  },
  {
    id: 5,
    name: 'Vip Level',
    keyword: 'vip',
    slug: 'vip',
  },
];
