import styled from 'styled-components';

import { IRow, IText } from './types';

export const Wrapper = styled.footer`
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.04),
      rgba(255, 255, 255, 0.04)
    ),
    #010d1b;
  border-top: 1px solid rgba(255, 255, 255, 0.1);
  box-shadow: inset 0px 4px 24px rgba(255, 255, 255, 0.04);
  width: 100%;
  position: relative;
  z-index: 5;
  @media screen and (max-width: 540px) {
    padding-bottom: 80px;
  }
`;

export const RuleFlex = styled.div`
  width: fit-content;
  max-width: 400px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-wrap: wrap;
  gap: 12px;
  @media screen and (max-width: 540px) {
    justify-content: center;
    width: 100%;
  }
`;

export const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const Indent = styled.hr`
  width: 100%;
  border: none;
  height: 1px;
  background: rgba(255, 255, 255, 0.16);
`;

export const Row = styled.div<IRow>`
  padding: 40px 0;
  @media screen and (max-width: 1620px) {
    padding: 32px 0;
  }
  ${(props) => props.padBottom && `padding-bottom: 0 !important;`}
  @media screen and (max-width: 767px) {
    padding: 24px 0;
  }
  @media screen and (max-width: 540px) {
    padding: 16px 0;
  }
`;

export const Navigation = styled.nav`
  display: flex;
  margin: 0 auto;
  justify-content: center;
  flex-wrap: wrap;
  width: 100%;
  column-gap: 4.35%;
  row-gap: 40px;
  color: #ffffff;
  a {
    font-size: 16px;
    line-height: 16px;
    font-weight: 500;
  }
  @media screen and (max-width: 1620px) {
    row-gap: 32px;
    column-gap: 2.9%;
  }
  @media screen and (max-width: 1440px) {
    row-gap: 32px;
    column-gap: 2.35%;
  }
  @media screen and (max-width: 767px) {
    column-gap: 40px;
    row-gap: 16px;
    a {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 540px) {
    column-gap: 24px;
    a {
      font-size: 12px;
    }
  }
`;

export const Rule = styled.img`
  max-width: 100px;
  max-height: 46px;
  object-fit: contain;
  @media screen and (max-width: 767px) {
    max-width: 80px;
    max-height: 40px;
  }
`;

export const Company = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  flex-direction: column;
  align-items: flex-end;
  gap: 32px;
  @media screen and (max-width: 1620px) {
    gap: 24px;
  }
  @media screen and (max-width: 767px) {
    gap: 16px;
  }
`;

export const Logo = styled.button`
  @media screen and (max-width: 767px) {
    img {
      width: 94px;
      height: 32px;
    }
  }
`;

export const Flex = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  gap: 40px;
  @media screen and (max-width: 1620px) {
    gap: 32px;
  }
  @media screen and (max-width: 767px) {
    gap: 24px;
  }
  @media screen and (max-width: 540px) {
    gap: 16px;
  }
`;

export const Text = styled.div<IText>`
  font-weight: 400;
  max-width: 100%;
  font-size: 16px;
  line-height: 24px;
  color: #a3a6b6;
  ${(props) => props.padTop && `padding: 32px 0;`}
  @media screen and (max-width: 1620px) {
    ${(props) => props.padTop && `padding: 32px 0 24px 0;`}
    font-size: 14px;
  }
  @media screen and (max-width: 767px) {
    ${(props) => props.padTop && `padding: 16px 0;`}
    font-size: 12px;
  }
  @media screen and (max-width: 540px) {
    ${(props) => props.padTop && `padding: 16px 0 12px 0;`}
  }
`;
