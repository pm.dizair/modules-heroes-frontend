import { withProvider } from '@/common/helpers/tests/providers/providers';
import '@/styles/scss/index.scss';
import { Meta, StoryObj } from '@storybook/react';

import Footer from './Footer';
import { companyText, footerNavigation, text } from './mocks';

const meta: Meta<typeof Footer> = {
  title: 'Common/Layouts/Basic/Footer',
  component: Footer,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;

export const Basic: StoryObj<typeof Footer> = {
  args: {
    description: text,
    navigation: footerNavigation,
    company: companyText,
  },
};
