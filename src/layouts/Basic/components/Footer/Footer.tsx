import imageLogoMobile from '@/assets/icons/MobileLogo.svg';
import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { DefaultError } from '@/common/components/errors';
import { LINK_TEMPLATES } from '@/common/const';
import { formatTabSlug } from '@/common/helpers';
import { getImage } from '@/common/helpers/getImage';
import { useGetFooterQuery } from '@/utils/api/rtk-query';
import Image from 'next/image';
import Link from 'next/link';
import { FC } from 'react';
import { useLayout } from '../../slice';
import { IIcon, INavigation } from '../../types';
import { companyText, text } from './mocks';
import {
  Company,
  Flex,
  Indent,
  Logo,
  Navigation,
  Row,
  Rule,
  RuleFlex,
  Text,
  Wrapper,
} from './styles';
import { IFooter } from './types';

const Footer: FC<IFooter> = (props) => {
  const { description = text, company = companyText } = props;
  const { footer: footerData, navigation } = useLayout();
  const { footer, isError, error } = navigation || {};

  const renderRules = () =>
    footerData.partnersFooter?.map((rule: IIcon) => (
      <Rule
        key={rule.id}
        src={getImage(rule.image)}
        alt=""
      />
    ));

  const renderNavigation = () =>
    footer?.map((item: INavigation) => (
      <Link
        href={LINK_TEMPLATES.WHITEPAPER({ tab: formatTabSlug(item.slug) })}
        key={item.id}
        shallow
      >
        {item.name}
      </Link>
    ));

  if (isError) {
    return <DefaultError error={error} />;
  }

  return (
    <Wrapper>
      <MainContainer>
        <Row>
          <Flex>
            <Logo>
              <Image
                src={imageLogoMobile}
                alt="site logo"
                blurDataURL="blur"
                placeholder="blur"
              />
            </Logo>
            <Navigation>{renderNavigation()}</Navigation>
          </Flex>
        </Row>
        <Indent />
        <Row padBottom>
          <Company>
            <Text>{description}</Text>
            <RuleFlex>{renderRules()}</RuleFlex>
          </Company>
          <Text padTop>{company}</Text>
        </Row>
      </MainContainer>
    </Wrapper>
  );
};

export default Footer;
