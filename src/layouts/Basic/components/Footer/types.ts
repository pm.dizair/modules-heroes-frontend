import { INavigation } from '../../types';

export interface IFooter {
  navigation?: INavigation[];
  description?: string;
  company?: string;
}
export interface IText {
  padTop?: boolean;
}
export interface IRow {
  padBottom?: boolean;
}
