import { NOTIFICATION } from '../const/notification';

export interface INotificationVariant {
  type: keyof typeof NOTIFICATION;
  status: string;
  value: number;
}

export interface INotificationText {
  type: keyof typeof NOTIFICATION;
  bonus_title: string;
  status: string;
  amount: number;
  currency: string;
  coins: number;
}
