export enum EResultGame {
  BET = 'bet',
  WIN = 'win',
}

export type TPeriodDate = [Date | null | undefined, Date | null | undefined];
export interface IGameID {
  id: number;
  game_id?: string;
  game_key?: string;
  is_bonus_game?: string;
  product?: {
    game_id?: string;
  };
}
export interface IGame extends IGameID {
  brand_name: string;
  image?: string;
  is_free_rounds: boolean;
  is_in_wish_list: boolean;
  name?: string;
  game_image_url?: string;
  game_name?: string;
  provider_name: string;
}

export interface IGameInfo {
  data: {
    image_url: string;
    name: string;
    id: number;
    description: string;
    is_in_wish_list: string;
    is_free_rounds: boolean;
    brand: number;
    is_demo: string;
  };
  bonus_balance: number;
  low_balance: number;
}
export interface IGameEvent {
  id: number;
  updated: Date | string | number;
  parent_id?: number;
  game: IGame;
  brand?: string;
  action?: EResultGame;
  amount: number;
  balance_before: number;
  balance_after: number;
  bet_id: number;
  status: boolean;
}

export interface IProvider {
  id: number;
  name: string;
  status: boolean;
  created: string;
  provider_name: string;
  image: string;
  game_count: number;
}

export interface IBrand {
  name: string;
  id: number;
  image: string;
}

export interface ITag {
  name: string;
}

export interface IFilterHistory {
  dates: TPeriodDate;
  gameByType: string;
}
