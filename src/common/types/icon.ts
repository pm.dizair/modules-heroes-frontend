export interface IIcon {
  fill?: string;
  stroke?: string;
  active?: boolean;
  size?: number;
  className?: string;
}
