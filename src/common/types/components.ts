export interface ITab {
  name: string;
  slug: string;
  image: string;
  mobile_image?: string;
  is_bottom?: boolean;
  position: number;
}
