export interface ITranslation {
  keyword: string;
  text: string;
}
