import { useAppSelector } from '@/common/hooks';
import { ITag } from '@/common/types';
import { ICountry, ICurrency } from '@/common/types/location';
import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { ICommonSliceInitialStateProps } from './types';

const initialState: ICommonSliceInitialStateProps = {
  language:
    typeof document !== 'undefined'
      ? localStorage.getItem('language') || 'en'
      : 'en',
  translations: null,
  socials: null,
  categories: [],
  brands: [],
  tags: [],
  isLoading: true,
  countries: null,
  allLanguages: null,
  paymethods: { isLoading: true, items: [] },
  branch: null,
  blacklistCity: null,
  blacklistCountry: null,
  hasPromotions: false,
};

export const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    updateBrands: (state, action) => {
      state.brands = action.payload;
    },
    updateCategories: (state, action) => {
      state.categories = action.payload.data;
    },
    updatePromotionStatus: (state, action) => {
      state.hasPromotions = action.payload;
      state.isLoading = false;
    },
    updateTranslations: (state, action) => {
      state.translations = action.payload;
    },
    updateLanguage: (state, action) => {
      state.language = action.payload.toLowerCase();
    },
    updateAllLanguages: (state, action) => {
      state.allLanguages = action.payload;
    },
    updateTags: (state, action: PayloadAction<ITag[]>) => {
      state.tags = action.payload;
    },
    updatePaymethods: (state, action) => {
      state.paymethods = action.payload;
    },
    updateCommon: (state, action) => {
      const { countries, blacklistCountry, blacklistCity, socials } =
        action.payload;

      state.countries = countries.map((item: ICountry) => ({
        ...item,
        value: item.id,
        label: item.name as string,
      }));

      state.blacklistCity = blacklistCity;
      state.blacklistCountry = blacklistCountry;
      state.socials = socials;
    },
  },
});

export const useCommon = () =>
  useAppSelector((state) => state[commonSlice.name]);
