import { commonSlice } from './common.slice';

export const {
  updateTranslations,
  updateLanguage,
  updatePaymethods,
  updateAllLanguages,
  updatePromotionStatus,
  updateCategories,
  updateBrands,
  updateTags,
  updateCommon,
} = commonSlice.actions;
