import { IProvider, ITab, ITag } from '@/common/types';
import { IPaymethod } from '@/common/types/deposit';
import { ICountry, ILanguage, ISocials } from '@/common/types/location';
import { ITranslation } from '@/common/types/translations';

export interface ICommonSliceInitialStateProps {
  language: string;
  translations: null | ITranslation[];

  socials: null | ISocials[];
  isLoading: boolean;
  countries: null | ICountry[];
  paymethods: { isLoading: boolean; items: IPaymethod[] };
  branch: null | string;
  categories: ITab[];
  brands: IProvider[];
  tags: ITag[];

  allLanguages: null | ILanguage[];
  blacklistCity: null | unknown;
  blacklistCountry: null | ICountry[];
  hasPromotions: boolean;
}
