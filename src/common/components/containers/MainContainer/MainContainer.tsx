import { FC } from 'react';
import styled from 'styled-components';

import { Wrapper } from './styles';
import { IContainer } from './types';

// TODO: delete this
export const EpicWrapper = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  overflow: auto;
  position: relative;
  z-index: 2;
  ${({ theme }) => theme.content.flex.center};
  ${({ theme }) => theme.text.gothic};

  font-size: 60px;
  @media screen and (max-width: 767px) {
    font-size: 20px;
  }
`;

export const Content = styled.div`
  zoom: 0.9;
`;

const MainContainer: FC<IContainer> = ({
  children,
  className,
  unLimited = false,
}) => (
  <Wrapper
    className={className || 'main-container'}
    unLimited={unLimited}
  >
    {children}
  </Wrapper>
);

export default MainContainer;
