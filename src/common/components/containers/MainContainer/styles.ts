import styled from 'styled-components';

import { IWrapper } from './types';

export const Wrapper = styled.section<IWrapper>`
  display: flex;
  height: 100%;
  position: relative;
  flex-direction: column;
  ${({ theme }) => theme.content.mainContainerWidth}
  ${({ unLimited, theme }) => !unLimited && theme.content.mainContainerPadding}
`;
