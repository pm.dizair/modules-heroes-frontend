import { PropsWithChildren } from 'react';

export interface IWrapper {
  unLimited?: boolean;
}

export interface IContainer extends PropsWithChildren, IWrapper {
  className?: string;
}
