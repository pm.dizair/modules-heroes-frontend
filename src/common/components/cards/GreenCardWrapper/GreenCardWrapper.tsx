import { FC, PropsWithChildren } from 'react';

import { Border, Container, Wrapper } from './styles';

const GreenCardWrapper: FC<PropsWithChildren> = (props) => {
  const { children } = props;
  return (
    <Wrapper>
      <Border />
      <Container>{children}</Container>
    </Wrapper>
  );
};

export default GreenCardWrapper;
