import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  position: relative;
`;
export const Container = styled.div`
  background: radial-gradient(
    50% 50% at 50% 50%,
    #005c62 0%,
    #004c51 45.5%,
    #003d41 69.46%,
    #002d30 100%
  );
  border-radius: 24px;
  width: 100%;
  height: 100%;
  z-index: 2;
  position: relative;
`;

export const Border = styled.div`
  background: radial-gradient(
    50% 775.7% at 50% 50%,
    #f5e58c 18.77%,
    #f5d121 86.01%,
    #e78f03 100%
  );
  border-radius: 24px;
  position: absolute;
  z-index: 1;
  top: -1px;
  left: -1px;
  width: calc(100% + 2px);
  height: calc(100% + 2px);
`;
