import { IGame } from '@/common/types';

export const printGame = (game: IGame) => {
  console.log(
    `game id: ${game.id}  name: ${
      game.name || game.game_name
    } has invalid image`
  );
};
