import '@/styles/scss/index.scss';
import { store } from '@/utils/store';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Provider } from 'react-redux';

import GameCard from './GameCard';
import { mockGame } from './mocks';
import { IGameCardProps } from './types';

const withProvider = (Story: StoryFn) => (
  <Provider store={store}>{<Story />}</Provider>
);

const meta: Meta<typeof GameCard> = {
  title: 'COMPONENTS/Cards/Game',
  component: GameCard,
  tags: ['autodocs'],
  decorators: [withProvider],
  args: {
    game: mockGame,
    fullSize: true,
    isLoading: false,
  },
};

export default meta;

export const CardGame: StoryObj<typeof meta> = (args: IGameCardProps) => (
  <GameCard {...args} />
);

CardGame.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);

  await step('Card Event', async () => {
    const card = canvas.getByTestId('game-card');
    const hover = canvas.getByTestId('hover');

    expect(hover).toHaveStyle('opacity: 0');
    expect(card).toBeInTheDocument();
    // css hover can't be tested from storybook, only with mocking upper level styles
  });
};
