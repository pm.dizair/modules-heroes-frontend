import { NeonSkeleton } from '@/ui-library/loaders';
import { FC, useRef, useState } from 'react';

import Broken from '@/assets/images/placeholders/broken-image.svg';

import { getImage } from '@/common/helpers/getImage';
import { useOutSideClick } from '@/common/hooks';
import Favorite from './Favorite/Favorite';
import GameCardHover from './Hover/Hover';
import { printGame } from './helpers';
import { Aspect, Content, GameImage, Wrapper } from './styles';
import { IGameCardProps } from './types';

const GameCard: FC<IGameCardProps> = (props) => {
  const {
    game,
    fullSize = false,
    customCovered,
    disableAction = false,
    onClick,
  } = props;
  const [isLoaded, setLoaded] = useState<boolean>(true);
  const [open, setOpen] = useState<boolean>(false);
  const [currentSrc, setCurrentSrc] = useState<string>(
    getImage(game?.image) || getImage(game?.game_image_url) || Broken.src
  );
  const ref = useRef<HTMLDivElement>(null);

  useOutSideClick([ref], () => setOpen(false));

  const handlerLike = () => {
    // TODO: api event
    console.log('like');
  };

  return (
    <Wrapper
      open={open}
      ref={ref}
      load={false}
      full={fullSize}
      data-testid="game-card"
      className="game-card"
      onClick={() => setOpen(!open)}
    >
      {!disableAction && (
        <GameCardHover
          item={game}
          onClick={onClick}
        />
      )}
      <Aspect customCovered={customCovered}>
        <Content isLoading={false}>
          <GameImage
            quality={100}
            src={currentSrc}
            loader={() => currentSrc}
            fill
            alt={game?.name || 'game name'}
            onLoad={() => setLoaded(false)}
            onError={() => {
              printGame(game);
              setCurrentSrc(Broken.src);
            }}
          />
          {isLoaded && <NeonSkeleton fill />}
        </Content>
      </Aspect>
      {!isLoaded && !disableAction && <Favorite event={handlerLike} />}
    </Wrapper>
  );
};

export default GameCard;
