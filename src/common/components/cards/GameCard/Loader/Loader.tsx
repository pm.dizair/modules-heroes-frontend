import { NeonSkeleton } from '@/ui-library/loaders';
import { FC } from 'react';

import { Aspect, Content, Wrapper } from '../styles';
import { IGameCardLoaderProps } from '../types';

const GameCardLoader: FC<IGameCardLoaderProps> = (props) => {
  const { fullSize = false, customCovered } = props;
  return (
    <Wrapper
      load
      full={fullSize}
    >
      <Aspect customCovered={customCovered}>
        <Content isLoading>
          <NeonSkeleton fill />
        </Content>
      </Aspect>
    </Wrapper>
  );
};

export default GameCardLoader;
