import { IGame } from '@/common/types';

export interface IGameCardLoaderProps {
  fullSize?: boolean;
  customCovered?: number;
  disableAction?: boolean;
  onClick?: () => void;
}
export interface IGameCardProps extends IGameCardLoaderProps {
  game: IGame;
  isLoading?: boolean;
}

export interface IWrapper {
  full: boolean;
  load: boolean;
  open?: boolean;
}

export interface IContentLoading {
  isLoading: boolean;
}
export interface ICustomAspect {
  customCovered: number | undefined;
}
