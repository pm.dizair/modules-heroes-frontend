import { FC, memo, MouseEventHandler, useState } from 'react';

import IconLikeActive from '@/assets/icons/likes/like-active.svg';
import IconLikeDefault from '@/assets/icons/likes/like-default.svg';
import IconLikeHover from '@/assets/icons/likes/like-hover.svg';

import { LikeImage, Relative, Wrapper } from './styles';
import { IFavorite } from './types';

const Favorite: FC<IFavorite> = (props) => {
  const { event, $active = false } = props;
  const [like, setLike] = useState<boolean>($active);

  const handlerLike: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.stopPropagation();
    setLike(!like);
    if (typeof event === 'function') {
      event();
    }
  };

  return (
    <Wrapper
      type="button"
      onClick={handlerLike}
      className="like-wrapper"
      data-testid="like"
    >
      <Relative
        data-testid="container"
        $active={like}
      >
        <LikeImage
          src={IconLikeActive}
          alt="like-active"
          className="active"
        />
        <LikeImage
          src={IconLikeHover}
          alt="like-hover"
          className="hover"
        />
        <LikeImage
          src={IconLikeDefault}
          alt="like-default"
          className="default"
        />
      </Relative>
    </Wrapper>
  );
};

export default memo(Favorite);
