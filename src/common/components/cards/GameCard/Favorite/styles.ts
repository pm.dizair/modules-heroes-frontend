import Image from 'next/image';
import styled, { css } from 'styled-components';

import { IFavoriteActive } from './types';

export const Relative = styled.div<IFavoriteActive>`
  width: 100%;
  height: 100%;
  position: relative;
  transition: all 0.8s ease;
  transform-origin: center;
  z-index: 2;
  @supports (filter: drop-shadow(0px 2px 10px rgba(0, 0, 0, 0.6))) {
    filter: drop-shadow(0px 2px 10px rgba(0, 0, 0, 0.6));
  }

  .active {
    opacity: 0;
    z-index: 3;
  }
  .hover {
    opacity: 0;
    z-index: 2;
  }
  .default {
    opacity: 1;
    z-index: 1;
  }
  @media screen and (min-width: 1024px) {
    &:active {
      transform: scale(0.85);
    }
    &:hover {
      .active {
        opacity: ${({ $active }) => ($active ? 1 : 0)};
      }
      .hover {
        opacity: 1;
      }
      .default {
        opacity: 0;
      }
    }
  }

  ${({ $active }) =>
    $active &&
    css`
      .active {
        opacity: 1;
      }
      .hover {
        opacity: 1;
      }
      .default {
        opacity: 0;
      }
    `}
`;

export const LikeImage = styled(Image)`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  transition: all 0.8s ease;
`;

export const Wrapper = styled.button`
  position: absolute;
  right: 8px;
  bottom: 12px;
  width: 36px;
  height: 26px;
  z-index: 4;
  @media screen and (max-width: 540px) {
    right: 8px;
    bottom: 8px;
    width: 36px;
    height: 34px;
  }
`;
