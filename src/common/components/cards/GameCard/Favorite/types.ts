export interface IFavoriteActive {
  $active?: boolean;
}

export interface IFavorite extends IFavoriteActive {
  event?: () => void;
}
