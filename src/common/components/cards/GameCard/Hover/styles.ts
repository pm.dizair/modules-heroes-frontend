import styled from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.center}
  flex-direction: column;
  height: 100%;
  width: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 3;
  background: rgba(1, 13, 27, 0.7);
  backdrop-filter: blur(4px);
  opacity: 0;
  transition: all 0.4s linear;
  pointer-events: none;
  border-radius: 12px;
  .decoration-bottom {
    bottom: -10px;
    transform: translateY(100%);
  }
  .decoration-top {
    top: 0;
    transform: translateY(-100%);
  }
`;

export const ButtonsWrapper = styled.div`
  ${({ theme }) => theme.content.flex.center}
  flex-direction: column;
  gap: 16px;
  position: relative;
  z-index: 2;
  width: 100%;
  .color-button {
    max-width: 140px;
    padding-top: 8px;
    padding-bottom: 8px;
  }
  @media screen and (max-width: 1440px) {
    gap: 12px;
    .color-button {
      max-width: 120px;
      padding-top: 6px;
      padding-bottom: 6px;
    }
  }
`;

export const JungleDecoration = styled.div`
  position: absolute;
  width: 100%;
  left: 0;
  right: 0;
  height: 80px;
  transition: all 0.8s ease;
  z-index: 2;
  img {
    object-fit: cover;
  }
  @media screen and (max-width: 1440px) {
    height: 60px;
  }
  @media screen and (max-width: 767px) {
    height: 70px;
  }
`;
