import { IGame } from '@/common/types';

export interface IHover {
  item: IGame;
  onClick?: () => void;
}
