import { Button } from '@/ui-library/buttons';
import Image from 'next/image';

import BottomDecoration from '@/assets/images/decoration/CardHoverBottom.webp';
import TopDecoration from '@/assets/images/decoration/CardHoverTop.webp';

import { useTranslations } from '@/common/hooks';
import { useGamePlay } from '@/common/hooks/useGamePlay/useGamePlay';
import { FC } from 'react';
import { ButtonsWrapper, JungleDecoration, Wrapper } from './styles';
import { IHover } from './types';

const GameCardHover: FC<IHover> = (props) => {
  const { item, onClick } = props;
  const { onPlayDemo, onPlayReal } = useGamePlay(item);
  const t = useTranslations();

  return (
    <Wrapper
      data-testid="hover"
      className="decoration-wrapper"
    >
      <JungleDecoration className="decoration-top">
        <Image
          alt="decoration top"
          src={TopDecoration}
          fill
          priority
        />
      </JungleDecoration>
      <ButtonsWrapper>
        <Button
          fullWidth
          onClick={(e) => {
            e.stopPropagation();
            if (typeof onClick === 'function') {
              onClick();
            }
            onPlayReal();
          }}
        >
          {t('game_detail_button_real')}
        </Button>
        <Button
          fullWidth
          theme="gray"
          onClick={(e) => {
            e.stopPropagation();
            if (typeof onClick === 'function') {
              onClick();
            }
            onPlayDemo();
          }}
        >
          {t('game_detail_button_demo')}
        </Button>
      </ButtonsWrapper>
      <JungleDecoration className="decoration-bottom">
        <Image
          alt="decoration bottom"
          src={BottomDecoration}
          fill
          priority
        />
      </JungleDecoration>
    </Wrapper>
  );
};

export default GameCardHover;
