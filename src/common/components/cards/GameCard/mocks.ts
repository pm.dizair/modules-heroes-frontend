import { IGame } from '@/common/types/game';

export const mockGame: IGame = {
  id: 3,
  image: 'https://static.slotomatic.net/imagesv2/100cats.jpg',
  name: '100 Cats',
  brand_name: 'egtjackpot',
  is_free_rounds: true,
  is_in_wish_list: true,
  provider_name: 'provider',
};
