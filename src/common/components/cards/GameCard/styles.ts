import Image from 'next/image';
import styled, { css } from 'styled-components';

import { IContentLoading, ICustomAspect, IWrapper } from './types';

const activeStyles = css`
  .decoration-bottom {
    transform: translateY(0%);
  }
  .decoration-top {
    transform: translateY(0%);
  }
  .decoration-wrapper {
    pointer-events: all;
    opacity: 1;
  }
`;

export const Wrapper = styled.div<IWrapper>`
  border: 1px solid var(--gold);
  border-radius: 12px;
  overflow-x: hidden;
  overflow-y: hidden;
  width: 100%;
  position: relative;
  cursor: pointer;
  z-index: 2;
  // right down shadow
  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    right: 0;
    width: 100%;
    aspect-ratio: 1 / 1;
    height: auto;
    z-index: 1;
    transform: translate(50%, 50%) rotate(45deg);
    background: linear-gradient(
      270deg,
      rgba(0, 0, 0, 0.9) 30%,
      rgba(255, 255, 255, 0) 100%
    );
  }

  ${({ full }) =>
    full &&
    css`
      max-width: 255px;
      max-height: 382.5px;
      @media screen and (max-width: 1440px) {
        max-width: 187px;
        max-height: 283px;
      }
      @media screen and (max-width: 767px) {
        max-width: 160px;
        max-height: 242px;
      }
    `}
  transition: all 0.4s ease-in;
  @media screen and (min-width: 1025px) {
    &:hover {
      ${activeStyles}
    }
  }
  @media screen and (max-width: 1024px) {
    ${({ open }) => open && activeStyles}
  }
`;

export const GameImage = styled(Image)`
  object-fit: cover;
  width: 100%;
  height: 100%;
  transition: all 0.3s ease;
  border-radius: 12px;
`;

export const Content = styled.div<IContentLoading>`
  position: absolute;
  inset: 0;
  z-index: ${({ isLoading }) => (isLoading ? 4 : 1)};
  span {
    height: 100%;
  }
`;
export const Aspect = styled.div<ICustomAspect>`
  height: 0;
  border: none;
  padding-top: ${({ customCovered }) =>
    customCovered ? `${customCovered}%` : '151%'};
`;
