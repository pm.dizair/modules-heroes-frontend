import { IGame } from '@/common/types';

import { NAutoload } from '../../autoloader/autoload/v4.0/types';

export interface IAutoLoadGameCard {
  item: IGame;
  observerParams: NAutoload.IObserverParams;
}
