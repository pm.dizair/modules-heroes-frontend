import { GameCard } from "@/common/components/cards";
import { FC } from "react";

import { ItemObserverV4 } from "../../autoloader/autoload/v4.0/modules/ItemObserver";
import { IAutoLoadGameCard } from "./types";

const AutoLoadGameCard: FC<IAutoLoadGameCard> = ({
  item,
  observerParams,
  ...props
}) => {
  const [isLiked, setIsLiked] = useState<boolean>(
    Boolean(item?.is_in_wish_list)
  );

  return (
    <ItemObserverV4 {...observerParams}>
      <GameCard
        {...props}
        setIsLiked={setIsLiked}
        game={{
          ...item,
          is_in_wish_list: isLiked,
        }}
      />
    </ItemObserverV4>
  );
};

export default AutoLoadGameCard;
