import React from 'react';

import { Icon, Loader } from './style';

const CircleLoader = () => {
  return (
    <Loader>
      <Icon />
    </Loader>
  );
};

export default CircleLoader;
