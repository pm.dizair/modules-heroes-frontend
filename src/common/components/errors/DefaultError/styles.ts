import styled from "styled-components";

export const Title = styled.div`
  color: #fff;
  font-size: 30px;
`;

export const Wrapper = styled.div`
  padding: 30px;
  width: 100%;
  ${({ theme }) => theme.content.flex.center};
  height: 300px;
`;
