import { ERRORS } from '@/common/const/errors';
import { FC } from 'react';

import { Title, Wrapper } from './styles';
import { IDefaultErrorProps } from './types';

const DefaultError: FC<IDefaultErrorProps> = (props) => {
  const { error = ERRORS.DEFAULT } = props;

  return (
    <Wrapper>
      <Title>{error}</Title>
    </Wrapper>
  );
};

export default DefaultError;
