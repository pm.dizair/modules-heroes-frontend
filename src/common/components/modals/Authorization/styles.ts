import Image from 'next/image';
import styled from 'styled-components';

import { ICharacterMode } from './types';

export const AnimationWrap = styled.div<ICharacterMode>`
  z-index: 1;
  padding: 1px;
  width: 100%;
  max-width: ${(props) => (props.character ? '1200px' : '518px')};
  height: fit-content;
  border-radius: 16px;
  transition: all 0.3s;
  background: linear-gradient(270deg, #4e5660 0%, #fbe13a 53.08%, #4e5660 100%);
  &.open {
    animation: opening 0.5s alternate;
    animation-fill-mode: forwards;
    transform-origin: center;
  }
  &.close {
    animation: closing 0.3s alternate;
    animation-fill-mode: forwards;
    transform-origin: center;
  }
  @keyframes opening {
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
    }
  }
  @keyframes closing {
    0% {
      transform: scale(1);
      opacity: 1;
    }
    100% {
      transform: scale(0);
      opacity: 0;
    }
  }
  @media screen and (max-width: 1280px) {
    max-width: ${(props) => (props.character ? '900px' : '518px')};
  }
  @media screen and (max-width: 1024px) {
    max-width: 518px;
  }
  @media screen and (max-width: 600px) {
    height: 100%;
    max-width: 100%;
    border-radius: 0;
    padding: 0px;
  }
`;
export const Container = styled.div`
  height: 100%;
  width: 100%;
  position: relative;
  background: #010d1b;
  border-radius: 16px;
  overflow: hidden;
  @media screen and (max-width: 600px) {
    border-radius: 0;
    overflow-y: auto;
    height: 100%;
    ::-webkit-scrollbar {
      width: 0px;
    }
  }
`;

export const FormWrapper = styled.div<ICharacterMode>`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  max-width: 516px;
  overflow: hidden;
  padding: 40px;
  margin-left: auto;
  height: ${(props) => (props.character ? '782px' : 'auto')};

  @media screen and (max-width: 1024px) {
    height: auto;
  }
  @media screen and (max-width: 767px) {
    max-width: none;
  }
  @media screen and (max-width: 600px) {
    justify-content: flex-start;
    padding: 32px 16px 24px 16px;
  }
`;

export const Background = styled(Image)`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 0;
  object-fit: contain;
  object-position: left center;
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;
export const Logo = styled(Background)`
  top: 32px;
  left: 270px;
  z-index: 1;
  height: 44px;
  width: auto;
  @media screen and (max-width: 1280px) {
    left: 150px;
  }
`;
export const Character = styled(Image)`
  position: absolute;
  height: 550px;
  width: auto;
  bottom: 50px;
  left: 80px;
  z-index: 0;
  @media screen and (max-width: 1280px) {
    height: 500px;
    left: 0px;
  }
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;
export const FlexSB = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 8px 24px 32px 24px;
  z-index: 1;
`;

export const BtnContainer = styled.div`
  width: 100%;
  padding-top: 16px;
  button {
    width: 100%;
  }
`;

export const WrapAgree = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: 0 0 20px 0;
  width: 100%;
  font-style: normal;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  font-family: var(--font-nunito);
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  gap: 10px;
  color: #fff;
  a {
    font-family: var(--font-nunito);
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    color: #ffd600;
    white-space: pre-wrap;
  }
  @supports (-webkit-touch-callout: none) {
    font-weight: 500;
    font-size: 15px;
    a {
      font-weight: 500;
      font-size: 15px;
    }
  }
`;
export const Terms = styled.div`
  word-break: break-word;
`;

export const Hr = styled.div`
  background: rgba(255, 255, 255, 0.16);
  height: 1px;
  min-height: 1px;
  width: 100%;
  margin: 24px 0;
  z-index: 1;
`;
export const SocialBtnWrap = styled.div`
  box-shadow: 2px 3px 12px rgba(0, 0, 0, 0.6),
    inset 0px 0px 6px rgba(255, 255, 255, 0.3);
  position: relative;
  overflow: hidden;
  cursor: pointer;
  padding: 1px;
  border-radius: 12px;

  &::after {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 250px;
    height: 250px;
    content: '';
    z-index: 0;
  }
  &:hover {
    background: #29333f;
    box-shadow: 2px 3px 12px rgba(0, 0, 0, 0.6),
      inset 0px 0px 6px rgba(255, 255, 255, 0.15);
    &::after {
      animation: border 3s linear;
      animation-iteration-count: infinite;
      background: linear-gradient(
        270deg,
        #4e5660 20%,
        #fbe13a 53.08%,
        #4e5660 80%
      );
    }
  }
  @keyframes border {
    0% {
      transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
      transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
      transform: translate(-50%, -50%) rotate(360deg);
    }
  }
`;
export const SocialBtn = styled.div`
  position: relative;
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 400;
  font-size: 16px;
  border-radius: 12px;
  background-color: #0b1624;
  color: var(--gray);
  padding: 9px 16px;
  transition: 0.4s;
  width: 100%;
  height: 100%;
  font-family: var(--font-nunito);
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;

  & > span {
    position: absolute;
    opacity: 0;
    width: 100%;
    height: 100%;
    & > button {
      width: 100%;
      height: 100%;
      background-color: transparent;
      border: none;
    }
  }
  & > svg,
  img {
    margin-right: 10px;
    width: 26px;
    height: 26px;
  }
  & > div {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
  @supports (-webkit-touch-callout: none) {
    font-weight: 500;
  }
`;
export const Row = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;
  justify-content: space-between;
  gap: 16px;
  z-index: 1;
  & > div {
    width: calc(50% - 8px);
    position: relative;
    & > span {
      position: absolute;
      width: 100%;
      height: 100%;
      & > button {
        width: 100%;
        height: 100%;
        background: transparent;
        border: none;
      }
    }
  }
  @media screen and (max-width: 600px) {
    gap: 10px;
    & > div {
      width: calc(50% - 5px);
    }
  }
`;

export const RestoreBtn = styled.button`
  font-family: var(--font-nunito);

  font-weight: 700;
  font-size: 16px;
  line-height: 20px;

  display: flex;
  align-items: center;

  color: var(--gray);
  margin: 12px 0 16px 0px;
  &:hover {
    text-decoration: underline;
  }
  @media screen and (max-width: 600px) {
    margin: 8px 0 16px 0px;
  }
`;
