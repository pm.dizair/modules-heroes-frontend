export const MODES = {
  CHARACTER: 'CHARACTER',
  NO_CHARACTER: 'NO_CHARACTER',
};

export type MODE = keyof typeof MODES;
