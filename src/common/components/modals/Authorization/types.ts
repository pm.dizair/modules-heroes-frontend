import { IModal } from '../types';

export interface IAnimation {
  hide: () => void;
  modal: string;
  gradientOff: () => void;
  mode: string;
}
export interface ICharacterMode {
  character?: boolean;
}
export interface IRegistrationModal extends IModal {
  mode: string;
  modal: string;
}
