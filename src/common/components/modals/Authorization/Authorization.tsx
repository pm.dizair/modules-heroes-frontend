import { fontGotisch, fontNunito } from '@/pages/_app';
import { create, useModal } from '@ebay/nice-modal-react';
import { FC, useRef } from 'react';
import Modal from 'react-modal';

import { Wrapper } from '../styles';
import Content from './components/Content/Content';
import { MODES } from './const';
import { RegistrationProvider } from './context';
import { IRegistrationModal } from './types';

const HIGHTEST_Z_INDEX = 10001;

const RegistrationModal: FC<IRegistrationModal> = create((props) => {
  const { id, mode = MODES.NO_CHARACTER, modal } = props;
  const ref = useRef<HTMLDivElement>(null);
  const { hide, visible } = useModal(id);
  const gradientOff = () => {
    if (ref.current) {
      ref.current.classList.remove('open');
      ref.current.classList.toggle('close');
    }
  };

  return (
    <Modal
      closeTimeoutMS={500}
      isOpen={visible}
      style={{ overlay: { zIndex: HIGHTEST_Z_INDEX } }}
      ariaHideApp={false}
      shouldCloseOnOverlayClick={true}
    >
      <RegistrationProvider>
        <Wrapper
          ref={ref}
          isOpen={visible}
          className={`${fontGotisch.variable} ${fontNunito.variable} open`}
        >
          <Content
            hide={hide}
            gradientOff={gradientOff}
            modal={modal}
            mode={mode}
          />
        </Wrapper>
      </RegistrationProvider>
    </Modal>
  );
});

export default RegistrationModal;
