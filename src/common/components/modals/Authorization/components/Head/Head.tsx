import { HeadWrap, Text, Title } from '@/common/components/modals/styles';
import { useTranslations } from '@/common/hooks';
import React, { FC } from 'react';

import { useRegistrationContext } from '../../context';
import { IHead } from './types';

const Head: FC<IHead> = (props) => {
  const { regMethod } = props;
  const { isRegister, setIsRegister } = useRegistrationContext();
  const t = useTranslations();

  return (
    <HeadWrap offset={regMethod}>
      <Title>
        {isRegister ? t('registration_form_title') : t('login_form_title')}
      </Title>
      <Text>
        {isRegister
          ? t('registration_form_sub_title')
          : t('login_form_sub_title')}

        <span
          data-testid="redirect-link"
          onClick={() => setIsRegister(!isRegister)}
        >
          {isRegister
            ? t('registration_form_sub_title_link')
            : t('login_form_sub_title_link')}
        </span>
      </Text>
    </HeadWrap>
  );
};
export default Head;
