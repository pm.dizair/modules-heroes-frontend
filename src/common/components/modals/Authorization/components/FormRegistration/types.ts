import { ICountry, ICurrency } from '@/common/types/location';
import { IHero } from '@/common/types/user';

export interface IFormValue {
  country: ICountry;
  currency: ICurrency;
  email: string;
  policy: boolean;
  promoCode: boolean;
}

export interface IFacebookRegistrationResponse {
  id: string;
  userID: string;
  accessToken: string;
  name?: string | undefined;
  email?: string | undefined;
  picture?:
    | {
        data: {
          height?: number | undefined;
          is_silhouette?: boolean | undefined;
          url?: string | undefined;
          width?: number | undefined;
        };
      }
    | undefined;
}

export interface IRegistation {
  hero?: IHero;
}
