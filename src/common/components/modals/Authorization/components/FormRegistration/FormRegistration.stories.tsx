import { withProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import FormRegistration from './FormRegistration';
import { mockIpResponse, mockRegisterResponse } from './mock';

export const Base: StoryObj<typeof FormRegistration> = {};

Base.play = async ({ canvasElement, step }) => {
  await step('check error validation', async () => {
    const canvas = within(canvasElement);
    const button = canvas.getByTestId('color-button');

    await sleep(600);
    expect(button).toBeVisible();

    userEvent.click(button);
    await sleep(600);

    userEvent.click(button);
    expect(canvas.getAllByTestId('form-error').length).toBeGreaterThan(2);
  });

  await step('type valid credentials', async () => {
    const canvas = within(canvasElement);
    const [emailInput, usernameInput, passwordInput] =
      canvas.getAllByTestId('form-input');

    const button = canvas.getByTestId('color-button');
    const [checkbox] = canvas.getAllByTestId('checkbox');

    await userEvent.type(emailInput, 'tester@casino.com');
    await userEvent.type(usernameInput, 'Tester1234567');
    await userEvent.type(passwordInput, 'Tester1234567');
    await userEvent.click(checkbox);

    userEvent.click(button);
    await sleep(600);

    userEvent.click(button);
    expect(canvas.queryAllByTestId('form-error').length).toBeLessThanOrEqual(2);
  });

  await step('check auto-select country currency', async () => {
    await sleep(1200);
    const canvas = within(canvasElement);

    const input = await canvas.findByText('Uganda');
    await userEvent.click(input);

    expect(input).toBeInTheDocument();
    const uk = await canvas.findByText(/United kingdom/i); // we need uganda to find uk because select uses virtual list

    const eur = await canvas.findByText(/eur/i);
    expect(eur).toBeVisible();

    await userEvent.click(uk);

    const gbp = await canvas.findByText(/gbp/i);
    expect(gbp).toBeVisible();
  });
};

const meta: Meta<typeof FormRegistration> = {
  title: 'Modules/Auth/FormRegistration',
  component: FormRegistration,
  tags: ['autodocs'],
  decorators: [withProvider],
  parameters: {
    mockData: [
      {
        url: 'https://api2.dizair.ee/auth/location-by-ip-address/',
        method: 'GET',
        status: 200,
        response: mockIpResponse,
      },
      {
        url: 'https://api2.dizair.ee/auth/register/',
        method: 'POST',
        status: 200,
        response: mockRegisterResponse,
      },
    ],
  },
};

export default meta;
