import styled from 'styled-components';

export const BtnContainer = styled.div`
  width: 100%;
  padding-top: 16px;
  button {
    width: 100%;
  }
`;
