import { ICurrency } from '@/common/types/location';
import { Select } from '@/ui-library/inputs';
import React, { FC, memo, useEffect, useState } from 'react';

import { IconCurrency } from '@/assets/icons-ts/form';

import { ICurrencySelect } from './types';

const CurrencySelect: FC<ICurrencySelect> = (props) => {
  const { currencies, country } = props;
  const [_, setValue] = useState<boolean>(false);

  useEffect(() => {
    setValue((prev) => !prev);
  }, [country]);

  return (
    <Select<ICurrency>
      name="currency"
      icon={<IconCurrency />}
      options={currencies}
      isSearchable={false}
      defaultValue={currencies[0]}
      placeholder="registration_form_currency"
      rules={{ required: true }}
    />
  );
};

export default memo(CurrencySelect);
