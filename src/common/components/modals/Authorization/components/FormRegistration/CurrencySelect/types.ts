import { ICountry, ICurrency } from '@/common/types/location';

export interface ICurrencySelect {
  country: ICountry;
  currencies: ICurrency[];
}
