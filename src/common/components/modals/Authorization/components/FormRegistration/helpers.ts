import { ICountry } from '@/common/types/location';

export const useCurrencySelect = () => {
  const getDefOptions = (country: ICountry) => {
    if (country) {
      return country.currency;
    }
    return [
      {
        value: 1,
        label: 'EUR',
      },
    ];
  };

  const getDefCurrency = (country: ICountry) => {
    if (country) {
      return country.currency[0];
    }
    return {
      value: 1,
      label: 'EUR',
    };
  };

  return { getDefOptions, getDefCurrency };
};
