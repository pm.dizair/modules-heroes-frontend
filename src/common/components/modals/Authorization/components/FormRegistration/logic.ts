import { LINK_TEMPLATES } from '@/common/const';
import { useTranslations, useWindowScroll } from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { FormError } from '@/common/types';
import { MODALS } from '@/utils/lib/modal/const';
import { useModal } from '@ebay/nice-modal-react';
import { useGoogleLogin } from '@react-oauth/google';
import { AxiosError } from 'axios';
import { useRouter } from 'next/router';
import { useSnackbar } from 'notistack';
import { useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';

import { useCallAuth } from '@/modules/auth/api/helpers/useCallAuth';
import { useCallRefreshUser } from '@/modules/auth/api/helpers/useCallRefreshUser';
import {
  useCheckFacebookSignInMutation,
  useCheckGoogleSignInMutation,
} from '@/modules/auth/api/rtk-query/auth/auth';
import { useUser } from '@/modules/auth/store/user/user.slice';

import { IHero } from '@/common/types/user';
import { useAxiosAuthHandler } from '@/modules/auth/api/helpers/useAxiosAuthHandler';
import { IFacebookRegistrationResponse, IFormValue } from './types';

export const useRegistrationFormLogic = (hero?: IHero) => {
  const { scrollToTop } = useWindowScroll();

  const [regMethod, setRegMethod] = useState<unknown | null>(null);
  const [emailInput, setEmailInput] = useState<string>('');
  const [usernameInput, setUsernameInput] = useState<string>('');
  const [passwordInput, setPasswordInput] = useState<string>('');
  const [isFacebook, setIsFacebook] = useState<boolean>(false);
  const { hide } = useModal(MODALS.REGISTER);

  const t = useTranslations();
  const { enqueueSnackbar } = useSnackbar();

  const { language, countries } = useCommon();
  const { isAuth, ip } = useUser();
  const router = useRouter();
  const { show: showLogin } = useModal(MODALS.LOGIN);

  const methods = useForm<IFormValue>({ mode: 'onSubmit' });
  const { handleSubmit, reset, setError, watch } = methods;
  const country = watch('country');

  const [checkFacebookSignIn] = useCheckFacebookSignInMutation();
  const [checkGoogleSignIn] = useCheckGoogleSignInMutation();

  const [register, { isError: isRegisterError, error: registerError }] =
    useAxiosAuthHandler('auth/register/');
  const [facebookSignIn] = useAxiosAuthHandler('auth/facebook-sign-in/');
  const [googleSignIn] = useAxiosAuthHandler('auth/google-sign-in/');

  const loginUserHandler = useCallRefreshUser();
  const signInFacebookHandler = useCallAuth(facebookSignIn, loginUserHandler);
  const signInGoogleHandler = useCallAuth(googleSignIn, loginUserHandler);
  const registerHandler = useCallAuth(register, loginUserHandler);

  const defCountry = useMemo(
    () => countries?.find(({ iso }) => iso === ip?.country),
    [countries, ip]
  );

  const getItemOrSetUndefined = () => {
    return localStorage.getItem('stag') || undefined;
  };

  const onClickLogin = () => {
    showLogin();
    setRegMethod(null);
  };

  const close = () => {
    hide();
    reset();
  };

  const googleLogin = useGoogleLogin({
    onSuccess: async (response) => {
      const data = await checkGoogleSignIn(response);

      if ('error' in data) {
        enqueueSnackbar(t('registration_user_exist_notification'), {
          variant: 'error',
        });
      } else {
        setIsFacebook(false);
        setRegMethod(response);
      }
    },
  });

  const facebookLogin = async (response: IFacebookRegistrationResponse) => {
    const data = await checkFacebookSignIn(response);

    if ('error' in data) {
      enqueueSnackbar(t('registration_user_exist_notification'), {
        variant: 'error',
      });
    } else {
      setIsFacebook(true);
      setRegMethod(response);
    }
  };

  const onSubmit = async (data: IFormValue) => {
    if (regMethod) {
      const params = {
        ...regMethod,
        language: language || localStorage.getItem('language'),
        country: data.country.id,
        currency: data.currency.value,
        stag: getItemOrSetUndefined(),
        registration_url: getItemOrSetUndefined(),
        hero_id: hero?.id,
      };

      if (isFacebook) signInFacebookHandler(params);
      else signInGoogleHandler(params);
    } else {
      try {
        await registerHandler({
          ...data,
          language: language || localStorage.getItem('language'),
          currency: data.currency.value,
          country: data.country.id,
          stag: getItemOrSetUndefined(),
          registration_url: getItemOrSetUndefined(),
          hero_id: hero?.id,
        });
        scrollToTop();
      } catch (e: unknown) {
        if (e instanceof AxiosError) {
          try {
            const errorText = JSON.parse(e?.request?.response);
            for (const key in errorText) {
              setError(key as keyof IFormValue, {
                message: t(errorText[key][0]),
                type: 'validate',
              });
            }
          } catch {}
        } else throw e;
      }
    }
  };

  useEffect(() => {
    if (isAuth) {
      router.push(LINK_TEMPLATES.HOME());
      close();
    }
  }, [isAuth]);

  useEffect(() => {
    if (
      registerError &&
      typeof registerError === 'object' &&
      isRegisterError &&
      'data' in registerError
    ) {
      const errors = registerError?.data as FormError;
      for (const key in errors) {
        setError(key as keyof IFormValue, {
          message: t(errors[key][0]),
          type: 'validate',
        });
      }
    }
  }, [isRegisterError]);

  return {
    ip,
    regMethod,
    setRegMethod,
    close,
    facebookLogin,
    googleLogin,
    handleSubmit,
    onSubmit,
    emailInput,
    passwordInput,
    setUsernameInput,
    setPasswordInput,
    defCountry,
    countries,
    language,
    onClickLogin,
    methods,
    usernameInput,
    setEmailInput,
    country,
  };
};
