import { BackBtn, Form } from '@/common/components/modals/styles';
import { patterns } from '@/common/helpers/patterns';
import { useTranslations } from '@/common/hooks';
import { ICountry } from '@/common/types/location';
import { Button } from '@/ui-library/buttons';
import { CheckBox, Input, Select } from '@/ui-library/inputs';
import Image from 'next/image';
import Link from 'next/link';
import React, { FC } from 'react';
import FacebookLogin from 'react-facebook-login';
import { FormProvider } from 'react-hook-form';

import {
  IconCountry,
  IconEmail,
  IconLock,
  IconPerson,
} from '@/assets/icons-ts/form';
import { IconBack } from '@/assets/icons-ts/global';
import IconFacebook from '@/assets/icons/socials/facebook.png';
import IconGoogle from '@/assets/icons/socials/google.png';

import {
  BtnContainer,
  Hr,
  Row,
  SocialBtn,
  SocialBtnWrap,
  Terms,
  WrapAgree,
} from '../../styles';
import Head from '../Head/Head';
import CurrencySelect from './CurrencySelect/CurrencySelect';
import { useCurrencySelect } from './helpers';
import { useRegistrationFormLogic } from './logic';
import { IRegistation } from './types';

const FormRegistration: FC<IRegistation> = (props) => {
  const { hero } = props;

  const {
    countries,
    regMethod,
    methods,
    handleSubmit,
    onSubmit,
    country,
    defCountry,
    googleLogin,
    facebookLogin,
    setRegMethod,
  } = useRegistrationFormLogic(hero);

  const { getDefOptions } = useCurrencySelect();
  const t = useTranslations();

  return (
    <>
      <Head regMethod={Boolean(regMethod)} />
      {regMethod && (
        <BackBtn
          onClick={() => setRegMethod(null)}
          type="button"
          title="back"
        >
          <IconBack />
        </BackBtn>
      )}
      {!regMethod && (
        <>
          <Row>
            <SocialBtnWrap>
              <SocialBtn>
                <Image
                  src={IconFacebook}
                  alt=""
                />
                {t('registration_form_facebook')}
                <FacebookLogin
                  appId={process.env.FACEBOOK_APP_ID}
                  textButton=""
                  callback={facebookLogin}
                />
              </SocialBtn>
            </SocialBtnWrap>
            <SocialBtnWrap onClick={() => googleLogin()}>
              <SocialBtn>
                <Image
                  src={IconGoogle}
                  alt=""
                />
                {t('registration_form_google')}
              </SocialBtn>
            </SocialBtnWrap>
          </Row>
          <Hr />
        </>
      )}
      <FormProvider {...methods}>
        <Form
          onSubmit={handleSubmit(onSubmit)}
          autoComplete="off"
        >
          {!regMethod ? (
            <>
              <Input
                icon={<IconEmail />}
                name="email"
                type="email"
                placeholder={t('registration_form_email') || ''}
                rules={{
                  required: true,
                  pattern: patterns?.email,
                }}
              />
              <Input
                icon={<IconPerson />}
                name="username"
                placeholder={t('registration_form_username') || ''}
                rules={{ required: true }}
              />
              <Input
                icon={<IconLock />}
                name="password"
                type="password"
                placeholder={t('registration_form_password') || ''}
                rules={{ required: true, minLength: 8 }}
              />
            </>
          ) : null}
          <Select<ICountry>
            name="country"
            icon={<IconCountry />}
            placeholder={t('registration_form_country') || ''}
            options={countries || []}
            defaultValue={defCountry}
            rules={{ required: true }}
          />
          <CurrencySelect
            currencies={getDefOptions(country)}
            country={country}
          />
          <WrapAgree>
            <CheckBox
              name="policy"
              rules={{ required: true }}
            />
            <Terms>
              <Link
                href={`/terms_&_conditions`}
                onClick={close}
              >
                {t('registration_form_i_agree')}&nbsp;
                {t('registration_form_first_link')}
              </Link>
              &nbsp;{t('registration_form_and')}&nbsp;
              <Link
                onClick={close}
                href={`/privacy_policy`}
              >
                {t('registration_form_second_link')}
              </Link>
            </Terms>
          </WrapAgree>
          <WrapAgree>
            <CheckBox
              name="promo_code"
              rules={{ required: false }}
            />
            I have a promotional code
          </WrapAgree>
          <BtnContainer>
            <Button
              size="xl"
              type="submit"
            >
              {t('registration_form_button')}
            </Button>
          </BtnContainer>
        </Form>
      </FormProvider>
    </>
  );
};

export default FormRegistration;
