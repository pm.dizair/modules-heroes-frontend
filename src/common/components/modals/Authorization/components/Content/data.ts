import ImageBowman from '@/assets/images/heroes/persons/Bowman.webp';
import ImageKnight from '@/assets/images/heroes/persons/Knight.webp';
import ImageMagician from '@/assets/images/heroes/persons/Magician.webp';
import ImageOutlaw from '@/assets/images/heroes/persons/Outlaw.webp';
import { EHeroType } from '@/common/types/user';

const { KNIGHT, BOWMAN, MAGICIAN, OUTLAW } = EHeroType;

export const characters = [
  { id: 1, name: KNIGHT, image: ImageKnight },
  { id: 2, name: BOWMAN, image: ImageBowman },
  { id: 3, name: MAGICIAN, image: ImageMagician },
  { id: 4, name: OUTLAW, image: ImageOutlaw },
];
