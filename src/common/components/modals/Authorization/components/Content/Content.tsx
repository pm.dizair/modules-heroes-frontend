import { CloseBtn } from '@/common/components/modals/styles';
import { MODALS } from '@/utils/lib/modal/const';
import { FC, useEffect, useMemo, useRef } from 'react';

import { IconClose } from '@/assets/icons-ts/global';
import IconLogo from '@/assets/icons/Logo.svg';
import ImageBG from '@/assets/images/backgrounds/register1.webp';
import ImageOutlaw from '@/assets/images/heroes/persons/Outlaw.webp';

import { getImage } from '@/common/helpers/getImage';
import { IHero } from '@/common/types/user';
import { useHeroes } from '@/modules/home/components/ChooseCharacter/slice';
import { useHomepageContext } from '@/modules/home/context';
import { MODES } from '../../const';
import { useRegistrationContext } from '../../context';
import {
  AnimationWrap,
  Background,
  Character,
  Container,
  FormWrapper,
  Logo,
} from '../../styles';
import { IAnimation } from '../../types';
import FormLogin from '../FormLogin/FormLogin';
import FormRegistration from '../FormRegistration/FormRegistration';

const Content: FC<IAnimation> = (props) => {
  const { gradientOff, hide, modal, mode } = props;
  const ref = useRef<HTMLDivElement>(null);
  const { isRegister, setIsRegister } = useRegistrationContext();
  const isCharacter = mode !== MODES.NO_CHARACTER;
  const { heroes } = useHeroes();

  const scaleOff = () => {
    if (ref.current) {
      ref.current.classList.remove('open');
      ref.current.classList.toggle('close');
    }
  };

  const close = () => {
    scaleOff();
    gradientOff();
    hide();
  };

  const hero = useMemo(() => {
    if (mode !== MODES.NO_CHARACTER) {
      const foundedCharacter = heroes.find((item: IHero) => item.type == mode);
      if (foundedCharacter) {
        return foundedCharacter;
      }
      return undefined;
    }
  }, [mode]);

  useEffect(() => {
    if (modal === MODALS.LOGIN) {
      setIsRegister(false);
    }
  }, []);

  return (
    <>
      <AnimationWrap
        className="open"
        ref={ref}
        character={isCharacter}
      >
        <Container>
          <FormWrapper character={isCharacter}>
            <CloseBtn
              title="close"
              data-testid="close-btn"
              onClick={() => close()}
              type="button"
            >
              <IconClose />
            </CloseBtn>

            {isRegister ? <FormRegistration hero={hero} /> : <FormLogin />}
          </FormWrapper>
          {isCharacter && (
            <>
              <Logo
                src={IconLogo}
                alt=""
              />
              <Background
                src={ImageBG}
                alt=""
              />
              <Character
                width={514}
                height={550}
                src={getImage(hero?.hero_image)}
                alt=""
              />
            </>
          )}
        </Container>
      </AnimationWrap>
    </>
  );
};

export default Content;
