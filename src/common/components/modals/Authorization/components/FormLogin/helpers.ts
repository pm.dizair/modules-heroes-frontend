import { useTranslations } from '@/common/hooks';
import { useSnackbar } from 'notistack';
import { UseFormSetError } from 'react-hook-form';

import { IFormValue } from './types';

export const useSetError = (setError: UseFormSetError<IFormValue>) => {
  const { enqueueSnackbar } = useSnackbar();
  const t = useTranslations();

  const handleError = (status: number) => {
    switch (status) {
      case 400: {
        setError('password', {
          message: t('login_incorrect_password_notification'),
          type: 'validate',
        });
        enqueueSnackbar(t('login_incorrect_password_notification'), {
          variant: 'error',
        });
        break;
      }
      case 404: {
        setError('username', {
          message: t('login_user_not_found_notification'),
          type: 'validate',
        });
        enqueueSnackbar(t('login_user_not_found_notification'), {
          variant: 'error',
        });
        break;
      }
      default:
        enqueueSnackbar(t('login_error_notification'), { variant: 'error' });
    }
  };

  return {
    handleError,
  };
};
