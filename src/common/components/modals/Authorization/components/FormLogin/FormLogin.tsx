import { Form } from '@/common/components/modals/styles';
import { patterns } from '@/common/helpers/patterns';
import { useTranslations } from '@/common/hooks';
import { Button } from '@/ui-library/buttons';
import { Input } from '@/ui-library/inputs';
import { MODALS } from '@/utils/lib/modal/const';
import { useModal } from '@ebay/nice-modal-react';
import Image from 'next/image';
import React from 'react';
import FacebookLogin from 'react-facebook-login';
import { FormProvider } from 'react-hook-form';

import { IconEmail, IconLock } from '@/assets/icons-ts/form';
import IconFacebook from '@/assets/icons/socials/facebook.png';
import IconGoogle from '@/assets/icons/socials/google.png';

import {
  BtnContainer,
  Hr,
  RestoreBtn,
  Row,
  SocialBtn,
  SocialBtnWrap,
} from '../../styles';
import Head from '../Head/Head';
import { useLoginFormLogic } from './logic';

const FormLogin = () => {
  const {
    handleSubmit,
    methods,
    onSubmit,
    googleLogin,
    signInFacebookHandler,
  } = useLoginFormLogic();

  const { show: showRestorePass } = useModal(MODALS.RESTORE_PASS);

  const t = useTranslations();

  const openRestorePass = () => {
    showRestorePass();
  };

  return (
    <>
      <Head />
      <Row>
        <SocialBtnWrap>
          <SocialBtn>
            <Image
              src={IconFacebook}
              alt=""
            />
            {t('login_form_facebook')}
            <FacebookLogin
              appId={process.env.FACEBOOK_APP_ID}
              textButton=""
              callback={signInFacebookHandler}
            />
          </SocialBtn>
        </SocialBtnWrap>
        <SocialBtnWrap>
          <SocialBtn onClick={() => googleLogin()}>
            <Image
              src={IconGoogle}
              alt=""
            />
            {t('login_form_google')}
          </SocialBtn>
        </SocialBtnWrap>
      </Row>
      <Hr />
      <FormProvider {...methods}>
        <Form
          onSubmit={handleSubmit(onSubmit)}
          autoComplete="off"
        >
          <>
            <Input
              icon={<IconEmail />}
              name="username"
              type="email"
              placeholder={t('registration_form_email') || ''}
              rules={{
                required: true,
                pattern: patterns?.email,
              }}
            />

            <Input
              icon={<IconLock />}
              name="password"
              type="password"
              placeholder={t('registration_form_password') || ''}
              rules={{ required: true, minLength: 8 }}
            />
          </>

          <RestoreBtn
            type="button"
            onClick={() => openRestorePass()}
          >
            {t('login_form_forgot_password')}
          </RestoreBtn>

          <BtnContainer>
            <Button
              size="xl"
              type="submit"
            >
              {t('login_form_log_in')}
            </Button>
          </BtnContainer>
        </Form>
      </FormProvider>
    </>
  );
};
export default FormLogin;
