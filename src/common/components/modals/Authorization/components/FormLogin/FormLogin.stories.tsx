import { withProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import FormLogin from './FormLogin';
import { mockResponse } from './mock';

export const Base: StoryObj<typeof FormLogin> = {};

Base.play = async ({ canvasElement, step }) => {
  await step('check error validation', async () => {
    const canvas = within(canvasElement);
    const button = canvas.getByTestId('color-button');

    await sleep(600);
    expect(button).toBeVisible();

    userEvent.click(button);
    await sleep(600);

    userEvent.click(button);
    expect(canvas.getAllByTestId('form-error')).toHaveLength(2);
  });

  await step('type valid credentials', async () => {
    sleep(1000);
    const canvas = within(canvasElement);
    const [emailInput, passwordInput] = canvas.getAllByTestId('form-input');
    const button = canvas.getByTestId('color-button');

    await userEvent.type(emailInput, 'tester@casino.com');
    await userEvent.type(passwordInput, 'Tester1234567');

    userEvent.click(button);
    await sleep(600);

    userEvent.click(button);
    expect(canvas.queryByTestId('form-error')).not.toBeInTheDocument();
  });

  await step('check email validation', async () => {
    await sleep(1400);
    const canvas = within(canvasElement);
    const [emailInput] = canvas.getAllByTestId('form-input');
    const button = canvas.getByTestId('color-button');

    await userEvent.clear(emailInput);
    await userEvent.type(emailInput, 'test@dsc');

    await userEvent.click(button);
    await sleep(600);

    await userEvent.click(button);
    await sleep(600);
    expect(
      await canvas.findByText('The field is filled incorrectly')
    ).toBeInTheDocument();
  });

  await step('check password validation', async () => {
    await sleep(2400);
    const canvas = within(canvasElement);
    const [_, passwordInput] = canvas.getAllByTestId('form-input');
    const button = canvas.getByTestId('color-button');

    await userEvent.clear(passwordInput);
    await userEvent.type(passwordInput, '1234');

    await userEvent.click(button);
    await sleep(600);

    await userEvent.click(button);
    expect(
      await canvas.findByText('Min length is NUMBER symbol')
    ).toBeInTheDocument();
  });
};

const meta: Meta<typeof FormLogin> = {
  title: 'Modules/Auth/FormLogin',
  component: FormLogin,
  tags: ['autodocs'],
  decorators: [withProvider],
  parameters: {
    mockData: [
      {
        url: 'https://api2.dizair.ee/auth/login/',
        method: 'POST',
        status: 200,
        response: mockResponse,
      },
    ],
  },
};

export default meta;
