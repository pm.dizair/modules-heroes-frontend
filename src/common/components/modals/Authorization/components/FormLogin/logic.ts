// import useAnalyticsEventTracker from '@/helpers/useAnalyticsEventTracker';
import { useTranslations, useWindowScroll } from '@/common/hooks';
import { MODALS } from '@/utils/lib/modal/const';
import { useModal } from '@ebay/nice-modal-react';
import { useGoogleLogin } from '@react-oauth/google';
import { useSnackbar } from 'notistack';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';

import { useCallAuth } from '@/modules/auth/api/helpers/useCallAuth';
import { useCallRefreshUser } from '@/modules/auth/api/helpers/useCallRefreshUser';

import { useCheckOTPMutation } from '@/modules/auth/api/rtk-query/user/user';

import { useAxiosAuthHandler } from '@/modules/auth/api/helpers/useAxiosAuthHandler';
import { useSetError } from './helpers';
import { IFormValue } from './types';

export const useLoginFormLogic = () => {
  const { scrollToTop } = useWindowScroll();
  const { visible, hide } = useModal(MODALS.REGISTER);
  const t = useTranslations();

  const { show: showRegistration } = useModal(MODALS.REGISTER);
  const { show: showDoubleAuth, hide: hideDoubleAuth } = useModal(
    MODALS.DOUBLE_AUTH
  );
  const { show: showForgotPassword } = useModal(MODALS.RESTORE_PASS);

  const methods = useForm<IFormValue>({ mode: 'onSubmit' });
  const { handleSubmit, reset, setError } = methods;

  const { enqueueSnackbar } = useSnackbar();

  const [facebookSignIn] = useAxiosAuthHandler('auth/facebook-sign-in/');
  const [googleSignIn] = useAxiosAuthHandler('auth/google-sign-in/');
  const [simpleLogin, { error, isError, isSuccess }] =
    useAxiosAuthHandler('auth/login/');

  const [checkOTP] = useCheckOTPMutation();

  const { handleError } = useSetError(setError);

  const close = () => {
    reset();
    hide();
  };

  const onClickRegister = () => {
    showRegistration();
    hide();
  };

  const loginAndClose = () => {
    close();
    scrollToTop();
  };

  const loginUserHandler = useCallRefreshUser(loginAndClose);

  const signInFacebookHandler = useCallAuth(facebookSignIn, loginUserHandler);
  const signInGoogleHandler = useCallAuth(googleSignIn, loginUserHandler);
  const simpleLoginHandler = useCallAuth(simpleLogin, loginUserHandler, false);

  const googleLogin = useGoogleLogin({
    onSuccess: async (data) => {
      await signInGoogleHandler(data);
    },
  });

  const login = (data: IFormValue, old_data = {}, status = false) => {
    simpleLoginHandler({ ...data, ...old_data });
    if (status) {
      hideDoubleAuth();
    }
  };

  useEffect(() => {
    if (isSuccess) {
      enqueueSnackbar(t('successfully_notification'), { variant: 'success' });
      reset();
      loginAndClose();
    } else if (
      error &&
      typeof error === 'object' &&
      isError &&
      'data' in error
    ) {
      enqueueSnackbar((error.data as string[])[0], {
        variant: 'error',
      });
    }
  }, [isSuccess, isError]);

  const onSubmit = async (data: IFormValue) => {
    const otp = await checkOTP(data);

    if ('error' in otp && 'status' in otp.error) {
      handleError(otp.error.status as number);
    } else if ('data' in otp && otp.data?.is_enabled) {
      showDoubleAuth({
        onSubmit: (check_otp: IFormValue) => login(check_otp, data, true),
      });
    } else {
      login(data);
    }
  };

  return {
    visible,
    hide,
    onSubmit,
    login,
    signInFacebookHandler,
    signInGoogleHandler,
    simpleLoginHandler,
    hideDoubleAuth,
    showDoubleAuth,
    showForgotPassword,
    showRegistration,
    googleLogin,
    close,
    methods,
    handleSubmit,
    onClickRegister,
  };
};
