import { createContext, FC, ReactNode, useContext, useState } from 'react';

interface IRegisterContext {
  isRegister: boolean;
  setIsRegister: (x: boolean) => void;
}

export const RegistrationContext = createContext<IRegisterContext>({
  isRegister: true,
  setIsRegister: (x: boolean) => {},
});

export const useRegistrationContext = () => useContext(RegistrationContext);

interface IRegistrationProps {
  children: ReactNode;
}

export const RegistrationProvider: FC<IRegistrationProps> = (props) => {
  const { children } = props;
  const [isRegister, setIsRegister] = useState<boolean>(true);

  return (
    <RegistrationContext.Provider
      value={{
        isRegister,
        setIsRegister: (value: boolean) => setIsRegister(value),
      }}
    >
      {children}
    </RegistrationContext.Provider>
  );
};
