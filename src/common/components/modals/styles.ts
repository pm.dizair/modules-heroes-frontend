import styled from 'styled-components';

import { IHead, IOpen } from './types';

export const Wrapper = styled.div<IOpen>`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  &.open {
    animation: gradient_open 0.5s alternate;
    animation-fill-mode: forwards;
    transform-origin: center;
  }
  &.close {
    animation: gradient_closing 0.4s alternate;
    animation-fill-mode: forwards;
    transform-origin: center;
  }
  @media screen and (max-height: 800px) and (min-width: 601px) {
    overflow: auto;
    align-items: flex-start;
    padding: 24px 0;
  }
  @keyframes gradient_open {
    0% {
      background: rgba(0, 0, 0, 0);
    }
    100% {
      background: rgba(0, 0, 0, 0.8);
    }
  }
  @keyframes gradient_closing {
    0% {
      background: rgba(0, 0, 0, 0.8);
    }
    100% {
      background: rgba(0, 0, 0, 0);
    }
  }
`;
export const Form = styled.form`
  width: 100%;
  z-index: 1;
  position: relative;
`;

export const CloseBtn = styled.button`
  position: absolute;
  right: 12px;
  top: 12px;
  width: 30px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  & > svg {
    fill: #b3b3b3;
    transition: 0.3s;
    width: 30px;
    height: 30px;
  }
  &:hover {
    & > svg {
      transition: 0.3s;
      fill: #ffd600;
    }
  }
  @media screen and (max-width: 600px) {
    right: 8px;
  }
`;
export const BackBtn = styled(CloseBtn)`
  position: absolute;
  left: 12px;
  top: 12px;
  right: auto;
  @media screen and (max-width: 600px) {
    left: 8px;
  }
`;

export const HeadWrap = styled.div<IHead>`
  width: 100%;
  padding-bottom: 40px;
  padding-top: ${(props) => (props.offset ? '24px' : '0')};
  z-index: 1;
  @media screen and (max-width: 1512px) {
    padding-bottom: 32px;
  }
  @media screen and (max-width: 600px) {
    padding-bottom: 24px;
    padding-top: ${(props) => (props.offset ? '16px' : '0')};
  }
`;

export const Title = styled.div`
  font-style: normal;
  font-family: var(--font-grenze-gotisch);
  color: gold;
  text-shadow: rgba(0, 0, 0, 0.5) 0px 1px 1px;
  transform: translateY(-3px);
  font-style: normal;
  font-weight: 700;
  font-size: 40px;
  line-height: 40px;
  text-align: center;
  text-align: start;
  @media screen and (max-width: 1512px) {
    font-size: 32px;
  }
`;

export const Text = styled.div`
  font-family: var(--font-nunito);
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  display: flex;
  align-items: center;
  color: var(--gray);
  padding-top: 12px;

  & span {
    color: #ffd600;
    padding-left: 8px;
    cursor: pointer;
    &:hover {
      text-decoration: underline;
    }
    @media screen and (max-width: 1024px) {
      &:hover {
        text-decoration: none;
      }
    }
  }
  @supports (-webkit-touch-callout: none) {
    font-size: 17px;
    a {
      font-size: 17px;
    }
  }
`;
