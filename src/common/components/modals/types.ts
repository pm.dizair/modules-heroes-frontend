export interface IModal {
  id: string;
}

export interface IOpen {
  isOpen: boolean;
}
export interface IHead {
  offset?: boolean;
}
