import { useMediaQuery } from 'react-responsive';

export const useIsTablet = () => useMediaQuery({ maxWidth: 1024 });
export const useIsDesktop = () => useMediaQuery({ minWidth: 1025 });
export const useIsMobile = () => useMediaQuery({ maxWidth: 767 });
export const useIsSmallMobile = () => useMediaQuery({ maxWidth: 540 });

export const useMinResponsive = (min: number) =>
  useMediaQuery({ minWidth: min });
export const useMaxResponsive = (max: number) =>
  useMediaQuery({ maxWidth: max });
