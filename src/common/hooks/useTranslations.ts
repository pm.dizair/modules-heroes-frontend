import { useCommon } from '@/common/store/common/common.slice';
import { ITranslation } from '../types';

export const useTranslations = (selectedTranslations?: ITranslation) => {
  const { translations } = useCommon();

  const currentTranslates = Array.isArray(selectedTranslations)
    ? selectedTranslations
    : translations;

  return (keyword: string) => {
    const result = currentTranslates?.find(
      (item) => item.keyword === keyword
    )?.text;

    return result || keyword;
  };
};
