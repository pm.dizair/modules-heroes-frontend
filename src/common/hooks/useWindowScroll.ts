export const useWindowScroll = () => {
  const scrollToTop = (behavior: 'smooth' | 'auto' = 'auto') => {
    const element = document.getElementById('header-invision');

    if (!element) {
      return;
    }

    element.scrollIntoView({ behavior });
  };

  return { scrollToTop };
};
