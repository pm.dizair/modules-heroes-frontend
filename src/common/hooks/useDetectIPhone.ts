import MobileDetect from 'mobile-detect';
import { useMemo } from 'react';

export const useDetectIPhone = () => {
  return useMemo(() => {
    const userAgent =
      typeof navigator === 'undefined' ? 'SSR' : navigator.userAgent;
    const mobile = new MobileDetect(userAgent).mobile();
    if (typeof mobile === 'string' && mobile.includes('iPhone')) {
      return true;
    }

    return false;
  }, []);
};
