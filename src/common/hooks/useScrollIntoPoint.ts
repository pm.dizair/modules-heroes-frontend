export const useScrollIntoPoint = (id: string) => {
  const scrollIntoPoint = () => {
    const element = document.getElementById(id);
    element?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  };

  return { scrollIntoPoint };
};
