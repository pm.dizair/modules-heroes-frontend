export const CATEGORIES = {
  POPULAR: 'popular',
  NEW: 'new',
  TOURNAMENT: 'tournament',
} as const;

export const GAME_MODE = {
  DEMO: 'demo',
  REAL: 'real',
} as const;

export const GAME_HISTORY_STATUS = {
  LOSS: 'loss',
  WIN: 'win',
  ALL: 'all',
} as const;
