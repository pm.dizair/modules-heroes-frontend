import CustomHead from '@/common/components/meta-head/MetaHead/MetaHead';
import { PageProvider } from '@/common/components/providers';
import { REQUEST_LIMITS } from '@/common/const';
import { ITranslation } from '@/common/types';
import {
  CommonApi,
  GameApi,
  PromotionApi,
  UserApi,
} from '@/utils/api/server-query';
import { IGetGamesResponse } from '@/utils/api/server-query/game/types';
import {
  IGetPromotionsResponse,
  IGetRandomPromotionResponse,
} from '@/utils/api/server-query/promotions/types';
import { GetServerSideProps } from 'next';

import { useAppDispatch } from '@/common/hooks';
import { IHero } from '@/common/types/user';
import { Home } from '@/modules/home/Home';
import { QuestionApi } from '@/modules/home/api/server-query/questions/questions';
import { IGetFaqResponse } from '@/modules/home/api/server-query/questions/types';
import { updateHeroes } from '@/modules/home/components/ChooseCharacter/slice';
import { useEffect } from 'react';

export interface IHomePageProps {
  popularGames: IGetGamesResponse;
  newGames: IGetGamesResponse;
  readyToPlayGames: IGetGamesResponse;
  promotion: IGetRandomPromotionResponse;
  promotions: IGetPromotionsResponse;
  bonusPromotions: IGetPromotionsResponse;
  questions: IGetFaqResponse;
  translation: ITranslation[];
  heroes: IHero[];
}

interface IGetServerSidePropsResponse {
  props: IHomePageProps;
}

export const getServerSideProps: GetServerSideProps = async (
  context
): Promise<IGetServerSidePropsResponse> => {
  const language: string = context.req.cookies.language || 'en';
  const sessionId: string = context.req.cookies.sessionId || '';
  const game = GameApi(sessionId);
  const promo = PromotionApi(sessionId);
  const user = UserApi(sessionId);

  const popularGames = await game.getGamesByCategory({
    category: 'popular',
  });
  const newGames = await game.getGamesByCategory({
    category: 'new',
  });
  const readyToPlayGames = await game.getGamesByCategory({
    category: 'new',
    limit: REQUEST_LIMITS.READY_TO_PLAY,
  });
  const promotion = await promo.getRandomPromotion({
    language,
    position: 'main_page',
  });
  const promotions = await promo.getPromotions({
    page: 1,
    limit: REQUEST_LIMITS.PROMOTIONS,
    language,
  });
  const bonusPromotions = await promo.getPromotions({
    page: 1,
    limit: REQUEST_LIMITS.BONUS_PROMOTIONS,
    language,
  });
  const heroes = await user.getHeroes(language);

  const questions = await QuestionApi.getFaq({ language });

  const translation = await CommonApi.getSiteText(language);

  return {
    props: {
      popularGames,
      newGames,
      readyToPlayGames,
      promotion,
      promotions,
      bonusPromotions,
      questions,
      translation,
      heroes,
    },
  };
};

export default function HomePage(props: IHomePageProps) {
  const dispatch = useAppDispatch();

  const { heroes } = props;
  useEffect(() => {
    dispatch(updateHeroes(heroes));
  }, [heroes]);

  return (
    <>
      <CustomHead />
      <PageProvider<IHomePageProps> value={props}>
        <Home />
      </PageProvider>
    </>
  );
}
