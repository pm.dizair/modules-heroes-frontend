import CustomHead from '@/common/components/meta-head/MetaHead/MetaHead';
import { PageProvider } from '@/common/components/providers';
import { ITranslation } from '@/common/types';
import { CommonApi, PromotionApi } from '@/utils/api/server-query';
import { GetServerSideProps } from 'next';
import { FC } from 'react';

import LiveCasino from '@/modules/live-casino/LiveCasino';
import { IGetRandomPromotionResponse } from '@/utils/api/server-query/promotions/types';

export interface ILiveCasinoPageProps {
  translation: ITranslation[];
  promotion: IGetRandomPromotionResponse;
}

interface IGetServerSidePropsResponse {
  props: ILiveCasinoPageProps;
}

export const getServerSideProps: GetServerSideProps = async (
  context
): Promise<IGetServerSidePropsResponse> => {
  const language: string = context.req.cookies.language || 'en';
  const sessionId: string = context.req.cookies.sessionId || '';
  const promo = PromotionApi(sessionId);

  const promotion = await promo.getRandomPromotion({
    language,
    position: 'main_page',
  });
  const translation = await CommonApi.getSiteText(language);

  return {
    props: {
      translation,
      promotion,
    },
  };
};

const LiveCasinoPage: FC<ILiveCasinoPageProps> = (props) => {
  return (
    <PageProvider<ILiveCasinoPageProps> value={props}>
      <CustomHead title="Live Casino" />
      <LiveCasino />
    </PageProvider>
  );
};

export default LiveCasinoPage;
