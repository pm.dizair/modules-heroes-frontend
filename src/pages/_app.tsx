import { Content } from '@/common/components/containers';
import { useListenSockets } from '@/common/helpers/socket/useListenSockets';
import { useWindowScroll } from '@/common/hooks';
import { ITranslation, TAppPropsWithLayout } from '@/common/types';
import { BasicLayout } from '@/layouts';
import {
  CookiesSnack,
  ErrorSnack,
  InfoSnack,
  SuccessSnack,
} from '@/layouts/Basic/components/Snacks';
import '@/styles/scss/index.scss';
import Theme from '@/styles/theme/Theme';
import { useInitFetch } from '@/utils/api/init/useInitFetch';
import '@/utils/lib/modal/register';
import { store } from '@/utils/store';
import NiceModal from '@ebay/nice-modal-react';
import localFont from '@next/font/local';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { Router, useRouter } from 'next/router';
import { SnackbarProvider } from 'notistack';
import nProgress from 'nprogress';
import { FC, PropsWithChildren, useEffect } from 'react';
import { Provider } from 'react-redux';

export const fontGotisch = localFont({
  src: '../assets/fonts/GrenzeGotisch-VariableFont_wght.ttf',
  variable: '--font-grenze-gotisch',
});

export const fontNunito = localFont({
  src: '../assets/fonts/Nunito-VariableFont_wght.ttf',
  variable: '--font-nunito',
});

Router.events.on('routeChangeStart', nProgress.start);
Router.events.on('routeChangeError', nProgress.done);
Router.events.on('routeChangeComplete', nProgress.done);

interface InitFetchProps extends PropsWithChildren {
  translation: ITranslation[];
}

export const InitFetch: FC<InitFetchProps> = (props) => {
  const { children, translation } = props;
  useInitFetch(translation);
  useListenSockets();
  return <>{children}</>;
};

const App = (props: TAppPropsWithLayout) => {
  const { Component, pageProps } = props;
  const { route } = useRouter();

  const { scrollToTop } = useWindowScroll();

  //  TODO: return when tacking will be done
  // useEffect(() => {
  //   const TRACKING_ID = process.env.REACT_APP_GOOGLE_ANALYTIC_TRACKING_ID;
  //   if (TRACKING_ID) {
  //     ReactGA4.initialize(TRACKING_ID);
  //   }
  // }, []);

  useEffect(() => {
    if (typeof document !== 'undefined') {
      scrollToTop();
    }
  }, [route]);

  const renderWithLayout =
    Component.getLayout ||
    function (page: any) {
      return <BasicLayout> {page}</BasicLayout>;
    };

  return (
    <Provider store={store}>
      <GoogleOAuthProvider clientId={process.env.GOOGLE_CLIENT_ID as string}>
        <Theme>
          <SnackbarProvider
            maxSnack={3}
            Components={{
              success: SuccessSnack,
              error: ErrorSnack,
              info: InfoSnack,
              cookies: CookiesSnack,
            }}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            preventDuplicate={true}
            autoHideDuration={5000}
          >
            <NiceModal.Provider>
              <InitFetch translation={pageProps.translation}>
                <Content
                  className={`${fontGotisch.variable} ${fontNunito.variable}`}
                >
                  {renderWithLayout(
                    <Component
                      {...pageProps}
                      key={route}
                    />,
                    pageProps
                  )}
                </Content>
              </InitFetch>
            </NiceModal.Provider>
          </SnackbarProvider>
        </Theme>
      </GoogleOAuthProvider>
    </Provider>
  );
};

export default App;
