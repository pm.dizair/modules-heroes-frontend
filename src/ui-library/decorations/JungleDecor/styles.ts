import Image from 'next/image';
import styled, { css } from 'styled-components';
import { IDecoration } from './types';

export const JungleImage = styled(Image)<IDecoration>`
  position: absolute;
  height: 366px;
  width: 170px;
  object-fit: contain;
  top: -50px;
  ${({ decorOrientation }) =>
    decorOrientation === 'left'
      ? css`
          right: -70px;
        `
      : css`
          left: -70px;
          transform: rotateY(180deg);
        `}
  @media screen and (max-width: 1620px) {
    top: -60px;
    ${({ decorOrientation }) =>
      decorOrientation === 'left'
        ? css`
            right: -60px;
          `
        : css`
            left: -60px;
          `}
  }
  @media screen and (max-width: 1280px) {
    height: 280px;
    width: 130px;
    top: -45px;
    ${({ decorOrientation }) =>
      decorOrientation === 'left'
        ? css`
            right: -45px;
          `
        : css`
            left: -45px;
          `}
  }
`;
