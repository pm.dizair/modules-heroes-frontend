import imageJungle from '@/assets/images/decoration/GrapeVineLeft.webp';
import { FC } from 'react';
import { JungleImage } from './styles';
import { IJungleDecor } from './types';

const JungleDecor: FC<IJungleDecor> = (props) => {
  const { decorOrientation, doubleDecoration } = props;
  return (
    <>
      {doubleDecoration ? (
        <>
          <JungleImage
            decorOrientation={'left'}
            src={imageJungle}
            alt="jungle image"
          />
          <JungleImage
            decorOrientation={'right'}
            src={imageJungle}
            alt="jungle image"
          />
        </>
      ) : (
        <JungleImage
          decorOrientation={decorOrientation}
          src={imageJungle}
          alt="jungle image"
        />
      )}
    </>
  );
};

export default JungleDecor;
