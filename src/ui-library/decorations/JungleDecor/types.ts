export interface IDecoration {
  decorOrientation?: 'left' | 'right';
}

export interface IJungleDecor extends IDecoration {
  doubleDecoration?: boolean;
}
