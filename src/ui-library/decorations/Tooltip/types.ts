import { PropsWithChildren } from 'react';

export interface ITooltip {
  size?: number;
  color?: string;
}

export interface ITooltipProps extends PropsWithChildren, ITooltip {
  text?: string;
}
