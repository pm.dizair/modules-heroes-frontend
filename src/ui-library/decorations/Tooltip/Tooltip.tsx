import { FC } from 'react';

import { Initiator, Window, Wrapper } from './styles';
import { ITooltipProps } from './types';

const Tooltip: FC<ITooltipProps> = (props) => {
  const { color, size, children, text } = props;
  return (
    <Wrapper>
      <Initiator>{children}</Initiator>
      <Window
        className="window-tooltip"
        color={color}
        size={size}
      >
        {text}
      </Window>
    </Wrapper>
  );
};

export default Tooltip;
