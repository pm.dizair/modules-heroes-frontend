import styled, { css } from 'styled-components';

import { ITooltip } from './types';

export const Wrapper = styled.div`
  position: relative;
`;

export const Initiator = styled.div`
  cursor: help;
  z-index: 5;
  @media screen and (min-width: 1025px) {
    &:hover ~ .window-tooltip {
      transform: translate3d(0, calc(-100% + -6px), 0);
      visibility: visible;
      opacity: 1;
    }
  }
`;

export const Window = styled.div<ITooltip>`
  top: 0;
  left: 0;
  transform: translate3d(0, -90%, 0);
  width: 320px;
  max-height: 115px;
  visibility: hidden;
  display: inline;
  opacity: 0;
  transition: all 0.25s cubic-bezier(0, 0, 0.2, 1);
  z-index: 4;
  position: absolute;
  box-shadow: inset 0px 0px 24px rgba(255, 255, 255, 0.12);
  backdrop-filter: blur(20px);
  padding: 10px;
  border-radius: 12px;
  border: 1px solid var(--gold);
  ${({ theme }) => theme.text.getLineClamp(6)}
  text-align: justify;
  text-transform: capitalize;
  color: #fff;
  letter-spacing: 0;
  line-height: 1.4;
  font-weight: 400;
  cursor: pointer;
  ${({ size, color }) => css`
    background: linear-gradient(
        0deg,
        rgba(255, 255, 255, 0.04),
        rgba(255, 255, 255, 0.04)
      ),
      ${color || 'rgba(1, 13, 27, 0.8)'};
    font-size: ${size ? size + 'px' : '12px'};
  `}
  @media screen and (min-width: 1025px) {
    &:hover {
      transform: translate3d(0, calc(-100% + -6px), 0);
      visibility: visible;
      opacity: 1;
    }
  }
`;
