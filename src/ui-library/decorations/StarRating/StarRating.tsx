import { FC } from 'react';

import imageStartRating from '@/assets/icons/RatingStar.webp';
import imageStartRatingGray from '@/assets/icons/RatingStarGray.webp';

import { Rating, Star } from './styles';
import { IRating } from './types';

const StarRating: FC<IRating> = (props) => {
  const { level } = props;
  const renderLevel = () => {
    return [...Array(5)].map((_, index) => (
      <Star
        key={index}
        src={index < level ? imageStartRating : imageStartRatingGray}
        alt="level"
        sizes="(max-width: 1280px) 26px, (max-width: 540px) 24px, 28px"
      />
    ));
  };

  return <Rating>{renderLevel()}</Rating>;
};

export default StarRating;
