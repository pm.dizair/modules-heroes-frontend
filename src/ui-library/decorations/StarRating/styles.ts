import Image from 'next/image';
import styled from 'styled-components';

export const Star = styled(Image)`
  width: 28px;
  height: 28px;

  @media screen and (max-width: 1280px) {
    width: 26px;
    height: 26px;
  }
  @media screen and (max-width: 540px) {
    width: 24px;
    height: 24px;
  }
`;

export const Rating = styled.div`
  ${({ theme }) => theme.content.flex.row};
  gap: 16px;
  @media screen and (max-width: 1280px) {
    gap: 12px;
  }
  @media screen and (max-width: 540px) {
    gap: 6px;
  }
`;
