import '@/styles/scss/index.scss';
import { store } from '@/utils/store';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { Provider } from 'react-redux';

import StarRating from './StarRating';
import { IRating } from './types';

const withProvider = (Story: StoryFn) => (
  <Provider store={store}>{<Story />}</Provider>
);

const meta: Meta<typeof StarRating> = {
  title: 'UI/Decorations/StarRating',
  component: StarRating,
  tags: ['autodocs'],
  decorators: [withProvider],
  args: {
    level: 2,
  },
};

export default meta;

export const StarRatingComponent: StoryObj<typeof meta> = (arg: IRating) => (
  <StarRating {...arg} />
);

StarRatingComponent.play = async ({ canvasElement, step }) => {
  const canvas = within(canvasElement);

  await step('test one', async () => {
    // TODO: test
  });
};
