import Image from 'next/image';

import imageLine from '@/assets/images/decoration/Line.webp';

import { Dot, Wrapper } from './styles';

const SeparateLine = () => {
  return (
    <Wrapper className="separate-line">
      <Image
        src={imageLine}
        alt="line"
      />
      <Dot />
      <Image
        src={imageLine}
        alt="line"
      />
    </Wrapper>
  );
};

export default SeparateLine;
