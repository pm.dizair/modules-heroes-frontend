import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 5px;
  margin: 4px 0;
  width: 100%;
  transform: translate3d(0, 0, 0);
  img {
    height: 2px;
    width: calc(50% - 10px);
    &:last-child {
      transform: rotate(180deg);
    }
  }
  @media screen and (max-width: 1620px) {
    margin: 8px 0;
  }
`;

export const Dot = styled.div`
  width: 5px;
  height: 5px;
  border-radius: 50%;
  background-color: var(--gold);
`;
