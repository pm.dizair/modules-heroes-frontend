import { FC } from 'react';
import { Wrapper } from './styles';
import { IRoundButton } from './types';

import React from 'react';

const RoundButton: FC<IRoundButton> = (props) => {
  const { children, ...rest } = props;

  return (
    <Wrapper
      {...rest}
      type="button"
    >
      {children}
    </Wrapper>
  );
};

export default RoundButton;
