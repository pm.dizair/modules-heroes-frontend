import { HTMLAttributes } from 'react';

export interface IRoundButton extends HTMLAttributes<HTMLButtonElement> {}
