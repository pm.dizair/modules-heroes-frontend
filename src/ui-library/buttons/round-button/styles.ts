import ButtonBG from '@/assets/icons/CircleButton.svg';
import styled from 'styled-components';

export const Wrapper = styled.button`
  z-index: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.4s linear;
  background-image: url(${ButtonBG.src});
  background-repeat: no-repeat;
  width: 43px;
  height: 40px;
  & > svg {
    width: 20px;
    height: 20px;
  }

  &:hover {
    & > svg {
      path {
        stroke: var(--gold);
      }
    }
  }
  &:active {
    transform: scale(0.8);
  }
  @media screen and (max-width: 1024px) {
    & > svg {
      path {
        stroke: var(--gold);
      }
    }
  }
`;
