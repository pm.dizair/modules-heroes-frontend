import imageIcon from '@/assets/icons/CircleButton.svg';

import { CircleLoader } from '@/ui-library/loaders';
import { forwardRef } from 'react';
import {
  Content,
  Gradient,
  GradientStriped,
  ImageIcon,
  LGButton,
  MDButton,
  SMButton,
  Striped,
  XLButton,
  XSButton,
} from './styles';
import { IButton } from './types';

const Button = forwardRef<HTMLButtonElement, IButton>((props, ref) => {
  const {
    children,
    theme = 'gold',
    className,
    type = 'button',
    size,
    onClick,
    isAnimated = true,
    isIcon,
    loading = false,
    disabled,
    ...rest
  } = props;

  const getWrapper = () => {
    switch (size) {
      case 'xl':
        return XLButton;
      case 'lg':
        return LGButton;
      case 'md':
        return MDButton;
      case 'sm':
        return SMButton;
      case 'xs':
        return XSButton;
      default:
        return MDButton;
    }
  };

  const Wrapper = getWrapper();

  return (
    <Wrapper
      ref={ref}
      isIcon={isIcon}
      onClick={onClick}
      themeBtn={theme}
      type={type}
      isAnimated={isAnimated}
      className={className || 'color-button'}
      data-testid="color-button"
      disabled={loading || disabled}
      {...rest}
    >
      {theme === 'icon' && (
        <ImageIcon
          src={imageIcon}
          alt="image-icon"
        />
      )}
      {theme === 'gold' && <Gradient />}
      {theme === 'striped' && (
        <GradientStriped>
          <Striped />
        </GradientStriped>
      )}
      {loading ? (
        <CircleLoader />
      ) : theme === 'bordered' ? (
        <>
          <Content>{children}</Content>
          {children}
        </>
      ) : (
        children
      )}
    </Wrapper>
  );
});

Button.displayName = 'Button';

export default Button;
