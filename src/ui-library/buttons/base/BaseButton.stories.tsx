import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import Button from './BaseButton';

const meta: Meta<typeof Button> = {
  title: 'UI/Buttons/BaseButton',
  component: Button,
  tags: ['autodocs'],
};

export default meta;

export const Base: StoryObj<typeof Button> = {};

Base.args = {
  onClick: () => console.log('Button clicked'),
  children: 'Click Me',
};

Base.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const button = canvas.getByText('Click Me');

  expect(button).toBeInTheDocument();

  userEvent.click(button);
};
