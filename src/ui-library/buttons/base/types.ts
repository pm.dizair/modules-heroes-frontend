import { BUTTON_TYPE, SIZE } from '@/common/types';
import { HTMLAttributes } from 'react';

export type BUTTON_THEME =
  | 'gray'
  | 'ghost'
  | 'gold'
  | 'dark-gold'
  | 'striped'
  | 'arrow'
  | 'icon'
  | 'bordered';

export interface IButton extends HTMLAttributes<HTMLButtonElement> {
  size?: SIZE;
  isIcon?: boolean;
  className?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  theme?: BUTTON_THEME;
  padding?: string;
  fontSize?: number;
  fullWidth?: boolean;
  isAnimated?: boolean;
  disabled?: boolean;
  type?: BUTTON_TYPE;
  loading?: boolean;
}

export interface IWrapper extends Omit<IButton, 'theme'> {
  themeBtn?: BUTTON_THEME;
}
