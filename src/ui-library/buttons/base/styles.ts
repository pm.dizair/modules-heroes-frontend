import styled, { css } from 'styled-components';

import imageStripedButton from '@/assets/images/backgrounds/mask/striped.png';

import Image from 'next/image';
import { IWrapper } from './types';

export const Gradient = styled.div`
  background: linear-gradient(284.06deg, #e2a12e 10.74%, #fef15f 84.23%);
  position: absolute;
  inset: 0;
  z-index: -1;
  border-radius: 99px;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.5);
`;

export const GradientStriped = styled.div`
  background: linear-gradient(301.11deg, #f5d121 0.42%, #db6401 58.78%);
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.5);
  position: absolute;
  inset: 0;
  z-index: -1;
  border-radius: 99px;
  overflow: hidden;
`;

export const Striped = styled.span`
  display: block;
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;
  &::after {
    content: '';
    position: absolute;
    inset: -10px;
    background: url(${imageStripedButton.src});
  }
`;

const goldButton = css`
  position: relative;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.5);
  color: #010d1b;
  z-index: 1;
  background: linear-gradient(
    90deg,
    rgba(131, 58, 11, 1) 0%,
    #f5e58c 52%,
    rgba(131, 58, 11, 1) 100%
  );

  border: 1px solid transparent;
  @media screen and (min-width: 1025px) {
    &:hover {
      ::before {
        /* TODO: animation */
      }
    }
  }
`;

export const Content = styled.span`
  position: absolute;
  inset: 1px;
  z-index: 1;
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.1) 0%,
      rgba(255, 255, 255, 0.1) 100%
    ),
    #010d1b;
  box-shadow: 0px 2px 8px 0px rgba(0, 0, 0, 0.5);
  border-radius: 99px;
  ${({ theme }) => theme.content.flex.center}
`;

const borderedButton = css`
  border-radius: 99px;
  position: relative;
  overflow: hidden;
  &::after {
    content: '';
    position: absolute;
    z-index: 0;
    width: calc(100% + 2px);
    aspect-ratio: 1 / 1;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: linear-gradient(
      270deg,
      #4e5660 20%,
      #fbe13a 53.08%,
      #4e5660 80%
    );
  }
  @keyframes border {
    0% {
      transform: translate(-50%, -50%) rotate(0deg);
    }
    50% {
      transform: translate(-50%, -50%) rotate(180deg);
    }
    100% {
      transform: translate(-50%, -50%) rotate(360deg);
    }
  }
  @media screen and (min-width: 1025px) {
    &:hover {
      span {
        color: var(--gold);
      }
      &::after {
        animation: border 3s linear infinite;
      }
    }
  }
  @media screen and (max-width: 1024px) {
    span {
      color: var(--gold);
    }
  }
`;

const iconButton = css`
  position: relative;
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.4s linear;
  padding: 0 !important;
  @media screen and (max-width: 1024px) {
    svg {
      path {
        fill: var(--gold);
      }
    }
  }
  @media screen and (min-width: 1025px) {
    &:hover {
      svg {
        path {
          fill: var(--gold);
        }
      }
    }
  }
  @media screen and (max-width: 540px) {
    width: 36px;
    height: 36px;
  }
`;

export const ImageIcon = styled(Image)`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

const stripedButton = css`
  position: relative;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.5);
  color: #010d1b;
  z-index: 1;
  background: linear-gradient(
    90deg,
    rgba(131, 58, 11, 1) 0%,
    #f5e58c 52%,
    rgba(131, 58, 11, 1) 100%
  );

  border: 1px solid transparent;
  @media screen and (min-width: 1025px) {
    &:hover {
      ::before {
        /* TODO: animation */
      }
    }
  }
`;

const grayButton = css`
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.08),
      rgba(255, 255, 255, 0.08)
    ),
    #010d1b;
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-shadow: inset 0px 0px 6px rgba(255, 255, 255, 0.2);
  color: rgba(251, 225, 58, 1);
  @media screen and (min-width: 1025px) {
    transition: all 0.4s linear;
    &:hover {
      border: 1px solid rgba(255, 255, 255, 0.2);
      box-shadow: inset 0px 0px 6px rgba(255, 255, 255, 0.5);
    }
  }
`;

const ghostButton = css`
  color: var(--gold);
`;

const arrowButton = css`
  padding: 0 !important;
  width: 40px;
  height: 40px;

  img,
  svg {
    width: 100%;
    height: 100%;
  }
  @media screen and (max-width: 767px) {
    width: 32px;
    height: 32px;
  }
  @media screen and (max-width: 540px) {
    width: 36px;
    height: 36px;
  }
`;

const darkGoldButton = css`
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.08),
      rgba(255, 255, 255, 0.08)
    ),
    #010d1b;
  box-shadow: inset 0px 0px 6px rgba(255, 255, 255, 0.2);
  border-radius: 99px;
  border: 1px solid gold;
  color: var(--gold);
`;

const BaseButton = styled.button<IWrapper>`
  border-radius: 100px;
  z-index: 2;
  gap: 10px;
  font-style: normal;
  display: flex;
  align-items: center;
  justify-content: center;
  white-space: nowrap;
  transition: all 0.4s linear;

  ${(props) => props.fullWidth && `width: 100%;`}
  ${(props) => props.padding && `padding: ${props.padding};`}

  & > img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  ${(props) => {
    switch (props.themeBtn) {
      case 'gray':
        return grayButton;
      case 'gold':
        return goldButton;
      case 'ghost':
        return ghostButton;
      case 'dark-gold':
        return darkGoldButton;
      case 'striped':
        return stripedButton;
      case 'arrow':
        return arrowButton;
      case 'icon':
        return iconButton;
      case 'bordered':
        return borderedButton;
    }
  }}

  ${(props) =>
    props.isIcon &&
    css`
      border-radius: 50%;
      aspect-ratio: 1 / 1;
      width: auto;
      height: auto;
    `}
    ${({ isAnimated }) =>
    isAnimated &&
    css`
      &:active {
        transform: scale(0.9);
      }
    `}
    :disabled {
    cursor: not-allowed;
    &:active {
      transform: scale(1);
    }
  }
`;

export const XLButton = styled(BaseButton)`
  ${(props) => (props.isIcon ? 'padding: 12px;' : `padding: 12px 32px;`)}
  font-weight: 700;
  font-size: 18px;
  line-height: 25px;

  @media screen and (max-width: 1024px) {
    font-size: 16px;
    line-height: 22px;
    ${(props) => (props.isIcon ? 'padding: 10px;' : `padding: 10px 32px;`)}
  }
  @supports (-webkit-touch-callout: none) {
    font-weight: 600;
  }
`;

export const LGButton = styled(BaseButton)`
  ${(props) => (props.isIcon ? 'padding: 12px;' : `padding: 6px 30px;`)}
  font-weight: 700;
  font-size: 18px;
  line-height: 25px;
  border: 1px solid transparent;

  @media screen and (max-width: 767px) {
    padding: 6px 24px;
    font-size: 14px;
    line-height: 19px;
  }
`;

export const MDButton = styled(BaseButton)`
  ${(props) => (props.isIcon ? 'padding: 14px;' : `padding: 10px 32px;`)}
  font-weight: 700;
  font-size: 16px;
  line-height: 22px;
  @media screen and (max-width: 1440px) {
    font-size: 14px;
    line-height: 18px;
    ${(props) => (props.isIcon ? 'padding: 10px;' : `padding: 10px 32px;`)}
  }
  @media screen and (max-width: 1024px) {
    ${(props) => (props.isIcon ? 'padding: 8px;' : `padding: 7.5px 24px;`)}
  }
`;

export const SMButton = styled(BaseButton)`
  ${(props) => (props.isIcon ? 'padding: 4px;' : ` padding: 4px 24px;`)}
  font-weight: 600;
  font-size: 12px;
  line-height: 14px;
`;

export const XSButton = styled(BaseButton)`
  font-weight: 800;
  font-size: 12px;
  line-height: 14px;
  padding: 8px 20px;

  @media screen and (max-width: 1440px) {
    padding: 6px 20px;
    font-weight: 800;
    font-size: 10px;
    line-height: 14px;
  }
`;
