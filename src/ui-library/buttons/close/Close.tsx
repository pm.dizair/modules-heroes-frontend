import React, { FC } from 'react';

import { IconClose } from '@/assets/icons-ts/global';

import { Wrapper } from './styles';
import { IClose } from './types';

const Close: FC<IClose> = ({ icon, onClick }) => {
  return (
    <Wrapper
      onClick={onClick}
      className="close-btn"
    >
      {icon || <IconClose />}
    </Wrapper>
  );
};

export default Close;
