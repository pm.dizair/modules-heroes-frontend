import { ReactNode } from 'react';

export interface IClose {
  onClick: () => void;
  icon?: ReactNode;
}
