import styled from 'styled-components';

export const Wrapper = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
  border-radius: 50%;
  transition: all 0.2s linear;
  svg {
    path {
      stroke: #191919;
    }
  }
`;
