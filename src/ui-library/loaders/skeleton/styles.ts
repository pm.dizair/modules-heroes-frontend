import React from 'react';

export const fillStyle: React.CSSProperties = {
  position: 'absolute',
  inset: 0,
  width: '100%',
  height: '100%',
};

export const defaultStyle: React.CSSProperties = {};
