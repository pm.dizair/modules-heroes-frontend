import { SkeletonProps } from 'react-loading-skeleton';

export interface INeonSkeletonProps extends SkeletonProps {
  fill?: boolean;
  theme?: 'default' | 'gothic';
}
