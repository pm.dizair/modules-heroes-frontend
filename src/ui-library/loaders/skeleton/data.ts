export const THEMES = {
  default: {
    baseColor: '#02152a',
    highlightColor: '#1f3a73',
  },
  gothic: {
    baseColor: ' #29333F',
    highlightColor: '#1c222a',
  },
};
