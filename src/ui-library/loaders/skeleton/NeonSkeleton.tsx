import { FC } from 'react';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

import { THEMES } from './data';
import { defaultStyle, fillStyle } from './styles';
import { INeonSkeletonProps } from './types';

const NeonSkeleton: FC<INeonSkeletonProps> = (props) => {
  const { fill, theme = 'default' } = props;

  return (
    <Skeleton
      {...props}
      style={fill ? fillStyle : defaultStyle}
      {...THEMES[theme]}
    />
  );
};

export default NeonSkeleton;
