import React, { FC } from 'react';

import { Icon, Loader } from './styles';
import { ICircleLoader } from './types';

const CircleLoader: FC<ICircleLoader> = ({ gold = false }) => {
  return (
    <Loader>
      <Icon gold={gold} />
    </Loader>
  );
};

export default CircleLoader;
