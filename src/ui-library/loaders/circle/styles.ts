import styled, { keyframes } from 'styled-components';

import { ICircleLoader } from './types';

export const Spin = keyframes`

  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const Loader = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: transparent;
`;

export const Icon = styled.div<ICircleLoader>`
  border-radius: 50%;
  width: 24px;
  height: 24px;
  border: 2px solid
    ${(props) =>
      props.gold ? 'rgba(251, 225, 58, 0.2)' : 'rgba(1, 13, 27, 0.2)'};
  border-top-color: ${(props) =>
    props.gold ? 'rgb(251, 225, 58)' : 'rgb(1, 13, 27)'};
  animation: ${Spin} 1s infinite linear;
  @media screen and (max-width: 1024px) {
    width: 22px;
    height: 22px;
  }
`;
