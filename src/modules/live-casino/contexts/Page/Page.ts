import { usePageContext } from '@/common/components/providers';
import { ILiveCasinoPageProps } from '@/pages/live-casino';

export const useLiveCasinoPageProps = () =>
  usePageContext<ILiveCasinoPageProps>();
