import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { useCommon } from '@/common/store/common/common.slice';
import { FC, useState } from 'react';

import backgroundImage from '@/assets/images/backgrounds/LiveCasinoBg.webp';
import backgroundMobileImage from '@/assets/images/backgrounds/LiveCasinoMobileBg.webp';
import { PromotionBanner } from '@/common/components/shared';
import { useLanguageChange, useTranslations } from '@/common/hooks';
import { GoldTitle } from '@/ui-library/title';
import { useGetRandomPromotionQuery } from '@/utils/api/rtk-query/promotions/promotions';

import imageBandit from '@/assets/images/heroes/persons/Outlaw.webp';
import LiveCasinoList from './components/LiveCasinoList/LiveCasinoList';
import { useLiveCasinoPageProps } from './contexts/Page/Page';
import { Wrapper } from './styles';
import { ILiveCasino } from './types';

const LiveCasino: FC<ILiveCasino> = () => {
  const { promotion } = useLiveCasinoPageProps();
  const { brands, language } = useCommon();
  const t = useTranslations();

  const [skip, setSkip] = useState<boolean>(true);

  const { data } = useGetRandomPromotionQuery(
    {
      language,
      position: 'main_page',
    },
    { skip }
  );
  useLanguageChange(() => setSkip(false));

  return (
    <>
      <PromotionBanner
        backgroundImageMobile={backgroundMobileImage}
        promotion={data || promotion.data}
        backgroundImage={backgroundImage}
        $isLive
        imagePersona={imageBandit}
      />
      <MainContainer>
        <Wrapper>
          <GoldTitle size="lg">{t('main_page_live_casino')}</GoldTitle>
          <LiveCasinoList brands={brands} />
        </Wrapper>
      </MainContainer>
    </>
  );
};
export default LiveCasino;
