import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 20px 0 80px 0;
  .gold-title {
    width: 100%;
    display: inline-block;
    text-align: center;
    margin-bottom: 40px;
    font-size: 56px;
    line-height: 40px;
  }
  @media screen and (max-width: 1440px) {
    padding: 20px 0 80px 0;
    .gold-title {
      font-size: 56px;
      line-height: 40px;
      margin-bottom: 32px;
    }
  }
  @media screen and (max-width: 767px) {
    padding: 24px 0 80px 0;
    .gold-title {
      font-size: 40px;
    }
  }
  @media screen and (max-width: 540px) {
    padding: 0 0 80px 0;
    .gold-title {
      margin-bottom: 24px;
    }
  }
`;
export const AllGamesWrapper = styled.div`
  margin-top: 32px;
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 767px) {
    margin-top: 24px;
  }
`;
