import { withPageProvider } from '@/common/helpers/tests/providers/providers';
import { IGamesPageProps } from '@/pages/games';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { translations } from '@/common/helpers/tests/mocks/translation';
import LiveCasino from './LiveCasino';
import { gamesResponse, mockPromotion } from './mock';

const withProvider = (Story: StoryFn) =>
  withPageProvider<IGamesPageProps>(
    { promotion: mockPromotion, translation: translations },
    Story
  );

export const Base: StoryObj<typeof LiveCasino> = {};

Base.play = async ({ canvasElement, step }) => {
  await step('check games length from api', async () => {
    const canvas = within(canvasElement);
    const gameCards = await canvas.findAllByTestId('game-card');

    expect(gameCards).toHaveLength(gamesResponse.items.length);
  });
};

const meta: Meta<typeof LiveCasino> = {
  title: 'Modules/LiveCasino',
  component: LiveCasino,
  tags: ['autodocs'],
  decorators: [withProvider],
  args: {
    promotion: mockPromotion,
  },
  parameters: {
    mockData: [
      {
        url: 'https://api2.dizair.ee/game/list/?limit=20&page=1',
        method: 'GET',
        status: 200,
        response: gamesResponse,
      },
    ],
  },
};

export default meta;
