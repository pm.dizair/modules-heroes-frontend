import { NAutoload } from '@/common/components/autoloader/autoload/v4.0/types';
import { IGame } from '@/common/types';

export interface IGamesFetchResponse {
  items: IGame[];
  total: number;
}

export interface ILiveListProps {
  fetch: NAutoload.TFetchFunction<{ tab: string }>;
}
