import AutoLoadModuleV4 from '@/common/components/autoloader/autoload/v4.0/Autoload';
import { NAutoload } from '@/common/components/autoloader/autoload/v4.0/types';
import { AutoLoadGameCard, GameCardLoader } from '@/common/components/cards';
import { IGame } from '@/common/types';
import { FC } from 'react';

import { NotFound } from '@/common/components/shared';
import { useRouter } from 'next/router';
import { autoloadModuleSlice } from '../LiveCasinoList/slice';
import { ILiveListProps } from './type';

const LiveList: FC<ILiveListProps> = (props) => {
  const { fetch } = props;
  const router = useRouter();
  const { tab = 'live' } = router.query;

  const config = {
    listStyle: {
      gap: '16px',
    },
    reduxSlice: autoloadModuleSlice,
    className: 'virtual-list',
    fetch,
    preloadRows: 12,
    columns: {
      767: 2,
      1440: 4,
      default: 6,
    },
    rows: {
      767: 6,
      default: 5,
    },
  };

  const renderItems = (items: NAutoload.TRenderItem[]) =>
    items.map(({ item, observerParams }, index: number) => {
      if (item) {
        return (
          <AutoLoadGameCard
            observerParams={observerParams}
            key={item.id}
            item={item as IGame}
            // favorite
          />
        );
      }
      return <GameCardLoader key={-index} />;
    });

  return (
    <>
      <AutoLoadModuleV4
        config={config}
        states={{ tab: `live-casino/${tab.toString()}` }}
      >
        {(items) => (items.length ? renderItems(items) : <NotFound />)}
      </AutoLoadModuleV4>
    </>
  );
};

export default LiveList;
