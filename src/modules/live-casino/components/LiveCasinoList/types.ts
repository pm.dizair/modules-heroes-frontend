import { IBrand, IGame } from '@/common/types';

export interface ILiveCasinoList {
  brands: IBrand[];
}

export interface IFetchLiveCasinoPayload {
  page: number;
  limit: number;
  states: { tab: string };
}

export interface IFetchResponse {
  total: number;
  items: IGame[];
}
