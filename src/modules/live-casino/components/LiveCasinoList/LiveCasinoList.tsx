import { useTranslations } from '@/common/hooks';
import { Button } from '@/ui-library/buttons';
import { useLazyGetGamesQuery } from '@/utils/api/rtk-query';
import { FC, useRef } from 'react';

import { NotFound } from '@/common/components/shared';
import { LINK_TEMPLATES } from '@/common/const';
import { IGame } from '@/common/types';
import { useRouter } from 'next/router';
import { AllGamesWrapper } from '../../styles';
import LiveList from '../LiveList/LiveList';
import { LIVE_CASINO_TABS } from './const';
import {
  IFetchLiveCasinoPayload,
  IFetchResponse,
  ILiveCasinoList,
} from './types';

const LiveCasinoList: FC<ILiveCasinoList> = (props) => {
  const { brands } = props;
  const [getGames] = useLazyGetGamesQuery();
  const t = useTranslations();
  const router = useRouter();
  const contentRef = useRef<string>('');

  const { tab = 'live' } = router.query;

  const fetchLiveCasino = async ({
    page,
    limit,
    states,
  }: IFetchLiveCasinoPayload) => {
    const body = {
      category: 'Live casino',
      limit,
      page,
    };

    if (
      states.tab === LIVE_CASINO_TABS.ALL &&
      page === 1 &&
      contentRef.current
    ) {
      const response = JSON.parse(contentRef.current || '{}');
      return response as IFetchResponse;
    }

    const res = await getGames(body);
    const data = res.data;

    const response = {
      total: data?.items?.length!,
      items: data?.items as IGame[],
    };

    if (states.tab === LIVE_CASINO_TABS.LIVE) {
      contentRef.current = JSON.stringify(data);
    }

    let resp =
      states.tab === LIVE_CASINO_TABS.ALL ? (data as IFetchResponse) : response;

    return resp;
  };

  if (!Array.isArray(brands) || !brands?.length) {
    return <NotFound />;
  }

  return (
    <>
      <LiveList fetch={fetchLiveCasino} />

      {tab === 'live' && (
        <AllGamesWrapper>
          <Button
            theme="dark-gold"
            onClick={() => {
              router.push(
                LINK_TEMPLATES.LIVE_CASINO_LISTING({ tab: 'all' }),
                undefined,
                { shallow: true }
              );
            }}
          >
            {t('games_page_all_games_button')}
          </Button>
        </AllGamesWrapper>
      )}
    </>
  );
};

export default LiveCasinoList;
