import { initializeAutoloadSlice } from '@/common/components/autoloader/autoload/v4.0/services';
import { CACHED_SLICES } from '@/utils/store/const';

export const autoloadModuleSlice = initializeAutoloadSlice(
  CACHED_SLICES.GAMES_LIST
);
