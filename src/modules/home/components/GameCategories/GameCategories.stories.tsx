import '@/styles/scss/index.scss';
import { Meta, StoryObj } from '@storybook/react';

import GameCategories from './GameCategories';
import { categoriesMap } from './mocks';

export const Categories: StoryObj<typeof GameCategories> = {
  args: { categories: categoriesMap },
};
const meta: Meta<typeof GameCategories> = {
  title: 'Modules/Home/GameCategories',
  component: GameCategories,
  tags: ['autodocs'],
};

export default meta;
