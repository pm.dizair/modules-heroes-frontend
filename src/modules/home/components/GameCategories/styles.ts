import styled from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.center}
`;

export const List = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: fit-content;
  max-width: 100%;
  gap: 30px;
  padding: 0 120px 0 120px;
  @media screen and (max-width: 1920px) {
    padding: 0 6.3vw;
  }
  @media screen and (max-width: 1440px) {
    padding: 0 7vw;
  }
  @media screen and (max-width: 1280px) {
    padding: 0 40px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }
  ${({ theme }) => theme.content.hiddenScrollHorizontal}
`;
