import { useScrollContainer } from '@/common/hooks';
import { useRouter } from 'next/router';
import { FC, useState } from 'react';

import CategoryItem from './components/CategoryItem/CategoryItem';
import { List, Wrapper } from './styles';
import { IGameCategories } from './types';

const GameCategories: FC<IGameCategories> = (props) => {
  const { categories } = props;
  const [active, setActive] = useState<number>();

  const router = useRouter();
  const { category = '' } = router.query;

  const { scrollContainer, itemRef } = useScrollContainer<number | string>(
    category.toString() || ''
  );

  const renderCategoryItems = () =>
    (categories || [])?.map((category) => (
      <CategoryItem
        category={category}
        active={active as number}
        setActive={setActive}
        ref={itemRef}
        key={category.position}
      />
    ));

  return (
    <Wrapper>
      <List ref={scrollContainer}>{renderCategoryItems()}</List>
    </Wrapper>
  );
};

export default GameCategories;
