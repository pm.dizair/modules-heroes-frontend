import { LINK_TEMPLATES } from '@/common/const';
import { useRouter } from 'next/router';
import React, { forwardRef } from 'react';

import { getImage } from '@/common/helpers/getImage';
import { printCategory } from './helpers';
import { Item, Name, Picture } from './styles';
import { ICategoryItem } from './types';

const CategoryItem = forwardRef<HTMLButtonElement, ICategoryItem>(
  (props, ref) => {
    const router = useRouter();
    const { category, setActive } = props;
    const { category: cat } = router.query;

    const pushToCategory = () => {
      setActive(category.position);
      router.push(
        LINK_TEMPLATES.GAME_LISTING({
          category: category.slug || '',
        })
      );
    };

    return (
      <Item
        active={category.slug === cat}
        ref={category.slug === cat ? ref : null}
        key={category.position}
        onClick={pushToCategory}
      >
        <Picture
          placeholder="blur"
          src={getImage(category.image)}
          blurDataURL={getImage(category.image)}
          alt={category.name}
          width={50}
          height={50}
          onError={() => printCategory(category)}
        />
        <Name active={cat === category.slug}>{category.name}</Name>
      </Item>
    );
  }
);

CategoryItem.displayName = 'CategoryItem';

export default CategoryItem;
