import { ITab } from '@/common/types';

export const printCategory = (category?: ITab) => {
  console.log(`category slug: ${category?.slug} name: ${category?.name}`);
};
