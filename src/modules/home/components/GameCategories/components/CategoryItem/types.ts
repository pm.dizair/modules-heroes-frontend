import { ITab } from '@/common/types';

export interface ICategoryItem {
  active: number;
  category: ITab;
  setActive: (category: number) => void;
}

export interface IActive {
  active: boolean;
}
