import Image from 'next/image';
import styled from 'styled-components';

import { IActive } from './types';

export const Item = styled.button<IActive>`
  ${({ theme }) => theme.content.flex.center}
  flex-direction: column;
  gap: 6px;

  @media screen and (min-width: 1024px) {
    transition: ${(props) => props.theme.content.transition};
  }
`;

export const Name = styled.span<IActive>`
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  white-space: nowrap;
  ${({ theme }) => theme.text.overflow}
  max-width: 100px;
  text-align: center;
  color: ${(props) => (props.active ? 'var(--gold)' : '#fff')};

  &:hover {
    color: var(--gold);
  }
`;
export const Picture = styled(Image)`
  width: 90px;
  height: 90px;
  aspect-ratio: 1 / 1;
`;
