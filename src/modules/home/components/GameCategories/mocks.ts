import { ITab } from '@/common/types';

import ImageCategory1 from '@/assets/images/temp/categories/1.png';
import ImageCategory10 from '@/assets/images/temp/categories/10.png';
import ImageCategory11 from '@/assets/images/temp/categories/11.png';
import ImageCategory2 from '@/assets/images/temp/categories/2.png';
import ImageCategory3 from '@/assets/images/temp/categories/3.png';
import ImageCategory4 from '@/assets/images/temp/categories/4.png';
import ImageCategory5 from '@/assets/images/temp/categories/5.png';
import ImageCategory6 from '@/assets/images/temp/categories/6.png';
import ImageCategory7 from '@/assets/images/temp/categories/7.png';
import ImageCategory8 from '@/assets/images/temp/categories/8.png';
import ImageCategory9 from '@/assets/images/temp/categories/9.png';

export const categoriesMap: ITab[] = [
  {
    name: 'Slots',
    slug: 'slots',
    image: ImageCategory1.src,
    mobile_image: ImageCategory1.src,
    position: 1,
  },
  {
    name: 'New',
    slug: 'new',
    image: ImageCategory2.src,
    mobile_image: ImageCategory2.src,
    position: 2,
  },
  {
    name: 'Hot',
    slug: 'hot',
    image: ImageCategory3.src,
    mobile_image: ImageCategory3.src,
    position: 3,
  },
  {
    name: 'Roulette',
    slug: 'roulette',
    image: ImageCategory4.src,
    mobile_image: ImageCategory4.src,
    position: 4,
  },
  {
    name: 'Table Games',
    slug: 'table-games',
    image: ImageCategory5.src,
    mobile_image: ImageCategory5.src,
    position: 5,
  },
  {
    name: 'Favorite',
    slug: 'favorite',
    image: ImageCategory6.src,
    mobile_image: ImageCategory6.src,
    position: 6,
  },
  {
    name: 'Promo',
    slug: 'promo',
    image: ImageCategory7.src,
    mobile_image: ImageCategory7.src,
    position: 7,
  },
  {
    name: 'Popular',
    slug: 'popular',
    image: ImageCategory8.src,
    mobile_image: ImageCategory8.src,
    position: 8,
  },
  {
    name: 'Fast Games',
    slug: 'fast-games',
    image: ImageCategory9.src,
    mobile_image: ImageCategory9.src,
    position: 9,
  },
  {
    name: 'Live',
    slug: 'live',
    image: ImageCategory10.src,
    mobile_image: ImageCategory10.src,
    position: 10,
  },
  {
    name: 'Tourneys',
    slug: 'tourneys',
    image: ImageCategory11.src,
    mobile_image: ImageCategory11.src,
    position: 11,
  },
];
