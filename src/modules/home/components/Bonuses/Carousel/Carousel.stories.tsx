import '@/styles/scss/index.scss';
import { store } from '@/utils/store';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { Provider } from 'react-redux';

import { bonuses } from '../mocks';
import BonusCarousel from './Carousel';

const withProvider = (Story: StoryFn) => (
  <Provider store={store}>{<Story />}</Provider>
);
const meta: Meta<typeof BonusCarousel> = {
  title: 'COMPONENTS/Sliders/BonusCarousel',
  component: BonusCarousel,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;
export const Bonuses: StoryObj<typeof BonusCarousel> = {
  args: { bonusData: bonuses },
};
