export const standardConfigCoverflow = {
  rotate: 0,
  stretch: -15,
  depth: 120,
  modifier: 5,
  slideShadows: false,
};

export const breakpoints = {
  0: {
    coverflowEffect: standardConfigCoverflow,
  },
  375: {
    coverflowEffect: {
      stretch: -10,
    },
  },
  400: {
    coverflowEffect: {
      stretch: -5,
    },
  },
  450: {
    coverflowEffect: {
      stretch: 0,
    },
  },
};
