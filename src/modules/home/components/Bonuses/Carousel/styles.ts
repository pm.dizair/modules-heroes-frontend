import styled from 'styled-components';

export const Arrow = styled.button`
  position: absolute;
  width: 60px;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 50%;
  z-index: 10;
  transform-origin: center;
  transition: all 400ms linear;
  transform: translateY(-50%);
  svg {
    width: 32px;
    height: 44px;
  }
`;
export const SwiperContainer = styled.div`
  width: 100%;
  overflow: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: none;
  ::-webkit-scrollbar {
    width: 0;
    height: 0;
  }
  .slider_container {
    width: calc(100% + 1px);
  }
`;

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  .left-arrow {
    left: 0px;
    transform: translateY(-50%) rotateY(180deg);
  }
  .right-arrow {
    right: 0px;
  }
  .swiper-slide {
    display: flex;
    transform: translate3d(0, 0, 0);
    align-items: center;
    justify-content: center;
    position: relative;
    height: 400px;
    @media screen and (max-width: 540px) {
      filter: blur(2px);
      &.swiper-slide-active {
        filter: none;
      }
    }
  }
`;
