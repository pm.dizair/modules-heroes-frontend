import { useIsSmallMobile } from '@/common/hooks';
import { FC, useState } from 'react';
import { EffectCoverflow, Navigation } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Swiper as SwiperClass } from 'swiper/types';

import { IconArrowNext } from '@/assets/icons-ts/global';

import ShieldCard from '../Card/Card';
import { breakpoints, standardConfigCoverflow } from './settings';
import { Arrow, SwiperContainer, Wrapper } from './styles';
import { IBonusCarousel } from './types';

const BonusCarousel: FC<IBonusCarousel> = (props) => {
  const { bonusData } = props;
  const slidesPerView = 2;
  const LENGTH = bonusData?.length;
  const isMobile = useIsSmallMobile();
  const isCoverFlow = LENGTH > slidesPerView && isMobile;
  const [swiperRef, setSwiperRef] = useState<SwiperClass>();
  // this need because swiper needs more as match than slidesPerView
  const dataSlides =
    slidesPerView * 2 > LENGTH ? bonusData.concat(bonusData) : bonusData;

  const handlePrevious = () => {
    return swiperRef?.slidePrev();
  };

  const handleNext = () => {
    return swiperRef?.slideNext();
  };

  const renderDataSlides = () =>
    dataSlides.map((promotion) => (
      <SwiperSlide key={promotion.id}>
        <ShieldCard promotion={promotion} />
      </SwiperSlide>
    ));

  return (
    <Wrapper>
      {isCoverFlow && (
        <Arrow
          title="prev"
          type="button"
          className="left-arrow"
          onClick={handlePrevious}
        >
          <IconArrowNext fill="rgba(251, 225, 58, 1)" />
        </Arrow>
      )}
      <SwiperContainer>
        <Swiper
          onSwiper={setSwiperRef}
          slidesPerView={isCoverFlow ? slidesPerView : 1}
          spaceBetween={0}
          loop={LENGTH > 1}
          speed={600}
          effect={isCoverFlow ? 'coverflow' : 'slide'}
          allowTouchMove={LENGTH > 1}
          allowSlideNext
          allowSlidePrev
          longSwipes={false}
          loopPreventsSliding={false}
          centeredSlides
          breakpoints={breakpoints}
          direction="horizontal"
          scrollbar={{ draggable: true }}
          className={'slider_container'}
          modules={[Navigation, EffectCoverflow]}
          coverflowEffect={standardConfigCoverflow}
        >
          {renderDataSlides()}
        </Swiper>
      </SwiperContainer>
      {isCoverFlow && (
        <Arrow
          title="next"
          type="button"
          className="right-arrow"
          onClick={handleNext}
        >
          <IconArrowNext fill="rgba(251, 225, 58, 1)" />
        </Arrow>
      )}
    </Wrapper>
  );
};

export default BonusCarousel;
