import { IPromotion } from '@/common/types';

export interface IBonusCarousel {
  bonusData: IPromotion[];
}
