import { IPromotion } from '@/common/types';

export const promotion: IPromotion = {
  id: 1,
  title: 'Welcome Pack',
  description:
    'Celebrate your birthday with us. We give gifts to all birthdays!',
  button_text: '',
  sub_title: 'Celebrate your birthday with us. We give gifts to all birthdays!',
};
