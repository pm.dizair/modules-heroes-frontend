import Image from 'next/image';
import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 250px;
  height: 390px;
  position: relative;
  z-index: 1;
  padding: 60px 24px 40px 24px;
  ${({ theme }) => theme.content.flex.column};
  align-items: center;
  justify-content: space-between;
  gap: 12px;
  .color-button {
    text-transform: uppercase;
    width: fit-content;
    font-weight: 800;
    padding: 6px 32px;
  }
  @media screen and (max-width: 1620px) {
    width: 196px;
    height: 310px;
    padding: 45px 24px 36px 24px;
    .color-button {
      padding: 7px 24px;
      font-size: 10px;
    }
  }
  @media screen and (max-width: 1024px) {
    min-width: 165px;
  }
  @media screen and (max-width: 767px) {
    width: 220px;
    padding: 60px 25px 50px 25px;
  }
  @media screen and (max-width: 540px) {
    width: 240px;
    height: 385px;
    padding: 60px 30px 40px 30px;
    margin: auto;
    position: absolute;
    top: 0;
    left: 50%;
    transform: translateX(-50%);
  }
`;

export const Text = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 160%;
  text-align: center;
  color: #ffffff;
  ${({ theme }) => theme.text.getLineClamp(5)}

  @media screen and (max-width: 1620px) {
    font-size: 14px;
  }
  @media screen and (max-width: 767px) {
    font-size: 12px;
    ${({ theme }) => theme.text.getLineClamp(4)}
  }
  @media screen and (max-width: 540px) {
    ${({ theme }) => theme.text.getLineClamp(6)}

    font-size: 16px;
  }
`;

export const Indent = styled.div`
  background: linear-gradient(
    90deg,
    rgba(251, 225, 58, 0) 0%,
    rgba(251, 225, 58, 0.5044) 20.15%,
    #b3261e 49.59%,
    rgba(251, 225, 58, 0.453167) 81.46%,
    rgba(251, 225, 58, 0) 100%
  );
  width: 100%;
  min-height: 1px;
  margin-top: 12px;
  margin-bottom: 16px;
`;

export const CardBg = styled(Image)`
  position: absolute;
  width: 100%;
  height: fit-content;
  object-fit: fill;
  left: 0;
  top: 0;
  z-index: -1;
`;

export const TextWrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 75px;
  .gold-title {
    text-align: center;
    ${({ theme }) => theme.text.getLineClamp(2)}
    font-weight: 700;
    font-size: 26px;
    line-height: 38px;
  }
  @media screen and (max-width: 1620px) {
    height: 54px;
    .gold-title {
      font-size: 20px;
      line-height: 30px;
    }
  }
  @media screen and (max-width: 767px) {
    height: 48px;
    .gold-title {
      font-size: 18px;
      line-height: 23px;
    }
  }
  @media screen and (max-width: 540px) {
    .gold-title {
      font-size: 24px;
      line-height: 32px;
    }
  }
`;
export const Flex = styled.div`
  ${({ theme }) => theme.content.flex.column};
  flex-grow: 1;
  flex-shrink: 0;
`;
