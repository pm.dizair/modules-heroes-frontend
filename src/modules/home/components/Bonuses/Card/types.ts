import { IPromotion } from '@/common/types';

export interface IShieldCard {
  promotion: IPromotion;
}
