import '@/styles/scss/index.scss';
import { store } from '@/utils/store';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { Provider } from 'react-redux';

import ShieldCard from './Card';
import { promotion } from './mock';

const withProvider = (Story: StoryFn) => (
  <Provider store={store}>{<Story />}</Provider>
);
const meta: Meta<typeof ShieldCard> = {
  title: 'COMPONENTS/Cards/ShieldCard',
  component: ShieldCard,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;

export const Shield: StoryObj<typeof ShieldCard> = {
  args: {
    promotion,
  },
};
