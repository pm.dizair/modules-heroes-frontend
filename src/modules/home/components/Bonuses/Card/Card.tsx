import { Button } from '@/ui-library/buttons';
import { GoldTitle } from '@/ui-library/title';
import { FC } from 'react';

import CardSmallImage from '@/assets/icons/BonusCardSmall.svg';

import { CardBg, Flex, Indent, Text, TextWrap, Wrapper } from './styles';
import { IShieldCard } from './types';

const ShieldCard: FC<IShieldCard> = (props) => {
  const { promotion } = props;
  const { sub_title, title, button_text } = promotion;

  return (
    <Wrapper>
      <CardBg
        src={CardSmallImage}
        alt="card"
        sizes="240px"
        placeholder="empty"
        fill
        priority
      />
      <Flex>
        <TextWrap>
          <GoldTitle size="md">{title}</GoldTitle>
        </TextWrap>
        <Indent />
        <Text>{sub_title}</Text>
      </Flex>
      <Button size="sm">{button_text}</Button>
    </Wrapper>
  );
};

export default ShieldCard;
