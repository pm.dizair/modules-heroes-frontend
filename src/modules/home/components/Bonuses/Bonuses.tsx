import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { DefaultError } from '@/common/components/errors';
import { REQUEST_LIMITS } from '@/common/const';
import { useAppDispatch, useLanguageChange } from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { Button } from '@/ui-library/buttons';
import { GoldTitle } from '@/ui-library/title';
import { getKey } from '@/utils/api/rtk-query/helpers';
import { useGetPromotionsQuery } from '@/utils/api/rtk-query/promotions/promotions';
import {
  LittleMobileOff,
  LittleMobileOn,
  MobileOff,
} from '@/utils/lib/responsive/responsive';
import { useEffect, useState } from 'react';

import { IconArrowNext } from '@/assets/icons-ts/global';
import ImageBgBonuses from '@/assets/images/backgrounds/Bonus.webp';
import ImageLeftChest from '@/assets/images/chests/BonusLeft.png';
import ImageRightChest from '@/assets/images/chests/BonusRight.png';

import { useHomepageContext } from '../../context';
import Card from './Card/Card';
import BonusCarousel from './Carousel/Carousel';
import { cacheBonusesAction } from './slice';
import { Background, Chests, Content, Flex, Present, Wrapper } from './styles';

const Bonuses = () => {
  const {
    bonusPromotions: {
      items: promotions = [],
      isError: isServerError,
      error: serverError,
    },
  } = useHomepageContext();
  const dispatch = useAppDispatch();
  // TODO: need translation
  const { language } = useCommon();

  const [skip, setSkip] = useState<boolean>(true);
  useLanguageChange(() => setSkip(false));

  const args = {
    language,
    page: 1,
    limit: REQUEST_LIMITS.BONUS_PROMOTIONS,
  };

  const {
    isError,
    error,
    data: { items } = {},
  } = useGetPromotionsQuery(args, { skip });

  useEffect(() => {
    dispatch(cacheBonusesAction({ key: getKey(args), data: promotions }));
  }, []);

  const renderBonusPromotions = () =>
    (items || promotions)?.map((promotion) => (
      <Card
        promotion={promotion}
        key={promotion.id}
      />
    ));

  if (isServerError) {
    return <DefaultError error={`serverError: ${serverError}`} />;
  } else if (isError && 'data' in error!) {
    return <DefaultError error={`error: ${error.data}`} />;
  }

  if (!items?.length && !promotions?.length) {
    return null;
  }

  return (
    <Wrapper>
      <Background
        alt="bonus fon"
        placeholder="empty"
        priority
        sizes="(min-width: 1024px) 100vw, 800px"
        src={ImageBgBonuses}
      />
      <Content>
        <MainContainer>
          <GoldTitle size="xl">We have something interesting for you</GoldTitle>
        </MainContainer>
        <LittleMobileOff>
          <Flex>{renderBonusPromotions()}</Flex>
        </LittleMobileOff>
        <LittleMobileOn>
          <BonusCarousel bonusData={items || promotions} />
        </LittleMobileOn>
        <MainContainer>
          <Button
            isAnimated={false}
            theme="striped"
            className="all-btn"
          >
            Show all bonuses <IconArrowNext />
          </Button>
        </MainContainer>
      </Content>
      <MobileOff>
        <MainContainer className="chest-container">
          <Chests>
            <Present
              src={ImageLeftChest}
              alt=""
              placeholder="empty"
              sizes="(max-width: 1440px) 400px, (max-width: 767px) 275px, 500px"
            />
            <Present
              src={ImageRightChest}
              alt=""
            />
          </Chests>
        </MainContainer>
      </MobileOff>
    </Wrapper>
  );
};

export default Bonuses;
