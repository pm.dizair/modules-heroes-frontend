import { getCacheSlice } from '@/utils/store/helpers/getCachedSlice';

export const bonusesSlice = getCacheSlice('bonus', {});

export const cacheBonusesAction = bonusesSlice.actions.cacheData;
