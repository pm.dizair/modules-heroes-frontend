import Image from 'next/image';
import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  ${({ theme }) => theme.content.flex.column};
  height: calc(100vh - 80px);
  max-height: 700px;
  min-height: 600px;

  justify-content: space-between;
  .chest-container {
    height: fit-content;
  }
  @media screen and (max-width: 1024px) {
    max-height: 600px;
    min-height: 500px;
  }
  @media screen and (max-width: 540px) {
    height: fit-content;
  }
`;

export const Background = styled(Image)`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  height: 100%;
  width: fit-content;
  min-width: 100%;
  @media screen and (max-width: 1024px) {
    height: 100%;
    top: 0;
    width: 100%;
    object-fit: cover;
  }
`;

export const Chests = styled.div`
  position: absolute;
  bottom: -60px;
  left: 55%;
  width: 100%;
  transform: translateX(-55%);
  height: 400px;
  ${({ theme }) => theme.content.flex.between}
  max-width: 1920px;
  @media screen and (max-width: 1440px) {
    height: 300px;
  }
  @media screen and (max-width: 1024px) {
    height: 270px;
    bottom: -40px;
  }
  @media screen and (max-width: 540px) {
    height: 180px;
  }
`;

export const Present = styled(Image)`
  height: 100%;
  object-fit: contain;
  width: 400px;
  @media screen and (max-width: 1440px) {
    width: 300px;
  }
  @media screen and (max-width: 540px) {
    width: 180px;
  }
`;

export const Content = styled.div`
  ${({ theme }) => theme.content.flex.column};
  justify-content: center;
  align-items: flex-start;
  text-align: center;
  gap: 40px;
  .gold-title {
    max-width: 850px;
  }
  .main-container {
    display: flex;
    align-items: center;
    justify-content: center;

    .all-btn {
      padding: 8px 20px;
      width: fit-content;
    }
  }
  @media screen and (max-width: 1620px) {
    .gold-title {
      max-width: 650px;
    }
  }
  @media screen and (max-width: 1024px) {
    .gold-title {
      max-width: 500px;
    }
    .all-btn {
      font-size: 14px;
      line-height: 20px;
    }
  }
`;

export const Flex = styled.div`
  width: 100%;
  gap: 120px;
  ${({ theme }) => theme.content.flex.center};
  @media screen and (max-width: 1620px) {
    gap: 80px;
  }
  @media screen and (max-width: 1024px) {
    /* .card-bonus {
      min-width: 165px;
    } */
    padding: 0 16px;
    gap: 16px;
    justify-content: center;
  }
`;
