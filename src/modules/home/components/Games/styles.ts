import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.column}
`;

export const Content = styled.div`
  ${({ theme }) =>
    css`
      ${theme.content.flex.center};
      ${theme.content.hiddenScrollHorizontal};
      ${theme.content.mainContainerPadding};
      ${theme.content.mainContainerWidth}
    `}
  gap: 24px;
  width: 100%;
  justify-content: flex-start;
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  margin-top: 16px;
  padding-bottom: 12px !important;

  @media screen and (max-width: 1440px) {
    gap: 16px;
    display: grid;
    grid-template-columns: repeat(6, 1fr);
  }

  @media screen and (max-width: 1024px) {
    grid-template-columns: repeat(6, 180px);
  }

  @media screen and (max-width: 767px) {
    margin-top: 12px;
    grid-template-columns: repeat(6, 160px);
  }
`;

export const HeaderWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonWrapper = styled.div`
  ${({ theme }) => theme.content.flex.center};
  width: 100%;
  padding: 0px 16px;
  margin-top: 12px;
`;
