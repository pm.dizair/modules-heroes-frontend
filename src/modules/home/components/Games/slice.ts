import { getCacheSlice } from '@/utils/store/helpers/getCachedSlice';

export const gameSlice = getCacheSlice('game', {});

export const cacheGameAction = gameSlice.actions.cacheData;
