import { GameCard, GameCardLoader } from '@/common/components/cards';
import { DefaultError } from '@/common/components/errors';
import { BasicTabs } from '@/common/components/tabs';
import { REQUEST_LIMITS } from '@/common/const';
import {
  useAppDispatch,
  useTranslations,
  useValueChange,
} from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { ITab } from '@/common/types/components';
import { IGame } from '@/common/types/game';
import { Button } from '@/ui-library/buttons';
import { useGetGamesQuery } from '@/utils/api/rtk-query';
import { getKey } from '@/utils/api/rtk-query/helpers';
import { TabletOn } from '@/utils/lib/responsive/responsive';
import { FC, useEffect, useState } from 'react';

import { LINK_TEMPLATES } from '@/common/const';
import { useRouter } from 'next/router';
import { useHomepageContext } from '../../context';
import { cacheGameAction } from './slice';
import { ButtonWrapper, Content, Wrapper } from './styles';
import { IGamesProps } from './types';

const Games: FC<IGamesProps> = () => {
  const {
    popularGames: {
      items: initialGames,
      isError: isServerError,
      error: serverError,
    } = {},
  } = useHomepageContext();

  const t = useTranslations();
  const { categories } = useCommon();
  const dispatch = useAppDispatch();

  const [skip, setSkip] = useState<boolean>(true);
  const [category, setCategory] = useState<ITab>(categories?.[0]);
  const router = useRouter();

  const args = {
    category: category?.slug,
    page: 1,
    limit: REQUEST_LIMITS.POPULAR_GAMES,
  };

  const { data, isLoading, isFetching, isError, error } = useGetGamesQuery(
    args,
    { skip }
  );
  useValueChange(category?.slug, () => setSkip(false));

  const pushToGames = () => {
    router.push(LINK_TEMPLATES.GAME_LISTING({ category: category.slug }));
  };

  useEffect(() => {
    if (category?.slug === categories?.[0]?.slug) {
      dispatch(cacheGameAction({ key: getKey(args), data: initialGames }));
    }
  }, [category]);

  useEffect(() => {
    if (categories?.length) {
      setCategory(categories[0]);
    }
  }, [categories?.length]);

  const renderPreloader = () =>
    [...Array(12)].map((_, index) => <GameCardLoader key={index} />);

  const renderGameCards = () =>
    (data?.items || initialGames)?.map((game: IGame) => (
      <GameCard
        game={game}
        key={game.id}
      />
    ));

  if (isServerError || isError) {
    return <DefaultError error={`server:${serverError} client:${error}`} />;
  }

  return (
    <Wrapper>
      <BasicTabs
        selectedTab={category}
        setSelectedTab={setCategory}
        tabs={categories}
        onClickArrow={pushToGames}
      />

      <Content>
        {isLoading || isFetching ? renderPreloader() : renderGameCards()}
      </Content>

      <TabletOn>
        <ButtonWrapper onClick={pushToGames}>
          <Button theme="gray">{t('show_more_games')}</Button>
        </ButtonWrapper>
      </TabletOn>
    </Wrapper>
  );
};

export default Games;
