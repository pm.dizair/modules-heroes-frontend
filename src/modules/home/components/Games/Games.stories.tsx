import { withPageProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import { IHomePageProps } from '@/pages';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import Games from './Games';
import { games } from './mock';

const withProvider = (Story: StoryFn) =>
  withPageProvider<IHomePageProps>({ newGames: { items: games } }, Story);

const meta: Meta<typeof Games> = {
  title: 'Modules/Home/Games',
  component: Games,
  decorators: [withProvider],
};

export default meta;

export const GameList: StoryObj<typeof Games> = {};

GameList.play = async ({ canvasElement, step }) => {
  await step('testing tabs', async () => {
    const canvas = within(canvasElement);
    await sleep(400);
    let tabsItems = await canvas.findAllByTestId('tab-item');

    expect(tabsItems[0]).toHaveStyle('color: #010d1b');

    await userEvent.click(tabsItems[1]);
    await sleep(500);

    tabsItems = await canvas.findAllByTestId('tab-item');
    expect(tabsItems[0]).not.toHaveStyle('color: #010d1b');

    await userEvent.click(tabsItems[2]);
    await sleep(500);

    tabsItems = await canvas.findAllByTestId('tab-item');
    expect(tabsItems[1]).not.toHaveStyle('color: #010d1b');
  });

  await step('test games', async () => {
    const canvas = within(canvasElement);
    const gameCards = await canvas.findAllByTestId('game-card');
    let tabItems = await canvas.findAllByTestId('tab-item');

    expect(gameCards.length > 0).toBeTruthy();

    await userEvent.click(tabItems[0]);
    await sleep(500);

    expect(await canvas.findAllByTestId('game-card')).not.toBe(
      await canvas.findAllByTestId('game-card')
    );
  });
};
