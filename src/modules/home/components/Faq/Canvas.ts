import { RefObject } from 'react';

export const dragonCanvas = (ref: RefObject<HTMLCanvasElement>) => {
  if (ref.current) {
    const canvas = ref.current;
    const ctx = canvas.getContext('2d');

    const CANVAS_WIDTH = (canvas.width = 520);
    const CANVAS_HEIGHT = (canvas.height = 520);

    const playerImage = new Image();
    playerImage.src =
      'https://gateway.pinata.cloud/ipfs/QmRRx6yX9HZW9KhYxHMgqGhgBzmwBC9oBVa8sjPJZjAsUX?_gl=1*1nottr3*rs_ga*YWU4NWM1ZmYtY2I0MS00OWY2LWEwZTctM2Y3N2RhZTdjZDYz*rs_ga_5RMPXG14TE*MTY4NTUzODczNS4xMS4xLjE2ODU1Mzg3NjguMjcuMC4w';
    const spriteWidth = 520;
    const spriteHeight = 520;
    const staggerFrames = 4;
    let gameFrame = 0;
    let frameX = 0;
    let frameY = 0;

    const animate = () => {
      if (ctx) {
        ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        ctx.drawImage(
          playerImage,
          frameX * spriteWidth,
          frameY * spriteHeight,
          spriteWidth,
          spriteHeight,
          0,
          0,
          spriteWidth,
          spriteHeight
        );
        if (gameFrame % staggerFrames == 0) {
          if (frameX < 30) {
            frameX++;
          } else {
            frameX = 0;
          }
        }

        gameFrame++;
        requestAnimationFrame(animate);
      }
    };

    animate();
  }
};
