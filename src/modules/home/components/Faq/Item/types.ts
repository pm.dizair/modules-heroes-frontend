import { Dispatch, SetStateAction } from 'react';

export interface IItemAccordion {
  title: string;
  description: string;
  id: number;
  open: boolean | number;
  setOpen: Dispatch<SetStateAction<boolean | number>>;
}
export interface IOpen {
  open?: boolean;
}
