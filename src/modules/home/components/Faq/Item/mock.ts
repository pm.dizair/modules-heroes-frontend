import { IQuestion } from '@/common/types/questions';

export const mockAccordionItem = {
  title: 'Can I join your Loyalty Program from the start?',
  description:
    'Mauris id odio mauris cursus. Blandit sed ultricies blandit dictum mollis ipsum. Velit pellentesque quisque velit lacus, faucibus ultrices feugiat ',
};

export const accordionsItems: IQuestion[] = [
  mockAccordionItem,
  mockAccordionItem,
  mockAccordionItem,
  mockAccordionItem,
].map((item, index) => ({ ...item, id: index + 1 }));
