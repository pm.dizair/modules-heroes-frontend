import { sleep } from '@/common/helpers/tests/sleep';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import { Item } from './Item';
import { mockAccordionItem } from './mock';

const meta: Meta<typeof Item> = {
  title: 'Modules/Home/Faq/Accordion/Item',
  component: Item,
  tags: ['autodocs'],
};

export default meta;

export const AccordionItem: StoryObj<typeof Item> = {
  args: {
    title: mockAccordionItem.title,
    description: mockAccordionItem.description,
  },
};

AccordionItem.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const item = canvas.getByTestId('accordion-item');

  expect(item.classList.contains('active')).toBeFalsy();
  expect(canvas.getByTestId('description')).toHaveStyle('height: 0');

  userEvent.click(item);

  await sleep(400);
  expect(item.classList.contains('active')).toBeTruthy();
  expect(canvas.getByTestId('description')).not.toHaveStyle('height: 0');
};
