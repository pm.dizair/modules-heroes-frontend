import { FC, useRef } from 'react';
import sanitizeHtml from 'sanitize-html';

import { Details, Line, Summary, Text, Wrapper } from './styles';
import { IItemAccordion } from './types';

export const Item: FC<IItemAccordion> = (props) => {
  const { title = '', description = '', open, setOpen, id } = props;
  const refItem = useRef<HTMLDivElement>(null);
  const IsOpen = open === id;
  return (
    <Details
      ref={refItem}
      data-testid="accordion-item"
      open={IsOpen}
      onClick={() => {
        if (IsOpen) {
          setOpen(false);
        } else {
          setOpen(id);
        }
      }}
    >
      <Summary className="title">
        <span>{title}</span>
        <Line />
      </Summary>
      <Wrapper open={IsOpen}>
        <Text
          open={IsOpen}
          data-testid="description"
          onClick={(e: any) => e.stopPropagation()}
          dangerouslySetInnerHTML={{
            __html: sanitizeHtml(description),
          }}
        />
      </Wrapper>
    </Details>
  );
};
