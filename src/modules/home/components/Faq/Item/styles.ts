import styled, { css } from 'styled-components';

import { IOpen } from './types';

export const Details = styled.div<IOpen>`
  background: rgba(16, 35, 58, 0.7);
  backdrop-filter: blur(6px);
  border-radius: 12px;
  isolation: isolate;
  transition: all 0.5s linear;
  padding-bottom: 0;
  border: 0.5px solid #828c9196;
  ${({ open }) =>
    open &&
    css`
      .description-item {
        opacity: 1;
        padding: 16px 16px 0 16px;
      }
      .title {
        ::before {
          transform: rotateZ(0deg);
          background: #ffffff;
        }
        hr {
          opacity: 1;
        }
        ::after {
          transform: rotateZ(0);
          background: #ffffff;
        }
      }
    `}
`;

export const Summary = styled.div<IOpen>`
  color: var(--gray);
  cursor: pointer;
  padding: 16px;
  width: 100%;
  text-align: left;
  position: relative;

  span {
    display: block;
    width: calc(100% - 40px);
    font-size: 16px;
    line-height: 1.5;
    ${({ theme }) => theme.text.getLineClamp(2)}
  }
  &::after,
  ::before {
    content: '';
    width: 14px;
    height: 2px;
    position: absolute;
    transform: translateY(-50%);
    top: 50%;
    right: 16px;
    background-color: #fff;
    transition: all 0.5s linear;
    transform: rotate(-90deg);
  }
  &::after {
    transform: rotate(-180deg);
  }

  @media screen and (max-width: 1440px) {
    span {
      font-size: 12px;
    }
  }
`;

export const Line = styled.hr`
  border: none;
  outline: 0;
  position: absolute;
  height: 1px;
  opacity: 0;
  width: calc(100% - 32px);
  background: rgba(255, 255, 255, 0.24);
  bottom: 0;
  left: 16px;
  transition: all 0.5s linear;
`;

export const Wrapper = styled.div<IOpen>`
  display: grid;
  overflow: hidden;
  transition: all 0.5s ease-out;
  ${({ open }) =>
    open
      ? css`
          grid-template-rows: 1fr;
        `
      : css`
          grid-template-rows: 0fr;
        `}
`;

export const Text = styled.div<IOpen>`
  margin: 0;
  transform: translate3d(0, 0, 0);
  position: relative;
  overflow: hidden;
  position: relative;
  color: var(--gray);
  font-weight: 600;
  font-size: 14px;
  transition: all 0.5s ease-out;
  line-height: 1.5;
  min-height: 0;
  ${({ open }) =>
    open
      ? css`
          padding: 16px;
        `
      : css`
          padding: 0 16px;
        `}
  p {
    color: var(--gray);
    font-weight: 600;
    font-size: 14px;
    line-height: 1.5;
  }
  @media screen and (max-width: 1440px) {
    font-size: 12px;
    font-weight: 400;

    p {
      font-size: 12px;
      font-weight: 400;
    }
  }
`;
