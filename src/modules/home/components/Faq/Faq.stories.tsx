import { withPageProvider } from '@/common/helpers/tests/providers/providers';
import { IHomePageProps } from '@/pages';
import '@/styles/scss/index.scss';
import { Meta, StoryFn, StoryObj } from '@storybook/react';

import Faq from './Faq';
import { accordionsItems } from './Item/mock';

const withProvider = (Story: StoryFn) =>
  withPageProvider<IHomePageProps>(
    { questions: { rightQuestion: accordionsItems } },
    Story
  );

const meta: Meta<typeof Faq> = {
  title: 'Modules/Home/Faq',
  component: Faq,
  tags: ['autodocs'],
  decorators: [withProvider],
};

export default meta;

export const List: StoryObj<typeof Faq> = {};
