import Image from 'next/image';
import styled from 'styled-components';

interface IHeight {
  height: number;
}

export const Wrapper = styled.div<IHeight>`
  display: flex;
  justify-content: center;
  gap: 24px;
  position: relative;
  ${({ theme }) => theme.content.baseBlockHeight};
  padding: 0 180px;
  z-index: 1;
  @media screen and (max-width: 1620px) {
    padding: 0 120px;
  }
  @media screen and (max-width: 1280px) {
    padding: 0 40px;
  }
  @media screen and (max-width: 1024px) {
    min-height: ${({ height }) => height + 260}px;
  }
  @media screen and (max-width: 767px) {
    padding: 0 16px;
  }

  canvas {
    position: absolute;
    bottom: -30px;
    left: 40%;
    transform: translateX(-50%);
    width: 600px;
    height: 600px;
    z-index: 1;
  }
`;

export const Container = styled.div`
  max-width: 1920px;
  margin: 0 auto;
  display: flex;
  width: 100%;
  justify-content: flex-end;
  @media screen and (min-width: 2100px) {
    padding: 0 180px;
  }
  @media screen and (max-width: 767px) {
    justify-content: center;
  }
`;

export const Title = styled.div`
  ${({ theme }) => theme.content.flex.column};
  gap: 14px;
  .gold-title {
    display: block;
  }
  @media screen and (max-width: 1440px) {
    gap: 4px;
    .gold-title {
      font-weight: 700;
      font-size: 32px;
      line-height: 1.1;
    }
  }
  @media screen and (max-width: 1024px) {
    width: 100%;
    text-align: center;
  }
`;

export const AccordionWrap = styled.div`
  max-width: 580px;
  position: relative;
  align-self: flex-end;
  height: 100%;
  z-index: 3;
  ${({ theme }) => theme.content.flex.column};
  justify-content: flex-start;
  padding-top: 5%;
  gap: 32px;

  @media screen and (max-width: 1440px) {
    max-width: 450px;
    gap: 20px;
  }
  @media screen and (max-width: 767px) and (orientation: landscape) {
    max-width: 100%;
  }
  @media screen and (max-width: 540px) {
    max-width: 100%;
  }
`;

export const Dragon = styled(Image)`
  position: absolute;
  z-index: 2;
  height: 80%;
  object-fit: contain;
  max-width: 620px;
  object-position: bottom;
  transform: translateY(5%);
  bottom: 0;
  left: 16%;
  @media screen and (max-width: 1024px) {
    left: 0;
    transform: translateX(-25%);
  }
  @media screen and (max-width: 767px) {
    max-width: 400px;
  }
  @media screen and (max-width: 540px) {
    left: 0;
    bottom: -20px;
    max-width: 240px;
    transform: translateX(-25%);
  }
`;

export const House = styled(Image)`
  position: absolute;
  height: 90%;
  object-fit: contain;
  max-width: 720px;
  transform: translateX(-8%);
  z-index: 2;
  bottom: -6%;
  left: 0;
  @media screen and (max-width: 1280px) {
    max-height: 420px;
  }
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;

export const Flashlight = styled(Image)`
  position: absolute;
  height: 220px;
  width: fit-content;
  object-fit: contain;
  left: 50%;
  transform: translateX(-30%);
  bottom: 50px;
  @media screen and (max-width: 1024px) {
    left: auto;
    right: -80px;
    object-position: right;
    bottom: 30px;
  }
  @media screen and (max-width: 540px) {
    right: -80px;
    object-position: right;
    bottom: 30px;
    height: 150px;
  }
`;

export const Background = styled(Image)`
  position: absolute;
  top: -30px;
  left: 50%;
  transform: translateX(-50%);
  height: calc(100% + 60px);
  width: fit-content;
  min-width: 100%;
  @media screen and (max-width: 767px) {
    width: 100%;
    object-fit: cover;
    left: 0;
    transform: translateX(0);
    object-position: 70%;
  }
`;

export const Description = styled.div`
  a {
    font-weight: 700;
    margin-left: 4px;
    color: #fbe13aff;
  }
  * {
    font-weight: 700;
    font-size: 16px;
    line-height: 160%;
    color: #ffffff;
    @media screen and (max-width: 1440px) {
      font-size: 14px;
      a {
        font-size: 14px;
      }
    }
  }
`;

export const AccordionList = styled.div`
  width: 100%;
  ${({ theme }) => theme.content.flex.column}
  gap: 16px;
  @media screen and (max-width: 1620px) {
    gap: 12px;
  }
`;
