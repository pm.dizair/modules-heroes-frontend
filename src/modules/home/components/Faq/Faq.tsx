import { DefaultError } from '@/common/components/errors';
import { useAppDispatch } from '@/common/hooks';
import { useLanguageChange } from '@/common/hooks/useLanguageChange/useLanguageChange';
import { useCommon } from '@/common/store/common/common.slice';
import { GoldTitle } from '@/ui-library/title';
import { getKey } from '@/utils/api/rtk-query/helpers';
import { useEffect, useRef, useState } from 'react';

import ImageDragonBg from '@/assets/images/backgrounds/DragonBg.webp';
import ImageDragonHome from '@/assets/images/decoration/DragonHome.webp';
import ImageDragon from '@/assets/images/heroes/Dragon.webp';
import ImageFlashLing from '@/assets/images/lights/Flashlight.webp';

import sanitizeHtml from 'sanitize-html';
import { useGetQuestionsQuery } from '../../api/rtk-query/questions/questions';
import { useHomepageContext } from '../../context';
import { dragonCanvas } from './Canvas';
import { Item } from './Item/Item';
import { cacheQuestionsAction } from './slice';

import { IQuestion } from '@/common/types/questions';
import {
  AccordionList,
  AccordionWrap,
  Background,
  Container,
  Description,
  Dragon,
  Flashlight,
  House,
  Title,
  Wrapper,
} from './styles';

const Faq = () => {
  const ref = useRef<HTMLCanvasElement>(null);
  const [open, setOpen] = useState<boolean | number>(false);

  const {
    questions: {
      leftQuestion,
      rightQuestion,
      error: serverError,
      isError: isServerError,
    },
  } = useHomepageContext();
  const dispatch = useAppDispatch();

  const [height, setHeight] = useState<number>(600);
  const { language } = useCommon();
  const [skip, setSkip] = useState<boolean>(true);

  useLanguageChange(() => setSkip(false));
  const args = {
    language,
  };

  const { isError, data, error } = useGetQuestionsQuery(
    {
      language,
    },
    { skip }
  );
  const titleTexts = !!data?.leftQuestion?.length
    ? data?.leftQuestion[0]
    : leftQuestion?.[0];

  const getAvailableSpace = (): number => {
    const dataSpace = document.getElementsByClassName('wrapper_item');
    let resultSpace = 0;
    for (let item of dataSpace) {
      const description: any =
        item.getElementsByClassName('description-item')[0];
      resultSpace += description.scrollHeight;
    }
    return resultSpace;
  };

  const QuestionItems = (data?.rightQuestion || rightQuestion)
    ?.slice(0, 5)
    .map((item: IQuestion) => (
      <Item
        open={open}
        setOpen={setOpen}
        title={item.title}
        id={item.id}
        description={item.description}
        key={item.id}
      />
    ));

  useEffect(() => {
    if (rightQuestion?.length) {
      dispatch(
        cacheQuestionsAction({
          key: getKey(args),
          data: { leftQuestion, rightQuestion },
        })
      );
    }
  }, []);

  useEffect(() => {
    setHeight(getAvailableSpace());
    if (ref.current) {
      dragonCanvas(ref);
    }
  }, []);

  if (isServerError) {
    return <DefaultError error={`serverError: ${serverError}`} />;
  } else if (isError && 'data' in error) {
    return <DefaultError error={`error: ${error.data}`} />;
  }

  if (!data?.rightQuestion?.length && !rightQuestion?.length) {
    return null;
  }

  return (
    <Wrapper height={height}>
      <House
        src={ImageDragonHome}
        alt="dragon home"
        sizes="(max-width: 1440px) 370px, 400px"
      />
      <Dragon
        src={ImageDragon}
        alt="dragon"
        sizes="(max-width: 1440px) 450px, (max-width: 1440px) 400px, (max-width: 540px) 300px, 500px"
      />
      {/* <canvas ref={ref} /> */}
      <Background
        src={ImageDragonBg}
        sizes="(max-width:767px)3000px,100vw"
        alt="forest with dragon"
        placeholder="blur"
      />
      <Flashlight
        src={ImageFlashLing}
        sizes="200px"
        alt="flesh light"
        placeholder="empty"
      />
      <Container>
        <AccordionWrap>
          <Title>
            <GoldTitle size="lg">{titleTexts?.title}</GoldTitle>
            <Description
              dangerouslySetInnerHTML={{
                __html: sanitizeHtml(titleTexts?.description || ''),
              }}
            />
          </Title>
          <AccordionList>{QuestionItems}</AccordionList>
        </AccordionWrap>
      </Container>
    </Wrapper>
  );
};

export default Faq;
