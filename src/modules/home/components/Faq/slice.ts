import { getCacheSlice } from '@/utils/store/helpers/getCachedSlice';

export const questionSlice = getCacheSlice('question', {});

export const cacheQuestionsAction = questionSlice.actions.cacheData;
