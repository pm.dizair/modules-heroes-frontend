import Image from 'next/image';
import styled, { css } from 'styled-components';
import { IWrapperCharacter } from './types';

export const Wrapper = styled.div<IWrapperCharacter>`
  ${({ theme }) => theme.content.flex.column};
  position: relative;
  height: fit-content;
  gap: 40px;
  .title {
    height: fit-content;
    width: 100%;
    text-align: center;
  }
  ${({ $isLogIn, theme }) =>
    $isLogIn &&
    css`
      .stats-container {
        ${theme.content.flex.center}
        padding-bottom: 15vh;
      }
    `}
  @media screen and (max-width: 1440px) {
    gap: 26px;
  }
  @media screen and (max-width: 1024px) {
    .choose-hero {
      min-height: 520px;
      height: calc(100vh - 220px);
      max-height: 700px;
    }
    .account-btn {
      width: 100%;
      max-width: 210px;
      margin: 20px auto 0 auto;
    }
    @media screen and (min-width: 541px) {
      .stats-container {
        height: 100%;
        transform: none;
        top: 0;
      }
      .stats-content {
        height: 100%;
      }
      .hero-container {
        top: 0;
        height: 100%;
        max-height: 800px;
        transform: translate(-50%, 0);
      }
    }
  }
  @media screen and (max-width: 540px) {
    max-height: 740px;
    .stats-container {
      align-items: flex-start;
    }
    .choose-hero {
      height: 520px;
    }
  }
`;

export const Background = styled(Image)<IWrapperCharacter>`
  position: absolute;
  top: -30px;
  left: 50%;
  transform: translateX(-50%);

  height: calc(100% + 20px);
  width: fit-content;
  min-width: 100%;
  @media screen and (max-width: 1620px) {
    object-position: 0 -30px;
  }
  @media screen and (max-width: 1024px) {
    object-position: 0;
  }
  @media screen and (max-width: 540px) {
    object-fit: cover;

    top: 0;
    max-height: 720px;
    height: 100%;

    ${({ $isLogIn }) =>
      $isLogIn
        ? css`
            min-height: 660px;
          `
        : css`
            min-height: 640px;
          `}
  }
`;
