import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { MODES } from '@/common/components/modals/Authorization/const';
import { ChooseHero } from '@/common/components/shared';
import { Button } from '@/ui-library/buttons';
import { GoldTitle } from '@/ui-library/title';
import { MODALS } from '@/utils/lib/modal/const';
import { TabletOn } from '@/utils/lib/responsive/responsive';
import { useModal } from '@ebay/nice-modal-react';
import { useEffect, useState } from 'react';

import imageBackground from '@/assets/images/backgrounds/ChooseHeroBg.webp';

import { IResponseHero } from '@/common/components/shared/ChooseHero/components/Stats/types';
import { useTranslations } from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { EHeroType, IHero } from '@/common/types/user';
import { useSelectHeroMutation } from '@/modules/auth/api/rtk-query/user/user';
import { useUser } from '@/modules/auth/store/user/user.slice';
import { useSnackbar } from 'notistack';
import { useHomepageContext } from '../../context';
import { Background, Wrapper } from './style';

const ChooseCharacter = () => {
  const { isAuth, user } = useUser();
  const { heroes } = useHomepageContext();
  const { language } = useCommon();
  const [active, setActive] = useState<string>(EHeroType.KNIGHT);
  const t = useTranslations();
  const [selectHero] = useSelectHeroMutation();
  const { enqueueSnackbar } = useSnackbar();
  const { show: showRegister } = useModal(MODALS.REGISTER);
  const openAuthorizationModal = () => {
    showRegister({ modal: MODALS.REGISTER, mode: active });
  };

  const handlerChangeHero = async () => {
    const hero = heroes.find((item) => item.type === active) as IHero;
    const response = await selectHero({ language, id: hero.id });
    const data = response as IResponseHero;
    if (data.error) {
      enqueueSnackbar(data.error.data.status, { variant: 'error' });
    } else {
      enqueueSnackbar(data.data.status, { variant: 'success' });
    }
  };

  useEffect(() => {
    if (user?.hero) {
      setActive(user.hero.type);
    } else {
      setActive(heroes[0].type);
    }
  }, [heroes, isAuth]);

  return (
    <Wrapper $isLogIn={!!user?.email}>
      <Background
        $isLogIn={!!user?.email}
        src={imageBackground}
        alt="choose"
        quality={100}
        priority
        placeholder="empty"
        sizes="(min-width: 1024px) 100vw, 800px"
      />
      <MainContainer className="title">
        <GoldTitle size="xl">{t('main_page_hero_title')}</GoldTitle>
      </MainContainer>
      <ChooseHero
        active={active}
        setActive={setActive}
      />

      <TabletOn>
        <Button
          className="account-btn"
          onClick={isAuth ? handlerChangeHero : openAuthorizationModal}
          disabled={user?.hero ? active === user?.hero.type : false}
        >
          {isAuth ? 'Change Hero' : 'Create Account'}
        </Button>
      </TabletOn>
    </Wrapper>
  );
};

export default ChooseCharacter;
