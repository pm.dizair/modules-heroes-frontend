import { IHero } from '@/common/types/user';

export interface IWrapperCharacter {
  $isLogIn: boolean;
}

export interface IHeroSliceProps {
  heroes: IHero[];
}
