import { createSlice } from '@reduxjs/toolkit';

import { useAppSelector } from '@/common/hooks';
import { IHeroSliceProps } from './types';

const initialState: IHeroSliceProps = {
  heroes: [],
};

export const heroesSlice = createSlice({
  initialState,
  name: 'heroes',
  reducers: {
    updateHeroes: (state, action) => {
      state.heroes = action.payload;
    },
  },
});

export const { updateHeroes } = heroesSlice.actions;
export const useHeroes = () =>
  useAppSelector((state) => state[heroesSlice.name]);
