import styled from 'styled-components';
import Image from 'next/image';

export const Wrapper = styled.div`
  position: relative;
`;

export const HeroImage = styled(Image)`
  object-position: center;
`;

export const HeroWrapper = styled.div`
  bottom: 0;
  position: absolute;
  height: 100%;
  max-width: 100%;
  width: 100%;
  overflow: hidden;

  @media screen and (max-width: 1024px) {
    width: 713px !important;
    height: 585px !important;
  }
`;

export const LightImage = styled(Image)`
  opacity: 0.35;
  position: absolute;
  top: 30%;
  left: 50%;
  z-index: 0;

  transform: translate(-50%, -50%);
`;