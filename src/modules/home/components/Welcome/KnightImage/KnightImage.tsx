import { useMaxResponsive } from '@/common/hooks';
import { FC } from 'react';

import Light from '@/assets/images/lights/Light.png';

import { HeroImage, HeroWrapper, LightImage, Wrapper } from './styles';
import { IKnightImage } from './types';

export const KnightImage: FC<IKnightImage> = (props) => {
  const { src, height, width, adaptiveScale = 0.76 } = props;

  const isDesktop = useMaxResponsive(1440);

  return (
    <Wrapper
      style={{
        height: isDesktop ? height * adaptiveScale : height,
        width: isDesktop ? width * adaptiveScale : width,
      }}
    >
      <LightImage
        alt="knightImage"
        src={Light}
      />

      <HeroWrapper>
        <HeroImage
          alt="image"
          src={src}
          objectFit={'contain'}
          fill
        />
      </HeroWrapper>
    </Wrapper>
  );
};
