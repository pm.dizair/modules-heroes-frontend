export interface IKnightImage {
    src: string;
    width: number;
    height: number;
    adaptiveScale?: number;
}