import Image from 'next/image';
import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-position: center;
  background-repeat: no-repeat;
  position: relative;
  background-size: cover;
  position: relative;
  height: calc(100vh - 120px);
  max-height: 800px;
  min-height: 600px;

  .main-container {
    height: 100%;
  }
  @media screen and (max-width: 1024px) {
    max-height: 550px;
    min-height: 500px;
  }
  @media screen and (max-width: 767px) {
    max-height: 600px;
    min-height: 500px;
    margin-bottom: 10px;
  }
`;

export const Gradient = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  height: 190px;
  background: linear-gradient(
    0deg,
    rgba(1, 13, 27, 1) 0%,
    rgba(0, 0, 0, 0) 100%
  );
`;

export const Knights = styled.div`
  display: flex;
  height: 100%;
  position: relative;
  flex-direction: column;
  ${({ theme }) => theme.content.mainContainerWidth}
  ${({ theme }) => theme.content.mainContainerPadding}
  ${({ theme }) => theme.content.flex.between}

  
  flex-direction: row;
  align-items: flex-end;
  width: 105%;
  height: 100%;
  position: absolute;
  max-width: 2200px;
  bottom: 6%;
  z-index: 0;
  left: 50%;
  transform: translateX(-50%);
  img {
    &:first-child {
      transform: translateX(-10%);
    }
    &:last-child {
      transform: translateX(10%);
    }
  }
  @media screen and (max-width: 1024px) {
    width: 100%;
    left: auto;
    right: 0;
    transform: translateX(0);
    justify-content: flex-end;
    align-items: center;
    img {
      &:first-child {
        transform: translateX(0px);
      }
    }
  }
  @media screen and (max-width: 767px) {
    justify-content: center;
  }
`;

export const KnightImage = styled(Image)`
  object-fit: contain;
  object-position: bottom;
  max-height: 70%;
  max-width: 600px;
  @media screen and (max-width: 1440px) {
    max-height: 65%;
  }
  @media screen and (max-width: 1024px) {
    max-width: fit-content;
    margin-top: 140px;
    max-height: 80%;
  }
  @media screen and (max-width: 767px) {
    max-width: 120vw;
  }
`;

export const Background = styled(Image)`
  position: absolute;
  width: 100%;
  height: 100%;
  top: -30px;
  left: 50%;
  transform: translateX(-50%);
  min-width: 100%;

  @media screen and (max-width: 1024px) {
    width: 100%;
    object-fit: cover;
  }
`;
