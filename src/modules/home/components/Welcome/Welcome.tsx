import { FC, useState } from 'react';

import ImageWelcomeCastle from '@/assets/images/backgrounds/WelcomeCastle.webp';
import LeftKnight from '@/assets/images/knights/LeftKnight.webp';
import RightKnight from '@/assets/images/knights/RightKnight.webp';

import { useLanguageChange } from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { useGetRandomPromotionQuery } from '@/utils/api/rtk-query/promotions/promotions';
import { TabletOff } from '@/utils/lib/responsive/responsive';
import { useHomepageContext } from '../../context';
import { WelcomePack } from './WelcomePack/WelcomePack';
import { Background, KnightImage, Knights, Wrapper } from './styles';

interface IWelcomePackProps {}

const WelcomeSection: FC<IWelcomePackProps> = () => {
  const { promotion } = useHomepageContext();
  const { language } = useCommon();
  const [skip, setSkip] = useState<boolean>(true);
  const { data } = useGetRandomPromotionQuery(
    {
      language,
      position: 'main_page',
    },
    { skip }
  );
  useLanguageChange(() => setSkip(false));

  return (
    <Wrapper>
      <WelcomePack promotion={data ? { data } : promotion} />
      <Background
        src={ImageWelcomeCastle}
        alt="background"
        placeholder="empty"
        sizes="(min-width: 1024px) 100vw, 800px"
        priority
      />
      <Knights>
        <TabletOff>
          <KnightImage
            src={LeftKnight}
            alt="knight"
            placeholder="empty"
            priority
            sizes="(min-width: 1024px) 50vw, 30vw"
          />
        </TabletOff>
        <KnightImage
          src={RightKnight}
          alt="knight"
          placeholder="empty"
          priority
          sizes="(min-width: 1024px) 50vw, 100vw"
        />
      </Knights>
    </Wrapper>
  );
};

export default WelcomeSection;
