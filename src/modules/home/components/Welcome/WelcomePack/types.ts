import { IGetRandomPromotionResponse } from '@/utils/api/server-query/promotions/types';

export interface IWelcomePackProps {
  promotion: IGetRandomPromotionResponse;
}
