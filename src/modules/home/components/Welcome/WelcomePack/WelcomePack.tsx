import { DefaultError } from '@/common/components/errors';
import { LINK_TEMPLATES } from '@/common/const';
import { useTranslations } from '@/common/hooks';
import { Button } from '@/ui-library/buttons';
import { MODALS } from '@/utils/lib/modal/const';
import { CSSMobileOff } from '@/utils/lib/responsive/breakpoints';
import { useModal } from '@ebay/nice-modal-react';
import { useRouter } from 'next/router';
import { FC, useRef } from 'react';

import { useUser } from '@/modules/auth/store/user/user.slice';

import sanitizeHtml from 'sanitize-html';
import Chest from './Chest/Chest';
import {
  ButtonWrapper,
  Content,
  ContentWrapper,
  LineClamp,
  WelcomeTitle,
  Wrapper,
} from './styles';
import { IWelcomePackProps } from './types';

export const WelcomePack: FC<IWelcomePackProps> = (props) => {
  const {
    promotion: { error, data, isError },
  } = props;

  const { show: showRegister } = useModal(MODALS.REGISTER);
  const ref = useRef<HTMLButtonElement>(null);
  const t = useTranslations();
  const { user } = useUser();
  const isLogIn = user?.email;
  const { push } = useRouter();

  const handlerAction = () => {
    if (isLogIn) {
      push(LINK_TEMPLATES.GAME_LISTING());
    } else {
      showRegister({ modal: MODALS.REGISTER });
    }
  };

  if (isError) {
    return (
      <Wrapper>
        <DefaultError error={error} />
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <Content>
        <CSSMobileOff>
          <Chest image={data?.background_baner_image} />
        </CSSMobileOff>
        <ContentWrapper>
          <LineClamp>
            {data?.title && (
              <WelcomeTitle
                dangerouslySetInnerHTML={{
                  __html: sanitizeHtml(data?.title),
                }}
              />
            )}
          </LineClamp>

          <ButtonWrapper>
            <Button
              ref={ref}
              size="xl"
              onClick={handlerAction}
            >
              {isLogIn
                ? t('profile_bonus_play_game_button')
                : t('promotion_button_create_account')}
            </Button>
          </ButtonWrapper>
        </ContentWrapper>
      </Content>
    </Wrapper>
  );
};
