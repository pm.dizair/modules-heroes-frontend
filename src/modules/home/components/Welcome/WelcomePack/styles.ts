import Image from 'next/image';
import styled from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.center};
  flex-direction: column;
  z-index: 2;
  position: relative;
  height: 100%;
  align-items: center;
  @media screen and (max-width: 1024px) {
    align-items: flex-start;
  }
  @media screen and (max-width: 767px) {
    justify-content: flex-end;
    align-items: flex-start;
    padding-bottom: 50px;
    background: linear-gradient(
      180deg,
      rgba(12, 1, 27, 0.1) 0%,
      rgba(10, 4, 27, 0.3) 24.39%,
      rgba(12, 1, 27, 0.25) 65.55%,
      rgba(3, 11, 27, 0.3) 85.35%,
      rgba(1, 13, 27, 0.5) 100%
    );
  }
`;

export const Content = styled.div`
  ${({ theme }) => theme.content.flex.center};
  flex-direction: column;
  gap: 16px;
  width: 100%;
  max-width: 677px;
  @media screen and (max-width: 1440px) {
    padding: 48px;
    gap: 0px;
  }

  @media screen and (max-width: 1024px) {
    max-width: 560px;
    padding: 20px;
  }

  @media screen and (max-width: 767px) {
    margin-bottom: 0;
    padding: 0px;
    max-width: none;
  }
`;

export const ContentWrapper = styled.div`
  ${({ theme }) => theme.content.flex.center};
  flex-direction: column;
  z-index: 3;
  padding: 0px 16px;
  text-align: center;
  width: 100%;

  @media screen and (max-width: 767px) {
    gap: 0px;
  }
`;

export const Chest = styled(Image)``;

export const ButtonWrapper = styled.div`
  ${({ theme }) => theme.content.flex.center}
  position: relative;
  margin-top: 36px;
  width: 100%;
  max-width: 537px;
  button {
    min-width: 135px;
  }
  @media screen and (max-width: 1440px) {
    max-width: 464px;
  }
  @media screen and (max-width: 767px) {
    margin-top: 28px;
  }
`;

export const LineClamp = styled.div`
  width: 100%;
  max-width: 520px;
  ${({ theme }) => theme.text.getLineClamp(4)}
  gap: 12px;
  @media screen and (max-width: 1280px) {
    max-width: 410px;
  }

  @media screen and (max-width: 767px) {
    gap: 0px;
    padding: 0 16px;
  }
`;

export const ChestWrapper = styled.div`
  height: 150px;
  width: 140px;

  ${({ theme }) => theme.content.absoluteCenter}
  ${({ theme }) => theme.content.flex.between}
  position: absolute;
  z-index: 3;
  img {
    object-fit: contain;
  }
  @media screen and (max-width: 1440px) {
    height: 135px;
    width: 125px;
  }

  @media screen and (max-width: 1024px) {
    height: 115px;
    width: 105px;
  }

  @media screen and (max-width: 767px) {
    height: 95px;
    width: 88px;
  }
`;

export const LightWrapper = styled.div`
  position: absolute;
  height: 300px;
  width: 300px;

  display: flex;
  justify-content: center;
  align-items: center;
  ${({ theme }) => theme.content.absoluteCenter}

  /* @media screen and (max-width: 1440px) {
    width:300px;
    height: 300px;
  } */

  @media screen and (max-width: 1024px) {
    width: 240px;
    height: 240px;
  }

  @media screen and (max-width: 767px) {
    width: 200px;
    height: 200px;
  }
`;

export const WelcomeTitle = styled.div`
  font-weight: 700;
  line-height: 120%;
  text-shadow: 0px 2px 2px #000000;
  font-size: 40px;
  span {
    font-weight: 700;
    font-size: 64px;
    line-height: 100%;
    margin: 0 4px;
    ${({ theme }) => theme.text.gothic};
    ${({ theme }) => theme.text.gold};
    text-shadow: none;
  }
  @media screen and (max-width: 1620px) {
    font-size: 36px;
    span {
      font-size: 56px;
    }
  }
  @media screen and (max-width: 1024px) {
    font-size: 24px;
    span {
      font-size: 40px;
    }
  }
  @media screen and (max-width: 540px) {
    font-size: 24px;
  }
`;
