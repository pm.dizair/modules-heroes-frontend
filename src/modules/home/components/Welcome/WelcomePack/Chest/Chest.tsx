import Image from 'next/image';

import ChestSrc from '@/assets/images/chests/Chest.webp';
import GlowSrc from '@/assets/images/lights/Glow.webp';
import LightSrc from '@/assets/images/lights/GodLight.png';

import { getImage } from '@/common/helpers/getImage';
import { FC } from 'react';
import { ChestWrapper, LightWrapper } from '../styles';
import { GlowImage, Wrapper } from './styles';
import { IChest } from './types';

const Chest: FC<IChest> = (props) => {
  const { image } = props;
  return (
    <Wrapper>
      <ChestWrapper>
        <Image
          alt="chest"
          src={image ? getImage(image) : ChestSrc}
          placeholder="empty"
          fill
        />
      </ChestWrapper>

      <GlowImage
        width={235}
        height={148}
        alt="glow"
        src={GlowSrc}
        placeholder="empty"
      />

      <LightWrapper>
        <Image
          fill
          alt="ligth"
          src={LightSrc}
          placeholder="empty"
        />
      </LightWrapper>
    </Wrapper>
  );
};

export default Chest;
