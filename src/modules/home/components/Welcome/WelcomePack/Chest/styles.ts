import styled from 'styled-components';
import Image from 'next/image';

export const Wrapper = styled.div`
  position: relative;
  height: 165px;
  width: 231px;

  @media screen and (max-width: 1440px) {
    height: 135px;
  }

  @media screen and (max-width: 1024px) {
    height: 105px;
  }
`;

export const GlowImage = styled(Image)`
  position: absolute;
  z-index: 2;
  bottom: 0;
`;

export const LightImage = styled(Image)`
  ${({ theme }) => theme.content.absoluteCenter}
  position: absolute;
  z-index: 1;
`;