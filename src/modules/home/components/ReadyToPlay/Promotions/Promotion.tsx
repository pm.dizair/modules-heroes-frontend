import { DefaultError } from '@/common/components/errors';
import { REQUEST_LIMITS } from '@/common/const';
import {
  useAppDispatch,
  useLanguageChange,
  useTranslations,
} from '@/common/hooks';
import { useCommon } from '@/common/store/common/common.slice';
import { Button } from '@/ui-library/buttons';
import { GoldTitle } from '@/ui-library/title';
import { getKey } from '@/utils/api/rtk-query/helpers';
import { useGetGamesPromotionsQuery } from '@/utils/api/rtk-query/promotions/promotions';
import { FC, memo, useEffect, useState } from 'react';

import PromotionBackground from '@/assets/images/backgrounds/PromotionBackground.webp';

import ChestItem from './ChestItem/ChestItem';
import { getPromotionsWithImages } from './data';
import Money from './Money/Money';
import { gamesPromotionsCacheAction } from './slice';
import {
  Content,
  HeaderWrapper,
  HeadSectionWrapper,
  PromotionImage,
  TextWrapper,
  Title,
  Wrapper,
} from './styles';
import { IPromotionsProps } from './types';

const Promotions: FC<IPromotionsProps> = (props) => {
  const { promotions: promos } = props;
  const [skip, setSkip] = useState<boolean>(true);
  const dispatch = useAppDispatch();

  const { language } = useCommon();

  useLanguageChange(() => setSkip(false));
  const args = {
    language,
    page: 1,
    limit: REQUEST_LIMITS.BONUS_PROMOTIONS,
  };

  const {
    isError,
    error,
    data: { items } = {},
  } = useGetGamesPromotionsQuery(args, { skip });

  const promotions = getPromotionsWithImages(items || promos);
  const t = useTranslations();

  useEffect(() => {
    dispatch(gamesPromotionsCacheAction({ key: getKey(args), data: promos }));
  }, []);

  const renderPromotions = () =>
    promotions.map((bonus) => (
      <ChestItem
        bonus={bonus}
        key={bonus.id}
      />
    ));

  if (isError && 'data' in error!) {
    return <DefaultError error={error.data + ''} />;
  }

  return (
    <Wrapper>
      <PromotionImage
        src={PromotionBackground}
        alt="promotion-image"
        sizes="(max-width: 900px) 100vw, 50vw"
        placeholder="blur"
        fill
      />
      <HeadSectionWrapper>
        <HeaderWrapper>
          <GoldTitle
            fontWeight={300}
            size="md"
          >
            Promotions
          </GoldTitle>

          <Button
            size="xs"
            theme="gray"
          >
            {t('show_more_games')}
          </Button>
        </HeaderWrapper>

        <TextWrapper>
          <Title>Register and get your bonuses</Title>

          <GoldTitle
            center
            size="lg"
            fontWeight={600}
          >
            Ready to play a game?
          </GoldTitle>
          <Money />
        </TextWrapper>
      </HeadSectionWrapper>

      <Content>{renderPromotions()}</Content>
    </Wrapper>
  );
};

export default memo(Promotions);
