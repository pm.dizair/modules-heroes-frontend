import { IPromotion } from '@/common/types';
import { IBonus } from '@/common/types/bonus';

import ChestCrown from '@/assets/images/chests/ChestCrown.webp';
import ChestGold from '@/assets/images/chests/ChestGold.webp';
import ChestCrystals from '@/assets/images/chests/Crystals.webp';

export const images = [ChestCrown, ChestGold, ChestCrystals];

export const getPromotionsWithImages = (promotions: IPromotion[]): IBonus[] =>
  promotions.map((promotion, index) => ({
    image: images[index],
    ...promotion,
  }));
