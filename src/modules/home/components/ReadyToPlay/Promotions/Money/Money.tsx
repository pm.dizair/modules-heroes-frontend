import Image from 'next/image';

import MoneyLeft from '@/assets/images/other/MoneyLeft.png';
import MoneyRight from '@/assets/images/other/MoneyRight.png';

import { MoneyLeftWrapper, MoneyRightWrapper, Wrapper } from './styles';

const Money = () => {
  return (
    <Wrapper>
      <MoneyLeftWrapper>
        <Image
          src={MoneyLeft}
          alt="MoneyLeft"
          fill
        />
      </MoneyLeftWrapper>

      <MoneyRightWrapper>
        <Image
          src={MoneyRight}
          alt="MoneyRight"
          fill
        />
      </MoneyRightWrapper>
    </Wrapper>
  );
};

export default Money;
