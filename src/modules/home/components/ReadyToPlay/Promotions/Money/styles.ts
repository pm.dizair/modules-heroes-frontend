import styled from 'styled-components';

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.between};
  width: 100%;
  margin: 0 auto;
  max-width: 547px;

  @media screen and (max-width: 1024px) {
    max-width: 370px;
  }
  @media screen and (max-width: 900px) {
    display: none;
  }
`;

export const MoneyLeftWrapper = styled.div`
  position: relative;
  transform: rotateX(180deg);
  width: 104px;
  height: 54px;

  @media screen and (max-width: 1024px) {
    width: 60px;
    height: 35px;
  }
`;

export const MoneyRightWrapper = styled.div`
  position: relative;

  width: 104px;
  height: 69px;

  @media screen and (max-width: 1024px) {
    width: 58px;
    height: 42px;
  }
`;
