import { getCacheSlice } from '@/utils/store/helpers/getCachedSlice';

export const gamesPromotionsSlice = getCacheSlice('games-promotions', {});

export const gamesPromotionsCacheAction =
  gamesPromotionsSlice.actions.cacheData;
