import Image from 'next/image';
import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  width: 100%;
  ${({ theme }) => theme.content.flex.between}
  margin-bottom: 42px;
  @media screen and (max-width: 767px) {
    margin-bottom: 16px;
  }
`;

export const Wrapper = styled.div`
  ${({ theme }) => theme.content.flex.column};
  position: relative;
  width: 100%;
  border-radius: 12px;
  overflow: hidden;
  padding: 24px;
  justify-content: flex-start;
  height: 100%;
  position: relative;
  z-index: 1;

  @media screen and (max-width: 900px) {
    gap: 16px;
    padding: 12px;
    min-height: 336px;
  }
`;

export const HeadSectionWrapper = styled.div`
  ${({ theme }) => theme.content.flex.column};
`;

export const GoldenTitle = styled.div`
  font-family: 'Grenze Gotisch';
  font-style: normal;
  font-weight: 700;
  font-size: 48px;
  line-height: 40px;

  text-align: center;
  text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.5);
  color: gold;
`;

export const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(3, calc(33.3% - 18px));
  width: 100%;
  grid-gap: 26px;
  margin: auto 0;
  @media screen and (max-width: 1620px) {
    grid-gap: 18px;
    grid-template-columns: repeat(3, calc(33.3% - 12px));
  }
  @media screen and (max-width: 1440px) {
    grid-template-columns: 100%;
    grid-gap: 12px;
  }

  @media screen and (max-width: 900px) {
    margin: 0 auto;
  }
`;

export const PromotionsTitle = styled.div`
  font-family: 'Grenze Gotisch';
  font-style: normal;
  font-weight: 500;
  font-size: 28px;
  line-height: 1.8;
  display: flex;
  align-items: center;
  color: gold;
`;

export const TextWrapper = styled.div`
  ${({ theme }) => theme.content.flex.center};
  gap: 12px;
  flex-direction: column;
  flex-grow: 1;
  flex-shrink: 0;
  @media screen and (max-width: 1440px) {
    gap: 6px;
  }

  @media screen and (max-width: 900px) {
    padding-top: 16px;
  }
`;

export const Title = styled.span`
  text-shadow: 1px 1px 2px #000000;
  font-style: normal;
  color: #ffffff;
  text-align: center;
  ${({ theme }) => theme.text.getLineClamp(2)}
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  @media screen and (max-width: 1440px) {
    font-size: 14px;
  }

  @media screen and (max-width: 1024px) {
    font-size: 16px;
    font-weight: 700;
  }
`;

export const PromotionImage = styled(Image)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
  object-fit: cover;
  object-position: center;
`;
