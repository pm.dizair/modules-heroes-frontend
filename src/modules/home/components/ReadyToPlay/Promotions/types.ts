import { IPromotion } from '@/common/types';

export interface IPromotionsProps {
  promotions: IPromotion[];
}
