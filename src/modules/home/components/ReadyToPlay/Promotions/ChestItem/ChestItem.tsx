import { Button } from '@/ui-library/buttons';
import { DesktopOff } from '@/utils/lib/responsive/responsive';
import { FC } from 'react';

import Chest from './Chest/Chest';
import {
  ButtonContainer,
  Description,
  Flex,
  Indent,
  Subtitle,
  Title,
  Wrap,
  Wrapper,
} from './styles';
import { IChestItem } from './types';

const ChestItem: FC<IChestItem> = (props) => {
  const {
    bonus: { button_text, image, description, sub_title, title },
  } = props;

  return (
    <Wrapper data-testid="chest-item">
      <Wrap>
        <DesktopOff>
          <Chest image={image} />
        </DesktopOff>
        {title && <Title>{title}</Title>}
        <DesktopOff>
          <Indent />
        </DesktopOff>
      </Wrap>
      <Flex>
        <div>
          {sub_title && <Subtitle> {sub_title}</Subtitle>}
          {description && <Description>{description}</Description>}
        </div>
        <ButtonContainer>
          <Button size="xs">
            <span>{button_text}</span>
          </Button>
        </ButtonContainer>
      </Flex>
    </Wrapper>
  );
};

export default ChestItem;
