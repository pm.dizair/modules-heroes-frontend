import { IBonus } from '@/common/types/bonus';

import ChestImage from '@/assets/images/chests/Chest.webp';

export const bonus: IBonus = {
  image: ChestImage,
  description: '260% up to €3500 + 270 FS',
  sub_title: 'on four deposits',
  title: 'Bonus package',
  id: 1,
  button_text: 'Get bonus',
};
