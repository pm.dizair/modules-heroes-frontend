import { StaticImageData } from 'next/image';

export interface IChestItemProps {
  image: StaticImageData | string;
}
