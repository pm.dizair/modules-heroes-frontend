import { FC } from 'react';

import Glow from '@/assets/images/lights/Glow.webp';
import ImageLight from '@/assets/images/lights/GodLight.png';

import { ChestImage, ChestWrapper, Light } from './styles';
import { IChestItemProps } from './types';

const Chest: FC<IChestItemProps> = (props) => {
  const { image } = props;

  return (
    <ChestWrapper>
      <Light
        src={Glow}
        alt="glow"
        fill
      />
      <Light
        src={ImageLight}
        alt="light"
        fill
      />

      <ChestImage
        src={image}
        alt="chest item"
        fill
      />
    </ChestWrapper>
  );
};

export default Chest;
