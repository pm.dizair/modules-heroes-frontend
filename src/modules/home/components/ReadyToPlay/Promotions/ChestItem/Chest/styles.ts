import Image from 'next/image';
import styled from 'styled-components';

export const ChestWrapper = styled.div`
  width: 100%;
  height: 90px;
  position: relative;
  z-index: 1;

  @media screen and (max-width: 1440px) {
    min-height: 46px;
    max-width: 70px;
  }
  @media screen and (max-width: 540px) {
    display: none;
  }
`;

export const ChestImage = styled(Image)`
  width: 100%;
  height: 100%;
  object-fit: contain;
  object-position: center;
`;

export const Light = styled(Image)`
  /* for some reason these forces are interrupted by inline styles */
  position: absolute;
  left: -30px !important;
  top: -30px !important;
  z-index: -1;
  width: calc(100% + 60px) !important;
  height: calc(100% + 60px) !important;
  object-fit: contain;
  object-position: center;
  @media screen and (max-width: 1440px) {
    left: -10px !important;
    top: -10px !important;
    width: calc(100% + 20px) !important;
    height: calc(100% + 20px) !important;
  }
`;
