import { IBonus } from '@/common/types/bonus';

export interface IChestItem {
  bonus: IBonus;
}
