import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.06),
      rgba(255, 255, 255, 0.06)
    ),
    #010d1b;
  width: 100%;
  border: 1px solid rgba(255, 255, 255, 0.1);
  box-shadow: inset 0px 0px 12px rgba(255, 255, 255, 0.06);
  border-radius: 8px;
  padding: 16px;
  overflow: hidden;
  @media screen and (max-width: 1440px) {
    padding: 12px;
  }
  @media screen and (max-width: 767px) {
    padding: 8px 12px;
  }
`;
export const Title = styled.h3`
  text-align: center;
  font-weight: 600;
  font-size: 14px;
  line-height: 19px;
  ${({ theme }) => theme.text.getLineClamp(2)};
  @media screen and (max-width: 1440px) {
    text-align: left;
    ${({ theme }) => theme.text.getLineClamp(1)};
  }
  @media screen and (max-width: 1280px) {
    font-size: 10px;
    line-height: 14px;
  }
`;

export const Wrap = styled.div`
  ${({ theme }) => theme.content.flex.column};
  gap: 6px;
  justify-content: space-between;
  width: 100%;
  min-height: 150px;
  @media screen and (max-width: 1440px) {
    min-height: fit-content;
  }
`;

export const Flex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  height: 100%;
  @media screen and (max-width: 1440px) {
    gap: 12px;
    flex-direction: row;
    width: 100%;
  }
  @media screen and (max-width: 767px) {
    gap: 6px;
  }
`;
export const Description = styled.span`
  ${({ theme }) => theme.text.getLineClamp(2)};
  text-align: center;
  font-weight: 600;
  font-size: 10px;
  line-height: 14px;
  margin-top: 6px;
  @media screen and (max-width: 1440px) {
    text-align: left;
    ${({ theme }) => theme.text.getLineClamp(1)};
  }
`;
export const Subtitle = styled.span`
  font-weight: 700;
  font-size: 14px;
  line-height: 19px;
  color: var(--gold);
  text-align: center;
  ${({ theme }) => theme.text.getLineClamp(3)};
  margin-top: 6px;
  @media screen and (max-width: 1440px) {
    font-size: 12px;
    line-height: 16px;
    text-align: left;

    ${({ theme }) => theme.text.getLineClamp(2)};
  }
`;

export const Indent = styled.div`
  background: linear-gradient(
    90deg,
    rgba(251, 225, 58, 0) 0%,
    rgba(251, 225, 58, 0.5044) 20.15%,
    #b3261e 49.59%,
    rgba(251, 225, 58, 0.453167) 81.46%,
    rgba(251, 225, 58, 0) 100%
  );
  width: 100%;
  min-height: 1px;
`;

export const ButtonContainer = styled.div`
  margin: 4px auto 0 auto;
  width: 100%;
  max-width: 130px;
  align-self: flex-end;
  .color-button {
    padding: 7px 10px;
    width: 100%;
    span {
      align-items: center;
      color: #010d1b;
      font-weight: 800;
      font-size: 11px;
      line-height: 14px;
      ${({ theme }) => theme.text.overflow}
    }
  }
  @media screen and (max-width: 1440px) {
    margin: 0;
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
`;
