import { IBonus } from '@/common/types/bonus';
import '@/styles/scss/index.scss';
import { store } from '@/utils/store';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import { FC } from 'react';
import { Provider } from 'react-redux';

import ChestItem from './ChestItem';
import { bonus } from './mocks';

const withProvider = (Story: StoryFn) => (
  <Provider store={store}>{<Story />}</Provider>
);

const Component: FC<{ bonus: IBonus }> = ({ bonus }) => (
  <div style={{ maxWidth: 200 }}>
    <ChestItem bonus={bonus} />
  </div>
);

const meta: Meta<typeof Component> = {
  title: 'Modules/Home/ReadyToPlay/ChestItem',
  component: Component,
  decorators: [withProvider],
};

export default meta;

export const Chest: StoryObj<typeof ChestItem> = {
  args: { bonus },
};

Chest.play = async ({ canvasElement }) => {
  const canvas = within(canvasElement);
  const title = canvas.getByText(bonus.title);
  const offer = canvas.getByText(bonus.description);
  const subtitle = canvas.getByText(bonus.sub_title);

  expect(title).toBeInTheDocument();
  expect(offer).toBeInTheDocument();
  expect(subtitle).toBeInTheDocument();
};
