import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100vh - 100px);
  max-height: 860px;
  min-height: 650px;
  .main-container {
    height: calc(100% - 90px);
    ${({ theme }) => theme.content.flex.column}
    margin-top: 16px;
  }
  @media screen and (max-width: 1440px) {
    max-height: 650px;
  }
  @media screen and (max-width: 900px) {
    height: fit-content;
    max-height: fit-content;
    .main-container {
      margin-top: 4px;
    }
  }
  @media screen and (max-width: 767px) {
    .main-container {
      height: calc(100% - 64px);
    }
  }
`;

export const ContentWrapper = styled.div`
  ${({ theme }) => theme.content.flex.row};
  align-items: stretch;
  height: 100%;
  gap: 30px;
  @media screen and (max-width: 1620px) {
    gap: 24px;
  }
  @media screen and (max-width: 900px) {
    flex-direction: column;
  }
`;

export const PromotionsWrapper = styled.div`
  width: calc(45% - 15px);
 
  @media screen and (max-width: 900px) {
    width: 100%;
  }
`;

export const GamesWrapper = styled.div`
  width: calc(55% - 15px);

  @media screen and (max-width: 900px) {
    width: 100%;
    height: 212px;
  }
`;
