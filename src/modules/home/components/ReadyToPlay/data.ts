import { ITab } from '@/common/types';

export const tabs: ITab[] = [
  {
    name: 'Beliebt',
    slug: 'popular',
    image: '/files/images/game/category/Popular_1.png',
    mobile_image: '/files/images/game/category/Popular_1.png',
    position: 0,
  },
  {
    name: 'Jackpot',
    slug: 'jackpot',
    image: '/files/images/game/category/Jackpot_1.png',
    mobile_image: '/files/images/game/category/Jackpot_1.png',
    position: 1,
  },
  {
    name: 'Live Kasino',
    slug: 'live_casino',
    image: '/files/images/game/category/Live_1.png',
    mobile_image: '/files/images/game/category/Live_1.png',
    position: 2,
  },
  {
    name: 'Neu',
    slug: 'new',
    image: '/files/images/game/category/New_1.png',
    mobile_image: '/files/images/game/category/New_1.png',
    position: 3,
  },
  {
    name: 'Hot',
    slug: 'hot',
    image: '/files/images/game/category/Hot_1.png',
    mobile_image: '/files/images/game/category/Hot_1.png',
    position: 4,
  },
  {
    name: 'Slots',
    slug: 'slots',
    image: '/files/images/game/category/Slot_1.png',
    mobile_image: '/files/images/game/category/Slot_1.png',
    position: 5,
  },
];
