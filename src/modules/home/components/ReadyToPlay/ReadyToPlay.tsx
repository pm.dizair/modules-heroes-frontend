import MainContainer from '@/common/components/containers/MainContainer/MainContainer';
import { DefaultError } from '@/common/components/errors';
import { BasicTabs } from '@/common/components/tabs';
import { useCommon } from '@/common/store/common/common.slice';
import { ITab } from '@/common/types';
import { FC, useEffect, useState } from 'react';

import { Responsive } from '@/utils/lib/responsive/responsive';
import { useHomepageContext } from '../../context';
import Games from './Games/Games';
import Promotions from './Promotions/Promotion';
import {
  ContentWrapper,
  GamesWrapper,
  PromotionsWrapper,
  Wrapper,
} from './styles';
import { IReadyToPlayProps } from './types';

const ReadyToPlay: FC<IReadyToPlayProps> = () => {
  const {
    readyToPlayGames: {
      items: games,
      isError: isGamesError,
      error: gamesError,
    },
    promotions: {
      items: promotions,
      isError: isPromotionError,
      error: promotionsError,
    },
  } = useHomepageContext();

  const { categories } = useCommon();
  const [categoryTab, setCategory] = useState<ITab>(categories?.[0]);

  useEffect(() => {
    if (categories?.length) {
      setCategory(categories[0]);
    }
  }, [categories?.length]);

  if (isPromotionError || isGamesError) {
    return (
      <DefaultError
        error={`games: ${gamesError}  promotions:${promotionsError}`}
      />
    );
  }

  if (!games?.length || !promotions?.length) {
    return null;
  }

  return (
    <Wrapper>
      <BasicTabs
        tabs={categories}
        setSelectedTab={setCategory}
        selectedTab={categoryTab}
      />
      <Responsive maxWidth={900}>
        <Games
          categories={categories}
          category={categoryTab}
          games={games}
        />
      </Responsive>
      <MainContainer>
        <ContentWrapper>
          <Responsive minWidth={901}>
            <GamesWrapper>
              <Games
                categories={categories}
                category={categoryTab}
                games={games}
              />
            </GamesWrapper>
          </Responsive>
          <PromotionsWrapper>
            <Promotions promotions={promotions} />
          </PromotionsWrapper>
        </ContentWrapper>
      </MainContainer>
    </Wrapper>
  );
};

export default ReadyToPlay;
