import { withPageProvider } from '@/common/helpers/tests/providers/providers';
import { sleep } from '@/common/helpers/tests/sleep';
import { IHomePageProps } from '@/pages';
import '@/styles/scss/index.scss';
import { expect } from '@storybook/jest';
import { Meta, StoryFn, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import { games, promotions } from './mock';
import ReadyToPlay from './ReadyToPlay';

const withProvider = (Story: StoryFn) =>
  withPageProvider<IHomePageProps>(
    { readyToPlayGames: { items: games }, promotions: { items: promotions } },
    Story
  );

const meta: Meta<typeof ReadyToPlay> = {
  title: 'Modules/Home/ReadyToPlay',
  component: ReadyToPlay,
  decorators: [withProvider],
};

export default meta;

export const Base: StoryObj<typeof ReadyToPlay> = {};

Base.play = async ({ canvasElement, step }) => {
  await step('testing tabs', async () => {
    const canvas = within(canvasElement);
    await sleep(400);
    let tabsItems = await canvas.findAllByTestId('tab-item');

    expect(tabsItems[0]).toHaveStyle('color: #010d1b');

    await userEvent.click(tabsItems[1]);
    await sleep(500);

    tabsItems = await canvas.findAllByTestId('tab-item');
    expect(tabsItems[0]).not.toHaveStyle('color: #010d1b');

    await userEvent.click(tabsItems[2]);
    await sleep(500);

    tabsItems = await canvas.findAllByTestId('tab-item');
    expect(tabsItems[1]).not.toHaveStyle('color: #010d1b');
  });

  await step('test games', async () => {
    const canvas = within(canvasElement);
    const gameCards = await canvas.findAllByTestId('game-card');
    let tabItems = await canvas.findAllByTestId('tab-item');

    expect(gameCards.length > 0).toBeTruthy();

    await userEvent.click(tabItems[0]);
    await sleep(500);

    expect(await canvas.findAllByTestId('game-card')).not.toBe(
      await canvas.findAllByTestId('game-card')
    );
  });

  await step('test promotions', async () => {
    const canvas = within(canvasElement);
    const promotions = await canvas.findAllByTestId('chest-item');

    expect(promotions).toHaveLength(3);
  });
};
