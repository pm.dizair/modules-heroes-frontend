import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  height: 100%;
  gap: 30px;
  @media screen and (max-width: 1620px) {
    gap: 24px;
  }
  @media screen and (max-width: 900px) {
    width: fit-content;
  }
`;

export const Container = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  @media screen and (max-width: 900px) {
    margin-top: 16px;
    padding-bottom: 12px;
    ${({ theme }) => theme.content.hiddenScrollHorizontal};
  }
  @media screen and (max-width: 767px) {
    margin-top: 12px;
  }
`;

export const Content = styled.div`
  gap: 16px;
  width: 100%;
  ${({ theme }) => theme.content.mainContainerPadding};
  display: grid;
  grid-template-columns: repeat(12, 160px);
`;

export const Row = styled.div`
  width: 100%;
  display: grid;
  height: calc(50% - 15px);
  grid-gap: 30px;
  &:first-child {
    grid-template-columns: 2fr 3fr 2fr;
  }
  &:last-child {
    grid-template-columns: 3fr 2fr 2fr;
  }
  @media screen and (max-width: 1620px) {
    grid-gap: 24px;
    height: calc(50% - 12px);
    &:first-child {
      grid-template-columns: 3fr 4fr 3fr;
    }
    &:last-child {
      grid-template-columns: 4fr 3fr 3fr;
    }
  }
  @media screen and (max-width: 1280px) {
    &:first-child {
      grid-template-columns: 1fr 1fr;
    }
    &:last-child {
      grid-template-columns: 1fr 1fr;
    }
  }
`;
