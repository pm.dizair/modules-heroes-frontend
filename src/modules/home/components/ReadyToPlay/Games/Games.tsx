import { GameCard, GameCardLoader } from '@/common/components/cards';
import { DefaultError } from '@/common/components/errors';
import {
  useAppDispatch,
  useMaxResponsive,
  useValueChange,
  useWindowWidth,
} from '@/common/hooks';
import { IGame } from '@/common/types';
import { useGetReadyTopPlayGamesQuery } from '@/utils/api/rtk-query';
import { getKey } from '@/utils/api/rtk-query/helpers';
import { FC, useEffect, useRef, useState } from 'react';

import { Responsive } from '@/utils/lib/responsive/responsive';
import { cacheReadyToPlayAction } from './slice';
import { Container, Content, Row, Wrapper } from './styles';
import { IGamesProps } from './types';

const Games: FC<IGamesProps> = (props) => {
  const { games: initialGames, category, categories } = props;
  const dispatch = useAppDispatch();

  const containerRef = useRef<HTMLDivElement>(null);
  const isLittleDesktop = useMaxResponsive(1280);
  const isTablet = useMaxResponsive(900);
  const COUNT_EL = isLittleDesktop ? (isTablet ? 12 : 4) : 6;
  const windowWidth = useWindowWidth();

  const [skip, setSkip] = useState<boolean>(true);

  useValueChange(category?.slug, () => setSkip(false));
  useValueChange(windowWidth, () => setSkip(false));

  const args = {
    page: 1,
    limit: COUNT_EL,
    category: category?.slug,
  };

  const { data, isLoading, isFetching, isError, refetch } =
    useGetReadyTopPlayGamesQuery(args, {
      skip,
    });

  useEffect(() => {
    if (category?.slug === categories?.[0]?.slug) {
      dispatch(
        cacheReadyToPlayAction({ key: getKey(args), data: initialGames })
      );
    }
  }, [category]);

  useEffect(() => {
    containerRef.current?.scrollTo({
      behavior: 'smooth',
      left: 0,
      top: 0,
    });
  }, [category]);

  useEffect(() => {
    if (!skip) {
      refetch();
    }
  }, [isTablet]);

  const getPreloader = () =>
    [...Array(12)].map((_, index) => (
      <GameCardLoader
        fullSize
        key={index}
      />
    ));

  const currentGames = data?.items || initialGames;

  const getGameCards = () =>
    !isLoading &&
    currentGames.map((game: IGame) => (
      <GameCard
        fullSize
        key={game.id}
        game={game}
      />
    ));

  if (isError) {
    return <DefaultError />;
  }

  if (!currentGames.length) {
    return null;
  }

  const gameCardRender = (count: number, games: IGame[]) => {
    if (isLoading || isFetching) {
      return [...Array(count)].map((_, index) => (
        <GameCardLoader key={index} />
      ));
    } else {
      return games.map((game: IGame) => (
        <GameCard
          key={game.id}
          game={game}
          isLoading={isLoading}
        />
      ));
    }
  };

  const isMatch = COUNT_EL === currentGames.length;

  return (
    <Container>
      <Wrapper ref={containerRef}>
        <Responsive maxWidth={900}>
          <Content>
            {isLoading && getPreloader()}
            {getGameCards()}
          </Content>
        </Responsive>
        <Responsive minWidth={901}>
          <Row>
            {gameCardRender(
              COUNT_EL / 2,
              currentGames.slice(
                0,
                Math.ceil((isMatch ? currentGames.length : COUNT_EL) / 2)
              )
            )}
          </Row>
          <Row>
            {gameCardRender(
              COUNT_EL / 2,
              isMatch
                ? currentGames.slice(Math.ceil(currentGames.length / 2))
                : currentGames.slice(COUNT_EL / 2, Math.ceil(COUNT_EL))
            )}
          </Row>
        </Responsive>
      </Wrapper>
    </Container>
  );
};

export default Games;
