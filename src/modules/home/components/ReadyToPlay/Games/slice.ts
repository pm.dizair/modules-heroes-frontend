import { getCacheSlice } from '@/utils/store/helpers/getCachedSlice';

export const readyToPlaySlice = getCacheSlice('ready-to-play', {});

export const cacheReadyToPlayAction = readyToPlaySlice.actions.cacheData;
