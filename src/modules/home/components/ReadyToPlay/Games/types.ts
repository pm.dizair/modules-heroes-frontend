import { IGame, ITab } from '@/common/types';

export interface IGamesProps {
  games: IGame[];
  category: ITab;
  categories: ITab[];
}
