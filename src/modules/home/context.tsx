import { usePageContext } from '@/common/components/providers';
import { IHomePageProps } from '@/pages';

export const useHomepageContext = () => usePageContext<IHomePageProps>();
