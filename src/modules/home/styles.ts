import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 100px;
  @media screen and (max-width: 1600px) {
    gap: 80px;
  }
  @media screen and (max-width: 1024px) {
    gap: 64px;
  }
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;

  @media screen and (max-width: 767px) {
    gap: 30px;
  }
`;

// its need because we should allocate space for padding of games block that mobile safari scroll will be good
// allocate space is 12px
export const Cover = styled(Wrapper)`
  gap: 88px;
  @media screen and (max-width: 1600px) {
    gap: 78px;
  }
  @media screen and (max-width: 1024px) {
    gap: 52px;
  }
`;

export const TempSpacing = styled.div`
  padding-bottom: 48px;
`;
