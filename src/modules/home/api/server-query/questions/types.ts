import { IWithError } from '@/common/types';
import { IQuestion } from '@/common/types/questions';

export interface IGetFaqResponse extends IWithError {
  leftQuestion?: IQuestion[];
  rightQuestion?: IQuestion[];
}

export interface IQuestionBody {
  language: string;
}
