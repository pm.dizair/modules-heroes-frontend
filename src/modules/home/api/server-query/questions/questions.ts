import { handleError } from '@/common/helpers/handleError';
import { axiosBase } from '@/utils/api/server-query/base';

import { IGetFaqResponse, IQuestionBody } from './types';

export const QuestionApi = {
  async getFaq(body: IQuestionBody): Promise<IGetFaqResponse> {
    return handleError<IGetFaqResponse>(async () => {
      const { language } = body;
      const { data } = await axiosBase({
        url: `/${language}/content/question/`,
      });

      return data;
    });
  },
};
