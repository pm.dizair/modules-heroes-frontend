import { IResponseQuestion } from '@/common/types/questions';
import { baseApi } from '@/utils/api/rtk-query';
import { getCachedQueryFn } from '@/utils/api/rtk-query/helpers';

import { cacheQuestionsAction } from '@/modules/home/components/Faq/slice';
import { IQuestionBody } from '../../server-query/questions/types';

export const questionApi = baseApi.injectEndpoints({
  overrideExisting: false,
  endpoints: (build) => ({
    getQuestions: build.query<IResponseQuestion, IQuestionBody>({
      queryFn: (body, ...args) => {
        const language = body?.language || localStorage.getItem('language');
        const fetch = getCachedQueryFn(
          'question',
          {
            url: `/${language}/content/question/`,
            method: 'GET',
          },
          cacheQuestionsAction,
          ['query']
        );
        return fetch(body, ...args);
      },
    }),
  }),
});

export const { useGetQuestionsQuery } = questionApi;
