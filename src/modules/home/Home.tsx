import { useCommon } from '@/common/store/common/common.slice';

import { PopularGames } from '@/common/components/shared';
import Bonuses from './components/Bonuses/Bonuses';
import ChooseCharacter from './components/ChooseCharacter/ChooseCharacter';
import Faq from './components/Faq/Faq';
import GameCategories from './components/GameCategories/GameCategories';
import Games from './components/Games/Games';
import ReadyToPlay from './components/ReadyToPlay/ReadyToPlay';
import Tournament from './components/Tournament/Tournament';
import WelcomeSection from './components/Welcome/Welcome';
import { useHomepageContext } from './context';
import { Content, Cover, Wrapper } from './styles';

export const Home = () => {
  const { categories } = useCommon();
  const { popularGames } = useHomepageContext();

  return (
    <Wrapper>
      <Content>
        <WelcomeSection />
        <GameCategories categories={categories} />
      </Content>
      <Cover>
        <PopularGames
          title="main_page_recommended_for_you"
          popularGames={popularGames}
        />
        <ChooseCharacter />
      </Cover>
      <Games />
      <Bonuses />
      <Tournament />
      <ReadyToPlay />
      <Faq />
    </Wrapper>
  );
};
